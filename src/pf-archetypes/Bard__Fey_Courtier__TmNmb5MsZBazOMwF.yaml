_id: TmNmb5MsZBazOMwF
_key: "!items!TmNmb5MsZBazOMwF"
_stats:
  coreVersion: "12.331"
folder: Xp99iiO8M955HvSy
img: systems/pf1/icons/items/inventory/lute.jpg
name: Bard (Fey Courtier)
system:
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>Fey who associate with courts often become bards skilled at navigating
      the inhuman societies of the trackless wilds.</p><p><strong>Bardic
      Performance</strong>: The fey courtier has access to the following bardic
      performances.</p><p><strong>Scorn of the Wilds (Sp)</strong>: At 8th
      level, the fey courtier can use bardic performance to permanently mark a
      creature with nature’s displeasure by spending 2 rounds performing while
      the target remains within 30 feet. The performance includes a litany of
      the target’s misdeeds against fey and nature. Unless the target succeeds
      at a Will saving throw (DC = 10 + half the bard’s level + his Charisma
      modifier), it is cursed as per <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=nature%E2%80%99s%20exile">nature’s
      exile</a> or @UUID[Compendium.pf1.spells.Item.lsboosfk26m0ycqv]{bestow
      curse}. At 14th level, the fey courtier’s options expand to include
      @UUID[Compendium.pf1.spells.Item.t8ei2mrmqg7252vw]{baleful polymorph} and
      @UUID[Compendium.pf1.spells.Item.w2pcwjs6tjk7kf8r]{green caress}. As part
      of the performance, the fey courtier must define a condition under which
      the curse will be lifted, which must relate to making amends for the crime
      against fey or nature and cannot be suicidal to carry out. A creature that
      succeeds at its saving throw is immune to the fey courtier’s scorn of the
      wild for 24 hours. This is a curse effect.</p><p>This replaces dirge of
      doom and frightening tune.</p><p><strong>Stone Dance (Sp)</strong>: At
      15th level, the bard’s performance can move even the features of the
      natural world. This performance functions as
      @UUID[Compendium.pf1.spells.Item.qt0xmkag6ahtvesm]{animate plants} (DC =
      10 + half the bard’s level + his Charisma modifier) except it can also
      animate unworked stone and water from natural bodies of water (with
      statistics equivalent to an appropriate plant).</p><p>This replaces
      inspire heroics.</p><p><strong>Fey Contacts</strong>: At 2nd level, thanks
      to his fey contacts and knowledge of the byzantine trade customs of the
      fey courts, a fey courtier can treat any wilderness area with fey
      residents as a settlement with a certain base value and purchase limit
      based on his bard level for the purpose of buying and selling magic items.
      See <a
      href="https://aonprd.com/Rules.aspx?Name=The%20Settlement%20Stat%20Block&amp;Category=Settlements%20in%20Play">here</a>
      for rules on settlement base values and purchase limits. The fey courtier
      can also use these contacts to gather information about the residents,
      geography, and nearby areas with Diplomacy.</p><p>This replaces versatile
      performance.</p><table><tbody><tr><td>Bard Level</td><td>Base
      Value</td><td>Purchase Limit</td></tr><tr><td>2nd</td><td>50
      gp</td><td>500 gp</td></tr><tr><td>6th</td><td>500 gp</td><td>2,500
      gp</td></tr><tr><td>10th</td><td>2,000 gp</td><td>10,000
      gp</td></tr><tr><td>14th</td><td>8,000 gp</td><td>50,000
      gp</td></tr><tr><td>18th</td><td>32,000 gp</td><td>200,000
      gp</td></tr></tbody></table><p><strong>Summon Fey Allies</strong>: At 3rd
      level, the fey courtier gains
      @UUID[Compendium.pf1.feats.Item.7bJjKqNRrvpXJ4L3]{Augment Summoning} as a
      bonus feat and can call upon natural allies who owe him favors. He adds <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=summon%20nature%E2%80%99s%20ally%201">summon
      nature’s ally I</a> to his list of 1st-level bard spells known. Whenever
      he gains access to another spell level, he adds the next-higher <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=summon%20nature%E2%80%99s%20ally%201">summon
      nature’s ally</a> spell to his list of bard spells known. The fey courtier
      adds the following creatures to the summon nature’s ally list: 1st—<a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Grimple">grimple
      (gremlin)</a>, <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Mite">mite</a>, <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Sprite">sprite</a>;
      2nd—<a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Atomie">atomie</a>,
      <a href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Fuath">fuath
      (gremlin)</a>, <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Nixie">nixie</a>;
      3rd—<a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Leprechaun">leprechaun</a>,
      <a href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Nuglub">nuglub
      (gremlin)</a>, <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Pooka">pooka</a>;
      4th—<a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Kelpie">kelpie</a>,
      <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Korred">korred</a>,
      <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Twigjack">twigjack</a>;
      5th—<a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Lampad">lampad</a>,
      <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Lurker%20in%20Light">lurker
      in light</a>, <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Swan%20Maiden">swan
      maiden</a>; 6th—<a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Cold%20Rider">cold
      rider</a>, <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Oceanid">oceanid</a>,
      <a
      href="https://aonprd.com/MonsterDisplay.aspx?ItemName=Sangoi">sangoi</a>.</p><p>This
      replaces inspire competence.</p>
  sources:
    - id: PZO1140
      pages: "11"
  subType: misc
  tags:
    - Fey Courtier
type: feat

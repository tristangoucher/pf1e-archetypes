_id: Wr246R7u5E25dngm
_key: "!items!Wr246R7u5E25dngm"
_stats:
  coreVersion: "12.331"
folder: zN9G5h6ESZRfsAoA
img: systems/pf1/icons/items/equipment/gloves.jpg
name: Monk UC (Invested Regent)
system:
  associations:
    classes:
      - Monk UC
  description:
    value: >-
      <p>The invested regent has been handpicked by inscrutable divine forces to
      command others—either at the present or at some future time. The invested
      regent can harness a divine spark to perform superhuman stunts, influence
      others, and escape injury. Invested regents come from all races and walks
      of life, but they are more likely to be nobleborn in their cultures.
      Although some invested regents are acutely aware of their powers’ divine
      origins (such as powers gained from an investment ceremony to Asmodeus in
      the nation of Cheliax), others do not know the source of their abilities.
      Invested regents have strong personalities and a destiny to rule.<br /><br
      />Investiture (Su): An invested regent gains a pool of investiture points,
      supernatural energy he can use to call upon amazing abilities and divine
      protection. The number of points in the invested regent’s investiture pool
      is equal to 1/2 his monk level + his Charisma modifier. As long as he has
      at least 1 point in his investiture pool, the invested regent can select
      one of his saving throws as a swift action and gain a +1 sacred bonus on
      saving throws of that type for 1 round. If he spends 1 point from his
      investiture pool during this swift action, he instead gains a sacred bonus
      equal to his Charisma modifier on the saving throw selected.</p><p>A
      character with this feature and the ki pool class feature tracks
      investiture points and ki points separately.</p><p>This ability replaces
      the bonus feat gained at 1st level.</p><p><strong>Vested Power (Ex or
      Sp)</strong>: At 2nd level and every 4 monk levels thereafter, an invested
      regent can select a vested power (see the Vested Powers section below) for
      which he qualifies in place of selecting a monk bonus feat. The invested
      regent need not do so and can instead take the bonus feat, but once the
      decision to take a bonus feat or a vested power is made, he can’t change
      it.</p><p>This replaces the bonus feat the invested regent gives up for
      the vested power.</p><p><strong>Vested Powers</strong>: Vested powers are
      abilities that draw on an invested regent’s investiture pool. Vested
      powers are divided into two categories: feats and
      spells.</p><p><strong>Requirements</strong>: All vested powers have a
      minimum level requirement to select them. An invested regent who hasn’t
      reached the required monk level cannot select that vested
      power.</p><p><strong>Activation</strong>: Most vested powers require the
      invested regent to spend investiture points; the exact amount is listed
      after the vested power. Vested powers that cost 0 investiture points don’t
      require the invested regent to have any investiture points in his
      investiture pool to use the ability. The saving throw against an invested
      regent’s vested power, if any, is equal to 10 + 1/2 the invested regent’s
      monk level + his Charisma bonus.</p><p><strong>Feats</strong>: These
      vested powers duplicate the effects of specific feats. An invested regent
      doesn’t need to qualify for a feat to select it as a vested power. For
      example, an invested regent can select Spring Attack as a vested power
      even if he doesn’t meet the prerequisites for selecting Spring Attack as a
      feat. Activating one of these vested powers is a free action on the
      invested regent’s turn; until the start of his next turn, the invested
      regent is treated as if he had that feat. Feats marked with an asterisk
      (*) can also be activated as an immediate action when it isn’t the
      invested regent’s turn.</p><p><strong>Spells</strong>: These vested powers
      duplicate the effects of a spell and are spell-like abilities. An invested
      regent’s monk level is the caster level for these spell-like abilities,
      and he uses Charisma to determine his concentration check bonus.
      Activating one of these vested powers is a standard
      action.</p><p>2nd-Level Vested Powers<br />Comprehend languages (2
      investiture points)<br />Divine favor (1 investiture point)<br />Dodge* (0
      investiture points)<br />Entropic shield (1 investiture point)<br
      />Expeditious retreat (2 investiture points)<br />Feather stepAPG (self
      only, 1 investiture point)<br />Hide from undead (self only, 1 investiture
      point)<br />Sanctuary (self only, 1 investiture point)<br />Shield of
      faith (self only, 1 investiture point)</p><p>6th-Level Vested Powers<br
      />Calm emotions (1 investiture point)<br />Cloak of windsAPG (self only, 2
      investiture points)<br />Enthrall (2 investiture points)<br />Gaseous form
      (self only, 1 investiture point)<br />Helping hand (1 investiture
      point)<br />Heroic DefianceAPG, * (1 investiture point)<br />Heroic
      RecoveryAPG, * (1 investiture point)<br />Protective spiritAPG (2
      investiture points)<br />Remove disease (self only, 2 investiture
      points)<br />SidestepAPG, * (1 investiture point)<br />Snatch Arrows* (1
      investiture point)<br />Spring Attack (1 investiture point)<br />Tongues
      (self only, 2 investiture points)</p><p>10th-Level Vested Powers<br />Air
      walk (self only, 2 investiture points)<br />Death ward (self only, 2
      investiture points)<br />Dimension door (self only, 2 investiture
      points)<br />Discern lies (3 investiture points)<br />Divine power (3
      investiture points)<br />Freedom of movement (self only, 3 investiture
      points)<br />Improved Blind-FightAPG, * (1 investiture point)<br
      />Improved Great Fortitude* (1 investiture point)<br />Improved Iron Will*
      (1 investiture point)<br />Improved Lightning Reflexes* (1 investiture
      point)<br />Mark of justice (3 investiture points)<br />Neutralize poison
      (self only, 3 investiture points)<br />Restoration (self only, 2
      investiture points)<br />Spell resistance (self only, 2 investiture
      points)<br />Wind Stance (2 investiture points)</p><p>14th-Level Vested
      Powers<br />Antilife shell (3 investiture points)<br />Disarming StrikeAPG
      (2 investiture points)<br />Greater Blind-FightAPG, * (2 investiture
      points)<br />Greater command (3 investiture points)<br />Greater forbid
      actionUM (3 investiture points)<br />Lightning Stance (3 investiture
      points)<br />Shadow walk (self only, 3 investiture points)<br />Unwilling
      shieldAPG (3 investiture points)</p><p>18th-Level Vested Powers<br
      />Divine vesselAPG (3 investiture points)<br />Foresight (self only, 3
      investiture points)<br />Greater restoration (self only, 3 investiture
      points)<br />Regenerate (self only, 3 investiture points)<br />Repulsion
      (3 investiture points)</p>
  sources:
    - id: PZO9476
      pages: "22"
  subType: misc
  tags:
    - Invested Regent
type: feat

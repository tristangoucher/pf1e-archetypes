_id: Gi7oNIg5CVj15jgb
_key: "!items!Gi7oNIg5CVj15jgb"
_stats:
  coreVersion: "12.331"
folder: cR3mojLmFiF6DK7t
img: systems/pf1/icons/skills/shadow_03.jpg
name: Antipaladin (Dread Vanguard)
system:
  associations:
    classes:
      - Antipaladin
  description:
    value: >-
      <p>Some antipaladins serve or ally themselves with villains who are bent
      on earthly conquest. They care nothing for the intricacies of divine
      spellcasting, but malevolent energy still surrounds them. Whether alone or
      at the head of a marauding host, these cruel warriors bring suffering and
      death—but their presence also heralds the coming of a greater
      evil.</p><p>Dread vanguards have the following class
      features.</p><p><strong>Beacon of Evil (Su)</strong>: A dread vanguard
      unleashes the powers of his vile masters to strengthen both himself and
      his allies.</p><p>At 4th level and every 4 level thereafter, a dread
      vanguard gains one additional use of his touch of corruption ability per
      day. As a standard action, he can spend a use of his touch of corruption
      ability to manifest the darkness in his soul as an area of flickering
      shadows with a 30-foot radius centered on him. These shadows don’t affect
      visibility. The antipaladin and all allies in the area gain a +1 morale
      bonus to AC and on attack rolls, damage rolls, and saving throws against
      fear, and also ignore the first 5 points of hardness when attacking
      unattended inanimate objects. This lasts for 1 minute, as long as the
      dread vanguard is conscious.</p><p>At 8th level, the aura grants fast
      healing 3 to the dread vanguard as well as to his allies while they remain
      within it. Additionally, while this aura is active, the antipaladin can
      use his touch of corruption ability against any targets within its radius
      by making a ranged touch attack.</p><p>At 12th level, when he activates
      this ability, a dread vanguard can choose to increase the radius of one
      antipaladin aura he possesses to 30 feet. Also, the morale bonus granted
      to AC and on attack rolls, damage rolls, and saving throws against fear
      increases to +2.</p><p>At 16th level, the fast healing granted by this
      ability increases to 5. Additionally, the antipaladin’s weapons and those
      of his allies within the aura’s radius are considered evil for the purpose
      of overcoming damage reduction.</p><p>At 20th level, the beacon of evil’s
      radius increases to 60 feet, and the morale bonus granted to AC and on
      attack rolls, damage rolls, and saving throws against fear increases to
      +4. Lastly, attacks made by the dread vanguard and his allies within the
      aura’s radius are infused with pure unholy power, and deal an additional
      1d6 points of damage.</p><p>This ability replaces the spells class
      feature. A dread vanguard does not gain any spells or spellcasting
      abilities, cannot use spell trigger or spell completion magic items, and
      does not have a caster level from this class.</p><p><strong>Dark Emissary
      (Sp)</strong>: At 14th level, a dread vanguard becomes a true messenger of
      the forces of darkness he serves. Once per day, the dread vanguard can
      expend two uses of his touch of corruption ability to mark one location
      within 60 feet with the stain of evil. This location can be any point in
      space, but the ability works best if placed on an altar, shrine, or other
      site important to a community. The location is affected as if by a
      @UUID[Compendium.pf1.spells.Item.ffsr22oaciurke4e]{desecrate} spell.
      Creatures approaching within 30 feet of the site must succeed at a Will
      save or suffer the effects of
      @UUID[Compendium.pf1.spells.Item.zt7cje76dck9hwp9]{crushing
      despair}.</p><p>At 17th level, the dread vanguard can also mark the site
      with a @UUID[Compendium.pf1.spells.Item.k6nfhf1wvik082wu]{symbol of pain},
      and at 20th level, he adds a
      @UUID[Compendium.pf1.spells.Item.ylewzn5nlzy7ugdm]{symbol of weakness}. If
      available, all three of these effects overlap. Creatures must save against
      each effect individually, and the effects stack. The caster level for all
      effects is equal to the dread vanguard’s class level. The save DC is equal
      to 10 + 1/2 the antipaladin’s level + his Charisma modifier. A location
      remains marked in this way for up to 1 day per antipaladin level. During
      this time, the @UUID[Compendium.pf1.spells.Item.zt7cje76dck9hwp9]{crushing
      despair} and symbols are triggered when the first target enters each
      spell’s area. They remain active for 10 minutes, then can be triggered
      again if targets are within range. If the effects are disabled, they
      become inactive for 10 minutes, but then can be triggered again as normal.
      @UUID[Compendium.pf1.spells.Item.gmgwyjfpeuuc4t4o]{Dispel magic} and
      similar spells suppress the effects, and all effects of dark emissary can
      be removed by a
      @UUID[Compendium.pf1.spells.Item.zpav8ywcqwkcuojw]{consecrate} spell cast
      on the location by a cleric of a level equal to or higher than the dread
      vanguard.</p><p>Allies or evil creatures who serve the same power or
      organization as the dread vanguard are immune to the
      @UUID[Compendium.pf1.spells.Item.zt7cje76dck9hwp9]{crushing despair} and
      symbol effects, and automatically know that the location has been marked
      for their masters. They can treat this location as very familiar for the
      purpose of @UUID[Compendium.pf1.spells.Item.ptqu8gtrujxsvwy1]{teleport}
      and similar spells and can use
      @UUID[Compendium.pf1.spells.Item.g66w32kdup1f43bp]{scrying} and related
      spells as though they were familiar with any subject within 20 feet of it.
      At 14th level, a dread vanguard can have one marked site active at a time.
      At 17th and 20th levels, he can add one additional site that is
      simultaneously active. Marking a new site beyond this limit ends all
      effects on the oldest active site. This ability replaces aura of sin.</p>
  sources:
    - id: PZO9450
      pages: "22"
  subType: misc
  tags:
    - Dread Vanguard
type: feat

_id: FwGyer5PLS83GchA
_key: "!items!FwGyer5PLS83GchA"
_stats:
  coreVersion: "12.331"
folder: Uw8cJo0BkVDMuq8W
img: systems/pf1/icons/skills/blue_34.jpg
name: Shifter (Swarm Shifter)
system:
  associations:
    classes:
      - Shifter
  description:
    value: >-
      <p>Swarm shifters channel the essence of the basest and most overlooked
      animals, vermin.<br /><br />Vermin Aspect (Su): As a swift action, a swarm
      shifter can transform into a swarm of vermin. While in her vermin form,
      she gains a +2 bonus to natural armor. Her size increases by one size
      category (as @UUID[Compendium.pf1.spells.Item.jnlr9cuepka1l26e]{enlarge
      person}, except her reach does not increase to 10 feet), but she can
      occupy the same space as a creature of any size. She must still attack a
      target as normal, even if occupying the same space as her target. She can
      maintain this form for a number of minutes per day equal to 3 + her
      shifter level. The duration does not need to be consecutive, but it must
      be spent in 1-minute increments.</p><p>At 5th level, a swarm shifter
      becomes immune to bull rush, grapple, and trip combat maneuvers while in
      vermin form. At 10th level, the swarm shifter’s bonus to natural armor
      increases to +4 while in vermin form.</p><p>At 15th level, a swarm shifter
      gains the <a
      href="https://aonprd.com/UMR.aspx?ItemName=Distraction">distraction</a>
      universal monster ability while in vermin form.</p><p>At 20th level, the
      swarm shifter becomes immune to critical hits and flanking while in vermin
      form, but unlike a normal swarm, doesn’t become immune to normal weapon
      damage.</p><p>This replaces wild shape, shifter aspect, and all
      improvements to shifter aspect.</p><p><strong>Swarmer (Su)</strong>:
      Starting at 4th level, while a swarm shifter is in her natural form, she
      can transform her hands into living vermin as a swift action at will. This
      grants her a touch attack that deals 1d6 points of piercing damage and
      counts as an area attack for the purpose of overcoming the damage
      reduction of swarms. She doesn’t add her Strength modifier as a bonus on
      damage rolls for this attack. This damage increases by 1d6 at 7th level
      and every 4 levels thereafter (maximum 6d6 points of damage at 20th
      level). A swarm shifter can’t use her normal shifter claws while her hands
      are transformed in this way.</p><p>At 10th level, a swarm shifter can use
      this ability to automatically deal her touch attack damage to foes that
      she is grappling.</p><p>At 15th level, a swarm shifter can use this
      ability while in her vermin form to automatically deal her touch attack
      damage to foes within her squares.</p><p><strong>Swarm Flow (Su)</strong>:
      At 9th level, when a swarm shifter uses her vermin aspect ability, she
      gains a swarm ability from the list below. She can select a different
      ability each time she uses her vermin aspect ability.
      </p><p><strong>Crawling</strong>: The swarm shifter gains a climb speed
      equal to her base speed. </p><p><strong>Flying</strong>: The swarm shifter
      gains a fly speed equal to her base speed, with good maneuverability.
      </p><p><strong>Undulating</strong>: The swarm shifter gains a burrow speed
      equal to her base speed.</p><p>This replaces chimeric
      aspect.</p><p><strong>Greater Swarm Flow (Su)</strong>: At 14th level,
      when using her vermin aspect ability, a swarm shifter also gains a greater
      swarm ability from the list below. She can select a different ability each
      time she uses her vermin aspect ability. </p><p><strong>Crawling</strong>:
      The swarm shifter gains <a
      href="https://aonprd.com/UMR.aspx?ItemName=Tremorsense">tremorsense</a>
      with a range of 60 feet and a +4 bonus on Perception checks. <br /><br
      />Flying: The swarm shifter’s claws change to venomous stingers that
      deliver poison on each attack. (Poison: injury; save Fort DC = 10 + half
      the swarm shifter’s level + her Constitution modifier; frequency 1/round
      for 6 rounds; effect 1d3 Dexterity damage; cure 2 consecutive saves). <br
      /><br />Undulating: Whenever the swarm shifter is subject to a critical
      hit, she gains <a
      href="https://aonprd.com/UMR.aspx?ItemName=Fast%20Healing">fast
      healing</a> equal to 1/3 her level for a number of rounds equal to 3 + her
      Wisdom modifier.</p><p>This replaces greater chimeric aspect.<br /><br
      />Final Aspect (Su): At 20th level, a swarm shifter becomes an embodiment
      of swarms and can cast
      @UUID[Compendium.pf1.spells.Item.25iy235izs71qlns]{swarm skin} as a
      spell-like ability at will with a caster level equal to her character
      level. She can separate into up to five swarms, regardless of the number
      of levels required per swarm. When transforming into swarms with this
      ability, the swarm shifter still selects swarm abilities granted by the
      swarm flow and greater swarm flow abilities to apply to her swarm forms.
      The chosen swarm abilities apply to all swarms.</p><p>This modifies final
      aspect.</p><h2>Alternate Natural Attacks</h2><p>A shifter can draw on her
      chosen animal aspect to transform her hands into deadly weapons, as
      represented by the shifter’s claws class feature, but not every animal has
      prominent claws. The following list provides alternate natural attacks for
      the shifter claws class feature. Each time the shifter activates her
      shifter’s claws ability in her natural form, she can manifest one of the
      alternate natural attacks listed below for any of her chosen aspects, or
      those that relate to her archetype. Each alternate natural attack replaces
      one of the shifter’s claw attacks. The shifter can gain up to two
      different alternate natural attacks with this method. These alternate
      natural attacks modify only the damage type of the shifter’s natural
      attacks and otherwise function exactly as the shifter claws class
      feature.</p><p><strong>Swarm (Swarm Shifter Archetype)</strong>: Bite (B,
      P, S), pincers (B), sting (P).</p>
  sources:
    - id: PZO9494
      pages: "7"
  subType: misc
  tags:
    - Swarm Shifter
type: feat

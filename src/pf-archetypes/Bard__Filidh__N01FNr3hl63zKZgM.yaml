_id: N01FNr3hl63zKZgM
_key: "!items!N01FNr3hl63zKZgM"
_stats:
  coreVersion: "12.331"
folder: Xp99iiO8M955HvSy
img: systems/pf1/icons/items/inventory/lute.jpg
name: Bard (Filidh)
system:
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>By tapping into the world’s natural music, specialized bards known as
      filidhs are able to see not only the tapestry of life but divine portents
      of the future from the rhythm of all life’s song.</p><p>Armor and Weapon
      Proficiency: A filidh is proficient with light armor and is prohibited
      from wearing metal armor. A filidh can wear wooden armor that has been
      altered by the ironwood spell. Filidhs are proficient with shields (except
      tower shields) but can use only those made of wood.</p><p>A filidh who
      wears prohibited armor or uses a prohibited shield is unable to cast bard
      spells or use any of his supernatural or spell-like class abilities while
      doing so and for 24 hours thereafter.</p><p>This alters a bard’s normal
      armor and weapon proficiencies.</p><p><strong>Natural Magic</strong>: A
      filidh casts spells as a bard, but the spells are divine, not arcane, and
      therefore not subject to arcane spell failure. A filidh must use a holy
      symbol or a musical instrument as a divine focus when a spell includes
      such a component. He still uses the bard spell list.</p><p>This alters the
      bard’s spellcasting.</p><p>Nature’s Song: Filidhs can hear the resonant
      song generated by all living creatures—a primal music that stretches back
      to the beginning of time and ahead to the unwritten future. By tapping
      into this cosmic melody, a filidh can steal glimpses of the future. Once
      per day per level, a filidh can sacrifice a spell slot as a swift action
      to gain an additional number of rounds of bardic performance for that day
      equal to the spell’s level.</p><p>In addition, a filidh gains the
      following types of bardic performances.</p><p>Echoes of Nature’s Song
      (Su): A filidh can use his bardic performance to imbue his allies with
      subconscious knowledge of their own futures, improving their reflexes and
      ability to avoid danger. An affected ally receives a +1 insight bonus on
      Reflex saving throws and to AC. At 5th level and every 6 bard levels
      thereafter, this bonus increases by 1, to a maximum of +4 at 17th level.
      This performance is a mind-affecting ability and relies on audible
      components.</p><p>This performance replaces inspire
      courage.</p><p><strong>Divinatory Song (Sp)</strong>: At 6th level, a
      filidh can use his performance to create an effect equivalent to
      @UUID[Compendium.pf1.spells.Item.pwueht12qqo0268e]{divination}, using the
      filidh’s level as the caster level. The filidh and all allies who can hear
      his performance receive the information provided by the divination as
      flashes of inspiration and knowledge. Divinatory song takes 10 minutes and
      uses 6 rounds of bardic performance. Divinatory song relies on audible
      components.</p><p>This performance replaces
      suggestion.</p><p><strong>Voices of Life (Su)</strong>: At 8th level, a
      filidh can grant himself and all allies who can hear his performance the
      ability to speak with animals and plants as if affected by
      @UUID[Compendium.pf1.spells.Item.ct9fhh8uawn8e4md]{speak with animals} and
      @UUID[Compendium.pf1.spells.Item.rrsefzpm3nhztvld]{speak with plants} so
      long as he maintains this performance.</p><p>This performance replaces
      dirge of doom.</p><p><strong>Unity of Life (Su)</strong>: At 15th level, a
      filidh can use his performance to interlink the life force of two allies
      who can hear his performance as though they were affected by
      @UUID[Compendium.pf1.spells.Item.menqakfoa1ftvi3f]{shield other}, using
      the filidh’s bard level as his caster level. The filidh designates which
      ally is the warded target and which ally receives half the warded
      creature’s damage. The filidh can switch the targets of this ability (and
      the effect of the performance the targets receive) as a free action once
      per round at the start of each turn that he maintains the
      performance.</p><p>This performance replaces inspire
      heroics.</p><p><strong>Song of the Cycle (Su)</strong>: At 20th level, a
      filidh can grant awe-inspiring glimpses into the future with his magic.
      All allies who can see and hear the filidh are affected as though by the
      personal version of
      @UUID[Compendium.pf1.spells.Item.vl7yer8k1leyuxld]{foresight} for the
      duration of the performance.</p><p>This performance replaces deadly
      performance.</p>
  sources:
    - id: PZO1140
      pages: "40"
  subType: misc
  tags:
    - Filidh
type: feat

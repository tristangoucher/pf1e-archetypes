_id: UTEx7eIUjPX0Cypz
_key: "!items!UTEx7eIUjPX0Cypz"
_stats:
  coreVersion: "12.331"
folder: Uw8cJo0BkVDMuq8W
img: systems/pf1/icons/skills/blue_34.jpg
name: Shifter (Feyform Shifter)
system:
  associations:
    classes:
      - Shifter
  description:
    value: >-
      <p>Feyform shifters are intimately linked to the First World, and can draw
      from it to gain otherworldly powers.</p><p><strong>Fey Aspect
      (Su)</strong>: A feyform shifter can take on a First World aspect and
      assume fey traits as a swift action. While in this form, she gains
      low-light vision (or darkvision with a range of 30 feet, if she already
      has low-light vision) and DR 1/cold iron. Her body outline becomes
      indistinct; she is treated as though she has concealment, except that she
      cannot use this concealment to attempt Stealth checks. She can maintain
      this form for a number of minutes per day equal to 3 + her shifter level.
      The duration does not need to be consecutive, but it must be spent in
      1-minute increments.</p><p>At 5th level, the feyform shifter’s DR
      increases to 2/cold iron, and she grows a pair of butterfly-like wings
      that grant her a fly speed of 30 feet with average
      maneuverability.</p><p>At 10th level, the feyform shifter’s DR increases
      to 5/cold iron, and she gains a +4 bonus on saving throws against
      enchantment spells and effects.</p><p>At 15th level, the feyform shifter’s
      DR increases to 7/cold iron, and her fly speed maneuverability increases
      to good.</p><p>At 20th level, the feyform shifter’s DR increases to
      10/cold iron. She becomes resistant to movement-impairing effects (as
      @UUID[Compendium.pf1.spells.Item.lvzq2mwkqmozolpl]{freedom of movement})
      and gains spell resistance equal to 10 + her level.<br /><br />This alters
      and replaces all improvements to shifter aspect.<br /><br />Fey Shape
      (Su): At 4th level, a feyform shifter can use her wild shape ability to
      become a fey creature. The fey shifter must spend at least one use of wild
      shape to transform into a fey creature; this ability functions as
      @UUID[Compendium.pf1.spells.Item.r26vk747yhmjdeqv]{fey form I}, except
      that it lasts for only 1 minute per use of wild shape spent. Using fey
      shape or reverting back is a standard action that does not cause attacks
      of opportunity. At 8th level this ability instead functions as
      @UUID[Compendium.pf1.spells.Item.m25u0kpeglsma55z]{fey form II}. At 10th
      level this ability functions as
      @UUID[Compendium.pf1.spells.Item.7il67b57towi3o6w]{fey form III}, and at
      14th level it functions as
      @UUID[Compendium.pf1.spells.Item.tj94dyqy8yczodsz]{fey form
      IV}.</p><p>This alters wild shape.</p><p><strong>Fey Shifter
      (Su)</strong>: At 9th level, a feyform shifter gains a second shifter
      aspect, chosen from the animal aspects normally available to shifters.
      When she uses her shifter aspect ability to take on her fey aspect, she
      can choose a second aspect and assume the minor form of that aspect,
      alongside her fey aspect, allowing her to combine her fey aspect with the
      animal aspects available to her.</p><p>This modifies chimeric
      aspect.</p><p><strong>Greater Fey Shifter (Su)</strong>: At 14th level, a
      feyform shifter gains a third aspect. When she uses her shifter aspect
      ability to take on her fey aspect, she can choose two aspects and assume
      the minor form of each aspect, alongside her fey aspect.</p><p>This
      modifies greater chimeric aspect.</p><p><strong>Final Aspect
      (Su)</strong>: At 20th level, a feyform shifter gains access to a fourth
      aspect. When she uses shifter aspect, she can assume the minor forms of
      all her aspects in addition to her fey aspect, and she can use her major
      and minor forms at will.</p><p>This alters final aspect.</p><h2>Alternate
      Natural Attacks</h2><p>A shifter can draw on her chosen animal aspect to
      transform her hands into deadly weapons, as represented by the shifter’s
      claws class feature, but not every animal has prominent claws. The
      following list provides alternate natural attacks for the shifter claws
      class feature. Each time the shifter activates her shifter’s claws ability
      in her natural form, she can manifest one of the alternate natural attacks
      listed below for any of her chosen aspects, or those that relate to her
      archetype. Each alternate natural attack replaces one of the shifter’s
      claw attacks. The shifter can gain up to two different alternate natural
      attacks with this method. These alternate natural attacks modify only the
      damage type of the shifter’s natural attacks and otherwise function
      exactly as the shifter claws class feature.</p><p><strong>Fey (Feyform
      Shifter Archetype)</strong>: Bite (B, P, S), sting (P).</p>
  sources:
    - id: PZO9494
      pages: "6"
  subType: misc
  tags:
    - Feyform Shifter
type: feat

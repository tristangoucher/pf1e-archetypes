_id: m9g55TMoyORNJWGu
_key: "!items!m9g55TMoyORNJWGu"
_stats:
  coreVersion: "12.331"
folder: Xp99iiO8M955HvSy
img: systems/pf1/icons/items/inventory/lute.jpg
name: Bard (Detective)
system:
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>Piecing together clues and catching the guilty with sheer cleverness,
      the detective is skilled at divining the truth.</p><p><strong>Bardic
      Performance</strong>: A detective gains the following types of bardic
      performance.</p><p><strong>Careful Teamwork (Su)</strong>: A detective
      uses performance to keep allies coordinated, alert, and ready for action.
      All allies within 30 feet gain a +1 bonus on Initiative checks,
      Perception, and Disable Device checks for 1 hour. They also gain a +1
      insight bonus on Reflex saves and to AC against traps and when they are
      flat-footed. These bonuses increase by +1 at 5th level and every six
      levels thereafter. Using this ability requires 3 rounds of continuous
      performance, and the targets must be able to see and hear the bard
      throughout the performance. This ability is language-dependent and
      requires visual and audible components. This performance replaces inspire
      courage.</p><p><strong>True Confession (Su)</strong>: At 9th level, a
      detective can use performance to trick a creature into revealing its
      secrets. Using this ability requires a successful Sense Motive check to
      see through a Bluff or notice mental compulsion. After 3 continuous rounds
      of performance, the target must make a Will save (DC 10 + 1/2 the bard’s
      level + the bard’s Cha modifier). Success renders the target immune to
      this power for 24 hours. On a failed save, a liar inadvertently reveals
      the lie and the truth behind it. A creature under a charm or compulsion
      reveals the nature of its enchantment and who placed it (if the creature
      knows) and gains a new saving throw to break free from the enchantment.
      This ability is language-dependent and requires audible components. Using
      this power requires only 2 rounds of performance at 15th level, and 1
      round of performance at 20th level. This performance replaces inspire
      greatness.</p><p><strong>Show Yourselves (Ex)</strong>: At 15th level, a
      detective can use performance to compel creatures to reveal themselves
      when hiding. All enemies within 30 feet must make a Will save (DC 10 + 1/2
      the bard’s level + the bard’s Cha modifier). If they fail, they must cease
      using Stealth, unlock and open doors between themselves and the detective,
      and dismiss, suppress, or dispel if necessary magical effects that grant
      invisibility or any other form of concealment from the detective. As long
      as they can hear the performance, affected creatures may not attack or
      flee until they have eliminated every such effect, though they are freed
      from this compulsion immediately if attacked. Creatures in the area must
      make this save each round the bard continues his performance. This ability
      is language-dependent and requires audible components. This performance
      replaces inspire heroics.</p><p><strong>Eye for Detail (Ex)</strong>: A
      detective gains a bonus equal to half his level on Knowledge (local),
      Perception, and Sense Motive checks, as well as Diplomacy checks to gather
      information (minimum +1). This ability replaces bardic
      knowledge.</p><p><strong>Arcane Insight (Ex)</strong>: At 2nd level, a
      detective can find and disable magical traps, like a rogue’s trapfinding
      ability. In addition, he gains a +4 bonus on saving throws made against
      illusions and a +4 bonus on caster level checks and saving throws to see
      through disguises and protections against divination (such as
      @UUID[Compendium.pf1.spells.Item.l01ajd0m4xgwq3fd]{magic aura},
      @UUID[Compendium.pf1.spells.Item.8nrt26t37v7koqr8]{misdirection}, and
      @UUID[Compendium.pf1.spells.Item.awia42zci8nufpfl]{nondetection}). This
      ability replaces well-versed.</p><p><strong>Arcane Investigation</strong>:
      In addition, a detective’s class spell list includes the following:
      1st—@UUID[Compendium.pf1.spells.Item.17eskras8cjynj10]{detect chaos}/<a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=detect%20evil">evil</a>/<a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=detect%20law">law</a>/<a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=detect%20good">good</a>;
      2nd—@UUID[Compendium.pf1.spells.Item.snspotd2cno49ed5]{zone of truth};
      3rd—@UUID[Compendium.pf1.spells.Item.fmuea6pmgrylbux7]{arcane eye},
      @UUID[Compendium.pf1.spells.Item.byn5q46du0ck592a]{speak with dead},
      @UUID[Compendium.pf1.spells.Item.rrsefzpm3nhztvld]{speak with plants};
      4th—@UUID[Compendium.pf1.spells.Item.9vdjohbrj896e2ot]{discern lies};
      5th—@UUID[Compendium.pf1.spells.Item.68quvqg93a3da02z]{prying eyes},
      @UUID[Compendium.pf1.spells.Item.wgm8mm1za909pwch]{stone tell};
      6th—@UUID[Compendium.pf1.spells.Item.gw2sgj93dgarpem8]{discern location},
      @UUID[Compendium.pf1.spells.Item.x619lrcykmmwts0f]{find the path}, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=prying%20eyes,%20greater">greater
      prying eyes</a>, @UUID[Compendium.pf1.spells.Item.2vb5orfcy57lrfmc]{moment
      of prescience}. A detective may add one of these spells or any divination
      spell on the bard spell list to his list of spells known at 2nd level and
      every four levels thereafter. This ability replaces versatile
      performance.</p>
  sources:
    - id: PZO1115
      pages: "81"
  subType: misc
  tags:
    - Detective
type: feat

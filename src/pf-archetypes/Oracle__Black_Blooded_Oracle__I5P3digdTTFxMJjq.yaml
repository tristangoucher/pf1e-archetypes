_id: I5P3digdTTFxMJjq
_key: "!items!I5P3digdTTFxMJjq"
_stats:
  coreVersion: "12.331"
folder: kjqC1EAAcdV4nnJs
img: systems/pf1/icons/skills/red_09.jpg
name: Oracle (Black-Blooded Oracle)
system:
  associations:
    classes:
      - Oracle
  description:
    value: >-
      <p>The black-blooded oracle has been infused with the eerie influence of
      strange fluids that seep from the rock in the deepest pits of the
      Darklands, the so-called “Black Blood of Orv.” Often, the eerie taint of
      the black blood can lie dormant in a lineage for generations before
      manifesting. It even seems capable of reaching out to touch those destined
      to become oracles through other strange methods. However an oracle becomes
      black-blooded, her divine powers carry within them the weird power of this
      chilling magical substance. A black-blooded oracle has the following class
      features.</p><p>See the sidebar on the facing page for rules on the black
      blood itself, but note that a black-blooded oracle does not require access
      to this eldritch substance to function.</p><p><strong>Curse of Black Blood
      (Su)</strong>: All black-blooded oracles effectively share the same
      curse—the curse of black blood. The material affects these oracles
      physically and mentally, altering both physiology and mystic powers. The
      blood of a black-blooded oracle actually runs black, and wounds she
      suffers are infected by her own power and are difficult to heal. She is
      immune to the effects (both beneficial and destructive) of black blood.
      Positive and negative energy affect a black-blooded oracle as if she were
      undead— positive energy harms her, while negative energy heals her (this
      aspect of the curse has no effect if the oracle is undead). The curse also
      dulls the oracle’s coordination somewhat, imparting a –4 penalty on all
      Dexterity-based skill checks. At 5th level, she gains cold resistance 5.
      This increases to cold resistance 10 at 10th level, and immunity to cold
      at 15th level. This ability replaces the oracle’s
      curse.</p><p><strong>Black Blood Revelations</strong>: All black-blooded
      oracles have access to the following revelations, regardless of what
      mystery they choose.</p><p><strong>Black Blood Spray (Su)</strong>: As an
      immediate action whenever a black-blooded oracle takes piercing or
      slashing damage, she can cause some of her black blood to spray from the
      wound to strike any adjacent target. She must make a touch attack to hit
      the target (if she’s attacking the creature that caused the wound, she
      gains a +4 circumstance bonus on her attack roll). If she hits, she deals
      1d8 points of cold damage + 1 point per 2 oracle levels she possesses. She
      can use this ability a number of times per day equal to 1/2 her oracle
      level (minimum 1/day).</p><p><strong>Dark Resilience (Su)</strong>: The
      black blood flowing through a black-blooded oracle’s veins gives her
      resistance to many effects to which undead are immune. Once per day, as an
      immediate action, whenever she fails a saving throw against an ability
      drain, a death effect, disease, energy drain, paralysis, or poison, she
      may attempt that saving throw again with a +4 circumstance bonus. She must
      take the second result, even if it is worse. At 7th level, she can use
      this ability twice per day. At 15th level, she can use the ability 3 times
      per day.</p><p><strong>Darkvision (Ex)</strong>: A black-blooded oracle
      gains darkvision with a range of 60 feet. The range increases to 90 feet
      at 15th level.</p><h2>Black Blood of Orv</h2><p>Black blood wells from the
      walls of a singular vault deep in Orv, a region known quite aptly as the
      Land of Black Blood. This noxious fluid repulses and freezes all natural
      life, although certain creatures like aberrations, undead, and
      black-blooded oracles possess an uncanny immunity to its effects. Any
      other creature that comes into contact with black blood takes 1d6 points
      of cold damage—10d6 points of cold damage per round if fully
      immersed.</p><p>Although black blood isn’t itself inherently evil, a pint
      of black blood can be used as unholy water. A spellcaster who consumes a
      dose of black blood casts all necromantic spells at +1 caster level for
      the next 10 minutes, but takes 3d6 points of cold damage and 1 point of
      Constitution damage by drinking the stuff. Normally, a dose of black blood
      becomes inert an hour after being harvested from the source in the Land of
      Black Blood, losing all mystical properties. Gentle repose can preserve up
      to 1 gallon of black blood’s magical properties for increased lengths of
      time, but no other method of stabilizing the stuff has yet been developed.
      The blood of a black-blooded oracle is diluted and does not have the
      properties of full-strength black blood.</p>
  sources:
    - id: PZO9237
      pages: "32"
  subType: misc
  tags:
    - Black-Blooded Oracle
type: feat

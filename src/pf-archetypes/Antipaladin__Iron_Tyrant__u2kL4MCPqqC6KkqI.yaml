_id: u2kL4MCPqqC6KkqI
_key: "!items!u2kL4MCPqqC6KkqI"
_stats:
  coreVersion: "12.331"
folder: cR3mojLmFiF6DK7t
img: systems/pf1/icons/skills/shadow_03.jpg
name: Antipaladin (Iron Tyrant)
system:
  associations:
    classes:
      - Antipaladin
  description:
    value: >-
      <p>Covered from head to toe in blackened armor decorated with grotesque
      shapes and bristling with spikes, iron tyrants make an unmistakable
      impression on the battlefield. These antipaladins’ armor is an outward
      symbol of their inner power, and they are rarely seen without it. Iron
      tyrants seek the strength to rule over domains as unfettered despots, and
      depend on their armor as protection against those they have not yet
      cowed.</p><p>Most iron tyrants are orcs or half-orcs who worship the orc
      deities <a
      href="https://www.aonprd.com/DeityDisplay.aspx?ItemName=Lanishra">Lanishra
      (god of slavery, subjugation, and tyranny)</a> or <a
      href="https://www.aonprd.com/DeityDisplay.aspx?ItemName=Varg">Varg (god of
      iron, siege weapons, and warfare)</a>. Though rarer, some iron tyrants
      belong to other races and worship <a
      href="https://www.aonprd.com/DeityDisplay.aspx?ItemName=Urazra">Urazra
      (giant god of battle, brutality, and strength)</a> or demon lords such as
      <a
      href="https://www.aonprd.com/DeityDisplay.aspx?ItemName=Aldinach">Aldinach
      (She of the Six Venoms)</a>, <a
      href="https://www.aonprd.com/DeityDisplay.aspx?ItemName=Angazhan">Angazhan
      (the Ravenous King)</a>, or <a
      href="https://www.aonprd.com/DeityDisplay.aspx?ItemName=Izyagna">Izyagna
      (She of the Sevenfold Swarm)</a>.</p><p><strong>Iron Fist (Ex)</strong>:
      At 2nd level, an iron tyrant gains Improved Unarmed Strike as a bonus
      feat. In addition, whenever the iron tyrant makes a successful attack with
      a gauntlet, spiked gauntlet, or armor spikes, the weapon damage is based
      on his level and not the weapon type, as per the <a
      href="https://www.aonprd.com/ClassDisplay.aspx?ItemName=Warpriest">warpriest’s
      sacred weapon ability</a>.</p><p>This ability replaces touch of
      corruption.</p><p><strong>Bonus Feats</strong>: At 3rd level and every 3
      antipaladin levels thereafter, an iron tyrant gains a bonus feat in
      addition to those gained from normal advancement. This feat must be a <a
      href="https://www.aonprd.com/Feats.aspx?Category=Combat">combat feat</a>
      that relates to the iron tyrant’s armor or shield, such as
      @UUID[Compendium.pf1.feats.Item.7mD4ZksvKFaorAfj]{Shield Focus}, or one of
      the <a
      href="https://www.aonprd.com/Feats.aspx?Category=Armor%20Mastery">armor
      mastery feats</a>.</p><p>This ability replaces cruelty.</p><p>Unstoppable
      (Ex): At 4th level, when wearing heavy armor, an iron tyrant is not slowed
      by terrain that halves movement (such as dense rubble, light undergrowth,
      and shallow bogs). Terrain that has been magically manipulated to impede
      motion or terrain that reduces movement by more than half still affects
      him.</p><p>This ability replaces channel negative
      energy.</p><p><strong>Fiendish Bond (Su)</strong>: At 5th level, instead
      of forming a fiendish bond with his weapon or a servant, an iron tyrant
      can form a bond with his armor. As a standard action, an iron tyrant can
      enhance his armor by calling upon a fiendish spirit’s aid. This bond lasts
      for 1 minute per antipaladin level. When called, the spirit causes the
      armor to shed unholy light like a torch. At 5th level, the spirit grants
      the armor a +1 enhancement bonus. For every 3 antipaladin levels beyond
      5th, the armor gains another +1 enhancement bonus, to a maximum of +6 at
      20th level. These bonuses stack with existing armor enhancement bonuses to
      a maximum of +5, or they can be used to add any of the following armor
      special abilities: <a
      href="https://www.aonprd.com/MagicArmorDisplay.aspx?ItemName=Dastard">dastard</a>,
      <a
      href="https://www.aonprd.com/MagicArmorDisplay.aspx?ItemName=Ghost%20Touch">ghost
      touch</a>, <a
      href="https://www.aonprd.com/MagicArmorDisplay.aspx?ItemName=Fortification%20(light)">fortification
      (heavy, light, or medium)</a>, <a
      href="https://www.aonprd.com/MagicArmorDisplay.aspx?ItemName=Invulnerability">invulnerability</a>,
      <a
      href="https://www.aonprd.com/MagicArmorDisplay.aspx?ItemName=Spell%20Resistance%20(13)">spell
      resistance (13, 15, 17, or 19)</a>. Adding these special abilities
      consumes an amount of bonus equal to the special ability’s base price
      modifier. In addition, the iron tyrant can grant his armor the <a
      href="https://www.aonprd.com/MagicArmorDisplay.aspx?ItemName=Unrighteous">unrighteous</a>
      special ability at the cost of a +4 bonus. These special abilities are
      added to any special abilities the armor already has, but duplicate
      abilities do not stack. If the armor is not magical, at least a +1
      enhancement bonus must be added before any other special abilities can be
      added. The bonus and special abilities granted by the spirit are
      determined when the spirit is called and cannot be changed until the
      spirit is called again. The fiendish spirit imparts no bonuses if the
      armor is worn by anyone other than the iron tyrant, but it resumes giving
      bonuses if the iron tyrant dons the armor again. An iron tyrant can use
      this ability once per day at 5th level, and one additional time per day
      for every 4 levels beyond 5th, to a total of four times per day at 17th
      level.</p><p>If a suit of bonded armor with a fiendish spirit is
      destroyed, the iron tyrant loses the use of this ability for 30 days, or
      until he gains a level, whichever comes first. During this 30-day period,
      the iron tyrant takes a –1 penalty on attack rolls and weapon damage
      rolls.</p><p>This ability replaces fiendish boon.</p>
  sources:
    - id: PZO9467
      pages: "4"
  subType: misc
  tags:
    - Iron Tyrant
type: feat

_id: xLogPnPcEA2MMSss
_key: "!items!xLogPnPcEA2MMSss"
_stats:
  coreVersion: "12.331"
folder: lAYs1moYs2v9eUpw
img: systems/pf1/icons/items/potions/grand-yellow.jpg
name: Alchemist (Mnemostiller)
system:
  associations:
    classes:
      - Alchemist
  description:
    value: >-
      <p>Mnemostillers extract memories, storing them as a thick vapor called
      mnemos. They can return memories as easily as administering a potion, and
      experienced mnemostillers create a variety of effects through the careful
      administration of past traumas or victories.</p><p><strong>Mental
      Ambix</strong>: A mnemostiller’s alchemy relies on force of personality,
      using his own mind as an alchemy lab to draw and purify the mnemos
      collected from others. A mnemostiller uses his Charisma score in place of
      his Intelligence score for the purpose of extracts and bonus extracts. A
      mnemostiller still uses his Intelligence score to learn new
      formulae.</p><p>The mnemostiller can cast
      @UUID[Compendium.pf1.spells.Item.xllxylvvqr82o2d5]{detect thoughts} as a
      spell-like ability a number of times per day equal to his Charisma
      modifier (minimum 1).</p><p>This alters alchemy and replaces throw
      anything.</p><p><strong>Rasugen (Su)</strong>: A mnemostiller learns to
      brew a unique type of mutagen, called a rasugen, that suppresses his mind
      to enter a state of sublime purity. In this state, a mnemostiller becomes
      incredibly resilient, but cannot perform complex skills. Once imbibed, a
      rasugen grants a +2 alchemical bonus on all saving throws and 2 temporary
      hit points per alchemist level for 10 minutes per alchemist level. In
      addition, while the rasugen is in effect, a mnemostiller takes a –2
      penalty to his Intelligence score and can’t attempt checks using Appraise,
      Craft, Disable Device, Heal, Knowledge (any), Profession, Sleight of Hand,
      or Spellcraft. This acts in all other ways like a mutagen.</p><p>This
      replaces mutagen. A mnemostiller can never gain the mutagen, cognatogen,
      or inspiring cognatogen ability, even from a discovery or another
      class.</p><p><strong>Anguish Bomb (Su)</strong>: A mnemostiller crafts his
      bombs from mnemos of pain and discomfort, inflicting psychic damage. An
      anguish bomb is identical to a bomb except that it deals nonlethal damage,
      it deals no damage to inanimate objects and creatures immune to mental
      effects, and creatures caught in the anguish bomb’s splash can attempt a
      Will save for half damage. A mnemostiller uses his Charisma score in place
      of his Intelligence score to determine the bonus damage of his anguish
      bomb and the save DC of the anguish bomb’s splash damage. He still uses
      his Intelligence score to determine his number of bombs per day. If the
      mnemostiller has taken any hit point damage in the past 24 hours, his
      anguish bombs inflict 1 additional point of nonlethal damage for every 2
      alchemist levels he has.</p><p>This replaces bombs.</p><p><strong>Brewed
      Memories</strong>: A mnemostiller can create several unique extracts from
      the mnemos he pulls from the minds of others. At 2nd level, he adds
      @UUID[Compendium.pf1.spells.Item.ojtxej8ir6nycrtr]{mindlink},
      @UUID[Compendium.pf1.spells.Item.0wahvj06h5eak2fk]{placebo effect}, and
      @UUID[Compendium.pf1.spells.Item.6qev1jyrevir14ku]{thought echo} to his
      formula book as 1st-level extracts. At 5th level, he adds
      @UUID[Compendium.pf1.spells.Item.m1kfr87xoad6xsxd]{aura alteration},
      @UUID[Compendium.pf1.spells.Item.f868pfvqa6ksg84w]{detect mindscape}, and
      @UUID[Compendium.pf1.spells.Item.90xaelj2ncuzq62n]{mindscape door} to his
      formula book as 2nd-level extracts. At 8th level, he adds
      @UUID[Compendium.pf1.spells.Item.s2qrizgokj6t6u7i]{mindwipe} and
      @UUID[Compendium.pf1.spells.Item.5wmn4airvkprv3gp]{thoughtsense} to his
      formula book as 3rd-level extracts; the range for a
      @UUID[Compendium.pf1.spells.Item.s2qrizgokj6t6u7i]{mindwipe} extract
      becomes creature touched, and it can be applied only to a helpless or
      willing creature.</p><p>This replaces poison
      resistance.</p><p><strong>Natural Empath (Ex)</strong>: A mnemostiller’s
      studies naturally focus on others. He gains infusion as a bonus discovery
      at 2nd level.</p><p>This replaces poison use.</p><p>Mind-Delver (Su): At
      10th level, a mnemostiller can use
      @UUID[Compendium.pf1.spells.Item.vrzgrnmiz8kmz6di]{mind probe} as a
      spell-like ability a number of times per day equal to his Charisma
      modifier (minimum 1).</p><h2>Mnemostil</h2><p>First developed to help heal
      souls who had experienced undeath for a time, the art of mnemostil
      involves a practitioner carefully extracting or returning memories as an
      oily vapor—mnemos—which allows tortured souls to confront their past
      horrors one at a time while finding closure. In a city largely dedicated
      to waiting out eternity and cultivating absent passion, however, there are
      abundant souls eager to feel anything, good or bad. Black-market
      mnemostillers pay handsomely to extract memories from remarkable souls,
      even traveling to other planes to build up their stock before returning to
      Spire’s Edge to sell their wares. While there is some crossover with
      information brokers, mnemostillers typically sell experiences and emotions
      rather than secrets. Petitioners searching for purpose buy what memories
      they can to try and shape themselves toward one Outer Plane destination or
      another, often trading in their genuine memories as payment, until little
      of the original individual remains. Some criminal syndicates target
      visiting souls and outsiders to extract their memories by
      force.</p><p>Mnemostil has also created a unique faith in Spire’s Edge:
      amnesia cults. Many petitioners succumb to the expectation that they
      grow—that who they were in life is somehow not enough—and so render
      themselves into temporary or permanent blank slates to dwell in inert
      bliss. Others cannot reconcile their own actions and so wipe themselves
      clean to live in ignorance.</p><p>Pruning the development of the soul is
      anathema to the purpose of Spire’s Edge, and the city’s psychopomps
      aggressively hunt both mnemostillers and amnesia cultists, with punishment
      for the crime invariably being exile into the Astral Plane.</p>
  sources:
    - id: PZO92109
      pages: "52"
  subType: misc
  tags:
    - Mnemostiller
type: feat

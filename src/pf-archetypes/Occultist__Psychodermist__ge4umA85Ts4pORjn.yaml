_id: ge4umA85Ts4pORjn
_key: "!items!ge4umA85Ts4pORjn"
_stats:
  coreVersion: "12.331"
folder: 3i6bW82t3LIXM0wY
img: systems/pf1/icons/items/inventory/book-purple.jpg
name: Occultist (Psychodermist)
system:
  associations:
    classes:
      - Occultist
  description:
    value: >-
      <p>Rather than tapping the psychic energy residing within esoteric items,
      psychodermists form supernatural bonds with trophies taken from creatures
      they have slain. Through these mementos, these occultists manifest not
      only their own magic, but also the unique powers of their fallen foes.<br
      /><br />Class Skills: A psychodermist loses Disable Device, Knowledge
      (engineering), Knowledge (history), and Sleight of Hand as class skills,
      and adds Heal, Knowledge (nature), Knowledge (local), and Survival as
      class skills.</p><p>This alters the occultist’s class skills.<br /><br
      />Trophies (Su): At 1st level, a psychodermist learns how to siphon power
      from pieces of creatures he has slain. The psychodermist gains Harvest
      Parts as a bonus feat, and he can select one monster part per implement
      school he knows to become a permanently preserved trophy so long as it
      remains in his possession. Each trophy functions as the psychodermist’s
      implement for its associated implement school. A trophy that fits multiple
      implement categories, such as a hand with clawed fingers, can function for
      only one implement school at a time. A trophy can be integrated into
      another item, such as an ornament or a magic item, but otherwise does not
      take up a magic item slot, even when worn. A psychodermist can replace
      trophies with new ones harvested from slain foes, although doing so causes
      the old trophies to rot at their normal rate. In order to craft a trophy,
      the psychodermist must have been present during the creature’s
      death.</p><p>This ability alters implements.</p><p>Monster Hunting Lore
      (Ex): At 2nd level, a psychodermist gains a bonus equal to half his
      occultist level on skill checks made to craft trophies and Knowledge
      checks made to identify the abilities and weaknesses of
      creatures.</p><p>This ability replaces magic item
      skill.</p><p><strong>Discern Death (Su)</strong>: At 2nd level, as a
      full-round action that provokes attacks of opportunity, a psychodermist
      can glean information from a creature’s corpse. The psychodermist must
      study at least one drop of the creature’s blood, a small portion of its
      flesh, or a fragment of one of its bones as a part of this action. This
      ability otherwise functions as blood biographyAPG, using the
      psychodermist’s occultist level as his caster level. The psychodermist can
      use this ability once per day at 2nd level, plus one additional time per
      day for every 3 levels beyond 2nd.</p><p>This ability replaces object
      reading.</p><p><strong>Seek Prey (Su)</strong>: At 5th level, a
      psychodermist can supernaturally detect creatures around him as a standard
      action. This functions as the spell aura sightACG, except the duration is
      1 round and the psychodermist learns the locations and power of all auras
      of a specific creature type, similar to detect undead except the
      psychodermist can choose any creature type represented among the trophies
      he has selected for that day.</p><p>This ability replaces aura sight.<br
      /><br />Residual Hatred (Su): At 8th level, a psychodermist can tap into
      the psychic death throes of the monsters his trophies are derived from,
      increasing his efficiency at slaying similar beasts. The psychodermist can
      spend mental focus invested in a trophy to gain the ranger’s favored enemy
      class feature against the kind of creature from which the trophy was
      crafted. The favored enemy bonus applies against the specific creature
      variety, not the creature type; a psychodermist could use a red dragon
      scale trophy to gain favored enemy (red dragon), but not favored enemy
      (dragon). Each point of mental focus spent grants a cumulative +2 favored
      enemy bonus, with a maximum bonus equal to +2 for every 5 psychodermist
      levels he has. This benefit lasts until the psychodermist regains his
      mental focus.</p><p>This replaces outside contact.</p><p>Manifest
      Abilities (Su): At 12th level, a psychodermist can use the latent energy
      within his trophies to replicate his victims’ special abilities. As a
      standard action, he can spend 1 point of mental focus invested in a trophy
      to gain one of that creature’s special abilities that could be granted by
      the spell monstrous physique IIIUM. At 16th level, the psychodermist adds
      abilities offered by monstrous physique IVUM to the options he can select
      using this ability. The saving throw DC for these special abilities is
      equal to 10 + half the psychodermist’s level + his Intelligence
      modifier.</p><p>Alternatively, he can spend 1 point of mental focus as a
      standard action to add any two of the creature’s spell-like abilities to
      his list of spells known. To determine the spell’s level for this ability,
      use the level as it appears on the spell list for an occultist, wizard, or
      cleric, in that order; if it appears on none of those lists, use the
      spell’s highest spell level. For example, lesser restoration would be a
      2nd-level spell, despite appearing on the paladin’s 1st-level
      list.</p><p>Both uses of this ability last for 1 minute per psychodermist
      level. He can instead spend 2 points of mental focus to activate this
      ability as a move action, or 3 points to activate it as a swift
      action.</p><p>This replaces binding circles and fast
      circles.</p><h2>TROPHIES</h2><p>Psychodermists use the following monster
      parts for each implement school.</p><p><strong>Abjuration</strong>:
      Chitin, hides, scales, and other natural armor.<br />Conjuration:
      Gizzards, glands, hearts, livers, and other internal organs.<br
      />Divination: Ears, eyes, tongues, and other sensory organs.<br
      />Enchantment: Feathers, fur, hair, spines, and other decorative
      features.<br />Evocation: Claws, fangs, horns, and other natural
      weapons.<br />Illusion: Fingers, tendrils, toes, and other prehensile
      digits.<br />Necromancy: Bones, bile, and life-sustaining fluids, such as
      blood.<br />Transmutation: Feet, hands, paws, wings, and other appendages
      tied to locomotion.</p>
  sources:
    - id: PZO9478
      pages: "25"
  subType: misc
  tags:
    - Psychodermist
type: feat

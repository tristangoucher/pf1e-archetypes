_id: Kn7ffwQ2D0opBge7
_key: "!items!Kn7ffwQ2D0opBge7"
_stats:
  coreVersion: "12.331"
folder: zXPk8C2HCecMozfy
img: systems/pf1/icons/spells/leaf-jade-3.jpg
name: Druid (Planar Extremist)
system:
  associations:
    classes:
      - Druid
  description:
    value: >-
      <p>Through determined interest or repeated exposure to those places where
      the borders between planes are weaker, some druids lose their neutral
      stance and find themselves gravitating toward one of the four most extreme
      alignments: chaotic evil, chaotic good, lawful evil, or lawful good. While
      no longer able to harness the forces of nature like their neutral kin,
      these planar extremists find that their powers have shifted to reflect the
      Outer Plane most closely associated with their alignment. This is an
      ex-class archetype and can be taken by a character immediately upon
      becoming an ex-druid. </p><p>EX-CLASS ARCHETYPES</p><p>The following
      archetype can be taken by an ex-druid immediately upon becoming an
      ex-druid, regardless of character level, replacing some or all of the lost
      class abilities. If another archetype the character had before she became
      an ex-druid replaces the same ability as the ex-class archetype, she loses
      the old archetype in favor of the new one; otherwise, she can retain both
      archetypes as normal. Planar extremixts can gain further levels in the
      druid class, even though becoming an ex-druid normally prohibits further
      advancement in the class. While an ex-member of a class can recant her
      failings and atone for her fall from her original class (typically
      involving an atonement spell), her acceptance of her ex-class archetype
      means she must atone both for her initial fall and for further straying
      from the path. As a result, such a character must be the target of two
      atonement spells or a similar effect to regain her lost class features.
      Upon doing so, she immediately loses this archetype and regains her
      original class (and archetype, if she had
      one).</p><p><strong>Alignment</strong>: A planar extremist must have an
      alignment of chaotic evil, chaotic good, lawful evil, or lawful
      good.</p><p>This alters the druid’s alignment.</p><p><strong>Aura
      (Ex)</strong>: The planar extremist radiates an aura matching her
      alignment as if she were a cleric of her druid
      level.</p><p><strong>Spells</strong>: A planar extremist gains one fewer
      spell slot per spell level than normal in which to prepare spells. The
      planar extremist removes all summon nature’s ally spells from her spell
      list and replaces them with the summon monster spells of the same levels.
      The druid can otherwise cast spells as normal for a druid of her
      level.</p><p>This alters the druid’s spells.</p><p><strong>Planar
      Bond</strong>: A planar extremist forms a bond with a manifestation of the
      Outer Plane with which she is aligned. This bond can take one of two
      forms. The first is a close tie to the plane to which she is aligned,
      granting the planar extremist one of the domains of her alignment (for
      example, a lawful good planar extremist could take either the Lawful or
      Good domain). This option otherwise functions as a druid’s nature bond if
      she chose a close tie to the natural world.</p><p>The second option is to
      form a close bond with an outsider from an Outer Plane. The abilities of
      this outsider companion are determined using the rules for eidolons for
      the unchained summonerPU class, as if the planar extremist were a summoner
      of her druid level, except the outsider companion gains no additional
      evolution pool (only the evolutions from its base form and base evolutions
      for its subtype), and it must be of a subtype whose alignment exactly
      matches the alignment of the planar extremist. The planar extremist can
      summon her outsider companion with the same 1-minute ritual a summoner
      normally uses to do so, but she can’t cast summon monster spells if she
      currently has her outsider companion summoned, and she can’t summon her
      companion if she already has a creature summoned through other
      means.</p><p>As the planar ally gains class levels, her eidolon’s base
      statistics and base evolutions increase as if her druid level were her
      summoner level. The eidolon gains the darkvision, link, share spells,
      evasion, ability score increase, devotion, multiattack, and improved
      evasion abilities at the appropriate levels, but never gains an evolution
      pool. Abilities and spells that grant additional evolution points to
      eidolons do not function for her outsider companion, though any abilities
      that would grant evolution points to an animal companion do work. The
      planar extremist does not gain life link or any other class features a
      summoner gains in relation to her eidolon.</p><p>This replaces nature’s
      bond.</p><p><strong>Spontaneous Casting</strong>: A planar extremist can
      channel stored spell energy into summoning spells that she hasn’t prepared
      ahead of time. She can lose a prepared spell in order to cast a summon
      monster spell of the same level or lower, but only to summon creatures
      whose alignment matches hers.</p><p>This alters a druid’s spontaneous
      casting.</p><p><strong>Resist the Opposite (Ex)</strong>: At 4th level, a
      planar extremist gains a +2 bonus on saving throws against the spell-like
      and supernatural abilities of creatures whose alignment is diametrically
      opposed to her own.</p><p>This replaces resist nature’s
      lure.</p><p><strong>Planar Aspect (Su)</strong>: At 4th level, as a
      standard action, a planar extremist can gain the benefits of the
      bloodrager bloodline associated with her alignment (choosing from Abyssal,
      Celestial, or Infernal), as if she were a bloodrager of her druid level.
      She can gain these benefits for 1 minute per druid level as if she were
      bloodraging (but she gains no other benefits or penalties of bloodrage) or
      until she dismisses it as a swift action; this duration need not be used
      consecutively but must be spent in 1-minute increments. She can use this
      ability an additional time per day at 6th level and every 4 levels
      thereafter, for a total of five times per day at 18th level. At 20th
      level, a planar extremist can use planar aspect at will.</p><p>This
      replaces wild shape.</p>
  sources:
    - id: PZO9484
      pages: "13"
  subType: misc
  tags:
    - Planar Extremist
type: feat

_id: ixKscikRuhIk0qCs
_key: "!items!ixKscikRuhIk0qCs"
_stats:
  coreVersion: "12.331"
img: icons/tools/smithing/horseshoe-steel-blue.webp
name: Aides-de-Camp (Daring General)
system:
  associations:
    classes:
      - Cavalier
  description:
    value: >-
      <p><strong>Level</strong>: 6</p><p>A daring general gains a modified
      version of the Leadership feat at 6th level, even if he doesn't meet the
      prerequisites. All followers that the daring general gains as a result of
      this version of the Leadership feat can only have levels in the warrior
      class. All cohorts that the daring general gains from this ability must
      have levels only in classes that grant a full base attack bonus (such as
      barbarian, fighter, paladin, or ranger), and must be at least 4 levels
      lower than the daring general himself. However, at 12th level and 18th
      level, the daring general gains an additional cohort (up to a maximum of
      three cohorts at 18th level) and a number of additional followers equal to
      the number of followers he gained from the Leadership feat (up to a
      maximum of triple his normal number of followers at 18th level). When
      determining the kinds of cohorts the daring general can attract, he treats
      his Leadership score as 3 lower for each subsequent cohort (effectively
      taking a -3 penalty for the second cohort and a -6 penalty for the third
      cohort).</p><p>Additionally, the daring general can grant special roles to
      his cohorts, as outlined below. Unless otherwise stated, only one cohort
      can fill any given role at any given time. Designating a cohort as filling
      a special role requires a 24-hour ceremonial ritual.</p><p><i>Groom:
      </i>The groom cares for the daring general's mount. She does not take an
      armor check penalty on Ride checks while riding the cavalier's mount.
      Additionally, once per day, the groom can spend 10 minutes preparing the
      daring general's mount for battle. If she does, the mount's speed
      increases by 10 feet and its Dexterity score increases by 2. These
      benefits last for 1 hour.</p><p><i>Squire: </i>The squire's main task is
      to care for the daring general's arms and armor. If the squire aids the
      daring general in donning his armor, it takes half as much time to don as
      it would if someone else aided the daring general. Additionally, when the
      squire is adjacent to the daring general, as a standard action, she can
      grant a +2 bonus to the daring general's AC against the first attack made
      against the daring general before the squire's next turn. This counts as
      an application of the aid another action, but the squire does not need to
      threaten the attacker in question.</p><p><i>Standard Bearer: </i>The
      daring general's standard bearer can carry his banner in his stead,
      granting the benefits of his banner ability (and later, his greater banner
      ability) to all allies within 60 feet of the standard bearer. If the
      standard bearer becomes unconscious or dead, she ceases to grant these
      benefits.</p><p>Beyond the rules noted above, if the daring general uses
      his followers to create an army using the mass combat rules, that army
      starts with one extra tactic of the daring general's choice and a +2 bonus
      to its morale score.</p><p>The daring general can assign each of his
      followers individually to serve as either footmen or honor guards
      (described below). He does not need to assign either role to a given
      follower, if he prefers. He can change the role of a follower at any time,
      but it takes 1 day per level for the follower to change her tactics to
      suit her new role. Followers with a particular role must group together
      with other followers with the same role to form an army in mass
      combat.</p><p><i>Footman: </i>Each follower gains a +2 bonus on attack and
      damage rolls, but takes a -2 penalty to her AC and on saving throws. A
      footman unit gains a +2 bonus to its OM and takes a -2 penalty to its DV
      in mass combat.</p><p><i>Honor Guard: </i>The follower gains a +2 bonus to
      her AC and on saving throws, but takes a -2 penalty on attack and damage
      rolls. An honor guard unit gains a +2 bonus to its DV and takes a -2
      penalty to its OM in mass combat.</p>
  sources:
    - id: PZO1134
      pages: "24"
  subType: classFeat
  tags:
    - Daring General
type: feat

_id: Bdwi9Zcythp6TnPw
_key: "!items!Bdwi9Zcythp6TnPw"
_stats:
  coreVersion: "12.331"
img: icons/equipment/neck/amulet-carved-stone-cross.webp
name: False Arcanist (Hidden Priest)
system:
  associations:
    classes:
      - Cleric
  description:
    value: >-
      <p><strong>Level</strong>: 1</p><p>At 1st level, a hidden priest is able
      to disguise his cleric spellcasting, presenting it as arcane magic of some
      kind. Typically, this is as alchemist, bard, sorcerer, or wizard magic,
      and the cleric disguises the words and gestures of the cleric spell with
      accoutrements appropriate to his apparent profession. He must make a skill
      check (DC 10 + twice the level of the spell) to disguise his casting and
      successfully cast the spell. The type of skill check depends on the type
      of caster he pretends to be: Craft (alchemy) for alchemist, Perform for
      bard, Knowledge (arcana) for sorcerer or wizard. Using Spellcraft to
      identify the spell works normally, though unless the observer beats the DC
      by 10 or more, she doesn't suspect the source of the magic is
      divine.</p><p>For example, a hidden priest pretending to be an alchemist
      wants to cast cure light wounds on a wounded townsperson. If he makes a DC
      12 Craft (alchemy) check, he disguises his spellcasting as the mixing of
      an alchemical extract or potion (perhaps with the words disguised as
      reciting an obscure formula or talking herself through the list of
      ingredients), which he gives to the target. An observer making a DC 16
      Spellcraft check can identify his spell as cure light wounds, but doesn't
      realize his &#147;alchemical&#148; methods are a sham unless her check
      result is 26 or higher.</p><p>When the hidden priest uses this ability, he
      must still provide any divine focus components for the spells he casts.
      However, the divine focus doesn't need to be an obvious symbol of his
      faith. It could be a small coin, tattoo, or garment bearing the symbol,
      whether presented openly, disguised, or hidden within a larger picture.
      For example, a hidden priest of Sarenrae may use a coin with an ankh or
      sunburst, a complex tattoo or scar that has an ankh shape hidden within
      it, a glove with an ankh stitched on the inside of the palm, and so on. He
      must use this replacement divine focus just as he would his true one (for
      example, he couldn't leave the coin in his shoe). If a spell requires a
      divine focus with a specific or minimum cost, the replacement divine focus
      must be of similar value to be used as the divine focus.</p><p>A hidden
      priest adds half his class level (minimum +1) on all Bluff skill checks to
      send secret messages about religious matters, and on all Sense Motive
      checks to recognize similar messages. He also adds this bonus on
      Perception and Sense Motive checks relating to agents of the laws against
      his religion (including city guards in lands where these laws are in
      effect).</p>
  sources:
    - id: PZO9237
      pages: "35"
  subType: classFeat
  tags:
    - Hidden Priest
type: feat

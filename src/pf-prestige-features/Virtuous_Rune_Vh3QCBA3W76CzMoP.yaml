_id: Vh3QCBA3W76CzMoP
_key: "!items!Vh3QCBA3W76CzMoP"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/skills/yellow_22.jpg
name: Virtuous Rune
system:
  abilityType: su
  associations:
    classes:
      - Runeguard
  description:
    value: >-
      <p>At 1st level, a runeguard can master a secret method of using one of
      the seven runes of Thassilonian magic in a beneficial way to aid himself
      or others. He must choose one of the seven virtues when he gains this
      ability, but can choose another at 2nd, 4th, 5th, 6th, 8th, and 9th
      levels; by 9th level, he has mastered all seven of the secrets of virtuous
      runes. A runeguard can use any of the virtuous runes that he has mastered
      in any combination per day, but no more times per day than his runeguard
      level overall. Using a virtuous rune is a standard action (unless
      otherwise indicated in the text) and provokes an attack of
      opportunity.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.8IymQcv1EaPvTXzS]{Virtuous
      Rune: Charity}</p><p><strong>Charity</strong></p><p>A runeguard can use
      the rune of charity to transfer a single abjuration spell he has prepared
      or knows (if he's a spontaneous caster), along with the ability to cast
      it, to a willing creature by touch, as per imbue with spell ability, save
      that the recipient's Hit Dice do not limit options. The level of the spell
      being transferred can't exceed the runeguard's level - 1 (and thus a 1st
      level runeguard can only transfer a 0-level spells in this way). Once the
      spell is transferred, that spell slot remains unavailable to the runeguard
      until the creature that gained the spell casts it, at which point the
      runeguard regains access to the spell slot the next time he rests and
      prepares his magic.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.YyqOcI4IO627Qmbj]{Virtuous
      Rune: Kindness}</p><p><strong>Kindness</strong></p><p>When using the Heal
      skill to treat deadly wounds, the runeguard can call upon the rune of
      kindness to restore double the normal amount of hit points he otherwise
      would have healed by treating deadly wounds. He need not expend uses from
      a healer's kit when treating deadly wounds in this manner, and using this
      rune does not count against the total number of times a creature can
      benefit from heaving deadly wounds treated in a day.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.NKTj2J2kMxUwPKkQ]{Virtuous
      Rune: Generosity}</p><p><strong>Generosity</strong></p><p>When using the
      aid another action, the runeguard can draw upon the rune of generosity and
      expend a prepared spell or spell slot as an immediate action to grant an
      ally an insight bonus on attack rolls, on skill checks, or to Armor Class
      equal to the level of spell expended. The bonus persists for a number of
      rounds equal to the runeguard's class level.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.8qdAbOFRHVzYwb2h]{Virtuous
      Rune: Humility}</p><p><strong>Humility</strong></p><p>When the runeguard
      gains the benefits of the aid another action from an ally, he can expend a
      prepared spell or a spell slot (for a spontaneous caster) as an immediate
      action to gain an insight bonus on attack rolls, on skill checks, or to
      Armor Class equal to the level of the spell expended. This bonus lasts for
      a number of rounds equal to the runeguard's class level.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.JFeEmxRujCStrxOi]{Virtuous
      Rune: Love}</p><p><strong>Love</strong></p><p>By sacrificing a prepared
      spell or a spell slot (for a spontaneous caster), a runeguard can use the
      rune of love to form a close and powerful bond with a number of willing
      allies equal to the level of the spell or spell slot sacrificed. All
      allies to be affected must be within 30 feet at the time the rune is used.
      Until the next time the runeguard prepares spells (at which point he
      regains the sacrificed spell or spell slot), each ally can sense the
      general emotional state, health, and direction of any other ally bearing
      the rune (as per status), as long as they are on the same plane. If a
      spell slot of 5th level or higher is sacrificed, the link also grants a
      telepathic bond (as per the spell) that can't be dispelled. A character
      with the rune of love active gains a +2 bonus on all saving throws against
      charm and compulsion effects.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.2um4pRiXrc7WvJeK]{Virtuous
      Rune: Temperance}</p><p><strong>Temperance</strong></p><p>By invoking the
      rune of temperance, the runeguard becomes immune to the effects of
      negative energy for 1 round per runeguard level. The runeguard can use
      this ability as an immediate action to gain immunity to the effects of a
      single negative energy effect as that effect targets him, but doing so
      leaves the runeguard staggered on his next action.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.j7TWejJtqy0LiKFD]{Virtuous
      Rune: Zeal}</p><p><strong>Zeal</strong></p><p>By invoking the rune of zeal
      as a swift action, the runeguard bolsters his spellcasting for 1 round.
      During this time, the runeguard gains a bonus on all concentration checks
      equal to his runeguard level, and he can enhance any spell he casts that
      round with the effects of any metamagic feat he knows, provided the
      metamagic feat only uses up a spell slot 1 level higher than the spell's
      actual level.</p>
  sources:
    - id: PZO9474
      pages: "22"
  subType: classFeat
  tags:
    - Prestige Feature
type: feat

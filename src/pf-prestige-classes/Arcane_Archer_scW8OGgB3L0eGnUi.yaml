_id: scW8OGgB3L0eGnUi
_key: "!items!scW8OGgB3L0eGnUi"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/skills/weapon_05.jpg
name: Arcane Archer
system:
  bab: high
  classSkills:
    per: true
    rid: true
    ste: true
    sur: true
  description:
    value: >-
      <p>Many who seek to perfect the use of the bow sometimes pursue the path
      of the arcane archer. Arcane archers are masters of ranged combat, as they
      possess the ability to strike at targets with unerring accuracy and can
      imbue their arrows with powerful spells. Arrows fired by arcane archers
      fly at weird and uncanny angles to strike at foes around corners, and can
      pass through solid objects to hit enemies that cower behind such cover. At
      the height of their power, arcane archers can fell even the most powerful
      foes with a single, deadly shot. </p><p>Those who have trained as both
      rangers and wizards excel as arcane archers, although other multiclass
      combinations are not unheard of. Arcane archers may be found wherever
      elves travel, but not all are allies of the elves. Many, particularly
      half-elven arcane archers, use elven traditions solely for their own gain,
      or worse, against the elves whose very traditions they adhere to.
      </p><p><strong>Role</strong>: Arcane archers deal death from afar,
      winnowing down opponents while their allies rush into hand-to-hand combat.
      With their capacity to unleash hails of arrows on the enemy, they
      represent the pinnacle of ranged combat.
      </p><p><strong>Alignment</strong>: Arcane archers can be of any alignment.
      Elf or half-elf arcane arches tend to be free-spirited and are rarely
      lawful. Similarly, it is uncommon for elven arcane archers to be evil, and
      overall the path of the arcane archer is more often pursued by good or
      neutral characters.</p><h2>Requirements</h2><p>To qualify to become an
      arcane archer, a character must fulfill all the following criteria.
      </p><p><strong>Base Attack Bonus</strong>: +6. <br
      /><strong>Feats</strong>: Point-Blank Shot, Precise Shot, Weapon Focus
      (longbow or shortbow). <br /><strong>Spells</strong>: Ability to cast
      1st-level arcane spells.</p><h2>Class Skills</h2><p>The Arcane Archer's
      class skills are Perception (Wis), Ride (Dex), Stealth (Dex), and Survival
      (Wis).</p><p><strong>Skill Points at each Level</strong>: 4 + Int
      modifier.<br /><strong>Hit Die</strong>: d10.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:9.80518%"><strong>Level</strong></td><td
      style="width:12.7274%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.37989%"><strong>Fort Save</strong></td><td
      style="width:7.63501%"><strong>Ref Save</strong></td><td
      style="width:8.37989%"><strong>Will Save</strong></td><td
      style="width:29.7952%"><strong>Special</strong></td><td
      style="width:21.4153%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:9.80518%">1st</td><td style="width:12.7274%">+1</td><td
      style="width:8.37989%">+1</td><td style="width:7.63501%">+1</td><td
      style="width:8.37989%">+0</td><td style="width:29.7952%">Enhance arrows
      (magic)</td><td style="width:21.4153%">—</td></tr><tr><td
      style="width:9.80518%">2nd</td><td style="width:12.7274%">+2</td><td
      style="width:8.37989%">+1</td><td style="width:7.63501%">+1</td><td
      style="width:8.37989%">+1</td><td style="width:29.7952%">Imbue
      arrow</td><td style="width:21.4153%">+1 level of existing
      class</td></tr><tr><td style="width:9.80518%">3rd</td><td
      style="width:12.7274%">+3</td><td style="width:8.37989%">+2</td><td
      style="width:7.63501%">+2</td><td style="width:8.37989%">+1</td><td
      style="width:29.7952%">Enhance arrows (elemental)</td><td
      style="width:21.4153%">+1 level of existing class</td></tr><tr><td
      style="width:9.80518%">4th</td><td style="width:12.7274%">+4</td><td
      style="width:8.37989%">+2</td><td style="width:7.63501%">+2</td><td
      style="width:8.37989%">+1</td><td style="width:29.7952%">Seeker
      arrow</td><td style="width:21.4153%">+1 level of existing
      class</td></tr><tr><td style="width:9.80518%">5th</td><td
      style="width:12.7274%">+5</td><td style="width:8.37989%">+3</td><td
      style="width:7.63501%">+3</td><td style="width:8.37989%">+2</td><td
      style="width:29.7952%">Enhance arrows (<em>distance</em>)</td><td
      style="width:21.4153%">—</td></tr><tr><td
      style="width:9.80518%">6th</td><td style="width:12.7274%">+6</td><td
      style="width:8.37989%">+3</td><td style="width:7.63501%">+3</td><td
      style="width:8.37989%">+2</td><td style="width:29.7952%">Phase
      arrow</td><td style="width:21.4153%">+1 level of existing
      class</td></tr><tr><td style="width:9.80518%">7th</td><td
      style="width:12.7274%">+7</td><td style="width:8.37989%">+4</td><td
      style="width:7.63501%">+4</td><td style="width:8.37989%">+2</td><td
      style="width:29.7952%">Enhance arrows (elemental burst)</td><td
      style="width:21.4153%">+1 level of existing class</td></tr><tr><td
      style="width:9.80518%">8th</td><td style="width:12.7274%">+8</td><td
      style="width:8.37989%">+4</td><td style="width:7.63501%">+4</td><td
      style="width:8.37989%">+3</td><td style="width:29.7952%">Hail of
      arrows</td><td style="width:21.4153%">+1 level of existing
      class</td></tr><tr><td style="width:9.80518%">9th</td><td
      style="width:12.7274%">+9</td><td style="width:8.37989%">+5</td><td
      style="width:7.63501%">+5</td><td style="width:8.37989%">+3</td><td
      style="width:29.7952%">Enhance arrows (aligned)</td><td
      style="width:21.4153%">—</td></tr><tr><td
      style="width:9.80518%">10th</td><td style="width:12.7274%">+10</td><td
      style="width:8.37989%">+5</td><td style="width:7.63501%">+5</td><td
      style="width:8.37989%">+3</td><td style="width:29.7952%">Arrow of
      death</td><td style="width:21.4153%">+1 level of existing
      class</td></tr></tbody></table><p><br />All of the following are class
      features of the arcane archer prestige class. </p><p><strong>Weapon and
      Armor Proficiency</strong>: An arcane archer is proficient with all simple
      and martial weapons, light armor, medium armor, and shields.
      </p><p><strong>Spells per Day</strong>: At the indicated levels, an arcane
      archer gains new spells per day as if he had also gained a level in an
      arcane spellcasting class he belonged to before adding the prestige class.
      He does not, however, gain other benefits a character of that class would
      have gained, except for additional spells per day, spells known (if he is
      a spontaneous spellcaster), and an increased effective level of
      spellcasting. If a character had more than one arcane spellcasting class
      before becoming an arcane archer, he must decide to which class he adds
      the new level for purposes of determining spells per day.
      </p><p><strong>Enhance Arrows (Su)</strong>: At 1st level, every
      nonmagical arrow an arcane archer nocks and lets fly becomes magical,
      gaining a +1 enhancement bonus. Unlike magic weapons created by normal
      means, the archer need not spend gold pieces to accomplish this task.
      However, an archer's magic arrows only function for him. </p><p>In
      addition, the arcane archer's arrows gain a number of additional qualities
      as he gains additional levels. The elemental, elemental burst, and aligned
      qualities can be changed once per day, when the arcane archer prepares
      spells or, in the case of spontaneous spellcasters, after 8 hours of rest.
      </p><p>At 3rd level, every nonmagical arrow fired by an arcane archer
      gains one of the following elemental themed weapon qualities:
      <em>flaming</em>, <em>frost</em>, or <em>shock</em>. </p><p>At 5th level,
      every nonmagical arrow fired by an arcane archer gains the <em>distance
      </em>weapon quality. </p><p>At 7th level, every nonmagical arrow fired by
      an arcane archer gains one of the following elemental burst weapon
      qualities: <em>flaming burst</em>, <em>icy burst</em>, or <em>shocking
      burst</em>. This ability replaces the ability gained at 3rd level.
      </p><p>At 9th level, every nonmagical arrow fired by an arcane archer
      gains one of the following aligned weapon qualities: <em>anarchic</em>,
      <em>axiomatic</em>, <em>holy</em>, or <em>unholy</em>. The arcane archer
      cannot choose an ability that is the opposite of his alignment (for
      example, a lawful good arcane archer could not choose <em>anarchic </em>or
      <em>unholy </em>as his weapon quality). </p><p>The bonuses granted by a
      magic bow apply as normal to arrows that have been enhanced with this
      ability. Only the larger enhancement bonus applies. Duplicate abilities do
      not stack. </p><p><strong>Imbue Arrow (Sp)</strong>: At 2nd level, an
      arcane archer gains the ability to place an area spell upon an arrow. When
      the arrow is fired, the spell's area is centered where the arrow lands,
      even if the spell could normally be centered only on the caster. This
      ability allows the archer to use the bow's range rather than the spell's
      range. A spell cast in this way uses its standard casting time and the
      arcane archer can fire the arrow as part of the casting. The arrow must be
      fired during the round that the casting is completed or the spell is
      wasted. If the arrow misses, the spell is wasted. </p><p><strong>Seeker
      Arrow (Sp)</strong>: At 4th level, an arcane archer can launch an arrow at
      a target known to him within range, and the arrow travels to the target,
      even around corners. Only an unavoidable obstacle or the limit of the
      arrow's range prevents the arrow's flight. This ability negates cover and
      concealment modifiers, but otherwise the attack is rolled normally. Using
      this ability is a standard action (and shooting the arrow is part of the
      action). An arcane archer can use this ability once per day at 4th level,
      and one additional time per day for every two levels beyond 4th, to a
      maximum of four times per day at 10th level. </p><p><strong>Phase Arrow
      (Sp)</strong>: At 6th level, an arcane archer can launch an arrow once per
      day at a target known to him within range, and the arrow travels to the
      target in a straight path, passing through any nonmagical barrier or wall
      in its way. (Any magical barrier stops the arrow.) This ability negates
      cover, concealment, armor, and shield modifiers, but otherwise the attack
      is rolled normally. Using this ability is a standard action (and shooting
      the arrow is part of the action). An arcane archer can use this ability
      once per day at 6th level, and one additional time per day for every two
      levels beyond 6th, to a maximum of three times per day at 10th level.
      </p><p><strong>Hail of Arrows (Sp)</strong>: In lieu of his regular
      attacks, once per day an arcane archer of 8th level or higher can fire an
      arrow at each and every target within range, to a maximum of one target
      for every arcane archer level she has earned. Each attack uses the
      archer's primary attack bonus, and each enemy may only be targeted by a
      single arrow. </p><p><strong>Arrow of Death (Sp)</strong>: At 10th level,
      an arcane archer can create a special type of <em>slaying arrow </em>that
      forces the target, if damaged by the arrow's attack, to make a Fortitude
      save or be slain immediately. The DC of this save is equal to 20 + the
      arcane archer's Charisma modifier. It takes 1 day to make a <em>slaying
      arrow</em>, and the arrow only functions for the arcane archer who created
      it. The <em>slaying arrow </em>lasts no longer than 1 year, and the archer
      can only have one such arrow in existence at a time.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Enhance Arrows (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.5cIlmiCAF4jXnj21
      - level: 2
        name: Imbue Arrow (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.u3FaR5SwGd1JNy07
      - level: 4
        name: Seeker Arrow (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.zsX2th2lu3gLIt7P
      - level: 6
        name: Phase Arrow (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.8N5To17Cqhl4052m
      - level: 8
        name: Hail of Arrows (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.gEqzRu6WsZdVlFeK
      - level: 10
        name: Arrow of Death (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.873sM1TTcevPBm24
  savingThrows:
    fort:
      value: high
    ref:
      value: high
  skillsPerLevel: 4
  sources:
    - id: PZO1110
      pages: "374"
  subType: prestige
  tag: arcaneArcher
type: class

_id: glpKroAXK6VsteQc
_key: "!items!glpKroAXK6VsteQc"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/feats/nimble-moves.png
name: Darechaser
system:
  bab: high
  classSkills:
    acr: true
    clm: true
    esc: true
    han: true
    per: true
    prf: true
    rid: true
    swm: true
  description:
    value: >-
      <p>While Kurgess’s faithful thrive in sporting arenas, they understand
      that nature itself creates the greatest obstacle courses. The athletic
      fanatics known as darechasers seek out these untamed obstacles as
      challenges against which to test their strength. In order to continue
      improving, darechasers must often resort to besting their own records.
      Sometimes a darechaser will even bet against herself, only to strain to
      prove herself wrong and amaze bystanders. Darechasers travel widely and
      delight in both officiating competitions and performing physical feats
      that inspire a new generation of athleticism. Some even go so far as to
      use sports as a form of diplomacy, settling disputes with athletic
      competitions.</p><h2>Requirements</h2><p>To qualify to become a
      darechaser, a character must fulfill all of the following
      criteria.</p><p><strong>Base Attack Bonus</strong>: +5.<br
      /><strong>Alignment</strong>: Lawful good, neutral good, or chaotic
      good.<br /><strong>Deity</strong>: Must worship Kurgess.<br
      /><strong>Feats</strong>: Athletic.<br /><strong>Skills</strong>:
      Acrobatics 5 ranks, Climb 5 ranks, Swim 5 ranks.</p><h2>Class
      Skills</h2><p>The Darechaser's class skills are Acrobatics (Dex), Climb
      (Str), Escape Artist (Dex), Handle Animal (Cha), Perception (Wis), Perform
      (Cha), Ride (Dex), Sense Motive (Wis), and Swim (Str).</p><p><strong>Skill
      Points at each Level</strong>: 4 + Int modifier.<br /><strong>Hit
      Die</strong>: d10.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:8.07441%"><strong>Level</strong></td><td
      style="width:17.4377%"><strong>Base Attack Bonus</strong></td><td
      style="width:9.86965%"><strong>Fort Save</strong></td><td
      style="width:8.93855%"><strong>Ref Save</strong></td><td
      style="width:9.68343%"><strong>Will Save</strong></td><td
      style="width:45.6238%"><strong>Special</strong></td></tr><tr><td
      style="width:8.07441%">1st</td><td style="width:17.4377%">+1</td><td
      style="width:9.86965%">+1</td><td style="width:8.93855%">+1</td><td
      style="width:9.68343%">+0</td><td style="width:45.6238%">Adrenaline rush
      +1, dare +1d6</td></tr><tr><td style="width:8.07441%">2nd</td><td
      style="width:17.4377%">+2</td><td style="width:9.86965%">+1</td><td
      style="width:8.93855%">+1</td><td style="width:9.68343%">+1</td><td
      style="width:45.6238%">Adrenaline deed</td></tr><tr><td
      style="width:8.07441%">3rd</td><td style="width:17.4377%">+3</td><td
      style="width:9.86965%">+2</td><td style="width:8.93855%">+2</td><td
      style="width:9.68343%">+1</td><td style="width:45.6238%">Adrenaline rush
      +2</td></tr><tr><td style="width:8.07441%">4th</td><td
      style="width:17.4377%">+4</td><td style="width:9.86965%">+2</td><td
      style="width:8.93855%">+2</td><td style="width:9.68343%">+1</td><td
      style="width:45.6238%">Adrenaline deed</td></tr><tr><td
      style="width:8.07441%">5th</td><td style="width:17.4377%">+5</td><td
      style="width:9.86965%">+3</td><td style="width:8.93855%">+3</td><td
      style="width:9.68343%">+2</td><td style="width:45.6238%">Adrenaline rush
      +3, dare +1d8</td></tr><tr><td style="width:8.07441%">6th</td><td
      style="width:17.4377%">+6</td><td style="width:9.86965%">+3</td><td
      style="width:8.93855%">+3</td><td style="width:9.68343%">+2</td><td
      style="width:45.6238%">Adrenaline deed</td></tr><tr><td
      style="width:8.07441%">7th</td><td style="width:17.4377%">+7</td><td
      style="width:9.86965%">+4</td><td style="width:8.93855%">+4</td><td
      style="width:9.68343%">+2</td><td style="width:45.6238%">Adrenaline rush
      +4</td></tr><tr><td style="width:8.07441%">8th</td><td
      style="width:17.4377%">+8</td><td style="width:9.86965%">+4</td><td
      style="width:8.93855%">+4</td><td style="width:9.68343%">+3</td><td
      style="width:45.6238%">Adrenaline deed</td></tr><tr><td
      style="width:8.07441%">9th</td><td style="width:17.4377%">+9</td><td
      style="width:9.86965%">+5</td><td style="width:8.93855%">+5</td><td
      style="width:9.68343%">+3</td><td style="width:45.6238%">Adrenaline rush
      +5</td></tr><tr><td style="width:8.07441%">10th</td><td
      style="width:17.4377%">+10</td><td style="width:9.86965%">+5</td><td
      style="width:8.93855%">+5</td><td style="width:9.68343%">+3</td><td
      style="width:45.6238%">Adrenaline deed, dare +1d10, record
      breaker</td></tr></tbody></table><p><br />Below are the class features of
      the darechaser prestige class.</p><p><strong>Adrenaline Rush
      (Ex)</strong>: A darechaser can call upon her inner reserves of strength
      and speed as a free action to overcome otherwise insurmountable obstacles.
      A darechaser can harness this energy for a number of rounds per day equal
      to 4 + her Constitution modifier. At each level after 1st, she can use
      adrenaline rush for an additional 2 rounds. Temporary increases to
      Constitution do not allow her to use this ability for additional rounds
      per day. If she has the rage class feature, she can expend rounds of rage
      to activate and sustain her adrenaline rush (e.g. for making Constitution
      checks).</p><p>While experiencing an adrenaline rush, a darechaser gains a
      +1 bonus on Acrobatics, Climb, Constitution, Dexterity, Escape Artist,
      Ride, Strength, and Swim checks. This bonus increases by 1 at 3rd level
      and every 2 levels thereafter (maximum +5).</p><p>A darechaser can end her
      adrenaline rush as a free action and is fatigued afterward for a number of
      rounds equal to double the number of rounds she used this ability. A
      darechaser can’t begin an adrenaline rush while fatigued or exhausted. If
      she falls unconscious, she can choose to continue her adrenaline
      rush.</p><p><strong>Dare (Ex)</strong>: As a swift action while using her
      adrenaline rush, a darechaser can expend a round of her adrenaline rush to
      dare herself to accomplish a great deed. Although many announce these
      dares aloud, the ability functions as long as she can coherently and
      mentally articulate her dare; she cannot use this ability while confused,
      dazed, stunned, or panicked. She can use it while using the rage class
      feature, but only if she loudly announces the dare (treat this as having
      audible components—it can be defeated by magical silence). Once she
      declares her dare, the darechaser can add 1d6 to the result of any one
      attack roll, saving throw, or other check that directly helps her fulfill
      the dare. If the result of the d6 roll is a natural 6, she can roll
      another 1d6 and add it to the check. She can continue to do this as long
      as she rolls natural 6s, up to a number of times equal to her Constitution
      modifier (minimum 1). At 5th level, she can instead add 1d8 to the result
      of one check to fulfill a dare, rolling an additional die only if she
      rolls a natural 8. At 10th level, she can instead add 1d10 to the result
      of one check, rolling an additional die only if she rolls a natural
      10.</p><p>A darechaser thrives on challenging herself to accomplish
      difficult goals, and she receives little satisfaction when fulfilling easy
      objectives. If her check succeeded only because of the additional die’s
      (or dice’s) result, she benefits from one of the following effects (chosen
      by her) immediately after fulfilling the dare. If she succeeds at the
      check despite the added die’s result, she gains no additional
      benefit.</p><p><em>Daring Endurance</em>: The darechaser immediately gains
      temporary hit points equal to 1d8 + her class level. These temporary hit
      points last for the duration of her adrenaline rush and stack with any
      temporary hit points granted by the unstoppable adrenaline
      deed.</p><p><em>Daring Finish</em>: The darechaser doesn’t become fatigued
      if she ends her adrenaline rush before the end of her next
      turn.</p><p><em>Daring Persistence</em>: The darechaser immediately
      regains 1d2 rounds of her adrenaline rush
      ability.</p><p><strong>Adrenaline Deed (Ex)</strong>: As a darechaser
      gains experience, she learns to leverage her adrenaline in new ways. At
      2nd level and every 2 levels thereafter, she gains an adrenaline deed from
      the following list that grants her additional benefits while she is using
      her adrenaline rush ability. She can’t choose an individual adrenaline
      deed more than once.</p><p><em>Dare-Driven</em>: If the darechaser adds
      1d8 to a roll as a part of a dare, she can roll again and add the result
      if she rolls a natural 7 or 8 (not just an 8). If she rolls 1d10, she
      instead rolls again on a result of a natural 9 or 10.</p><p><em>Diehard
      Performer</em>: The darechaser adds her adrenaline rush bonus on all
      Perform checks and performance combat checks (<em>Pathfinder RPG Ultimate
      Combat 153</em>). </p><p><em>Impossible Speed</em>: The darechaser’s base
      speed increases by 10 feet. At 6th level, her speed instead increases by
      20 feet, and at 10th level it increases by 30 feet.</p><p><em>Mounted
      Adrenaline</em>: The darechaser shares the base effects of her adrenaline
      surge (but not any adrenaline deeds) with her mount, as long as the
      darechaser is mounted on it or adjacent to it.</p><p><em>Powerful
      Climber</em>: The darechaser gains a natural climb speed equal to her land
      speed.</p><p><em>Powerful Leaper</em>: The darechaser always counts as
      having a running start when jumping, and she triples her adrenaline rush
      bonus on Acrobatics checks to jump. Powerful Swimmer: The darechaser gains
      a natural swim speed equal to her land speed.</p><p><em>Unstoppable</em>:
      When the darechaser begins her adrenaline rush, she gains a number of
      temporary hit points equal to 2 × her class level. These temporary hit
      points last for the duration of her adrenaline rush. In addition, she
      doubles her adrenaline rush bonus on Constitution checks to stabilize
      while dying.</p><p><strong>Untouchable</strong>: The darechaser gains a +1
      dodge bonus to her Armor Class. At 6th level this increases to +2, and at
      10th level it increases to +3.</p><p><strong>Record Breaker (Ex)</strong>:
      At 10th level, a darechaser can push herself to surpass known mortal
      limits. Once per day, she can use her dare ability to add 2 × the normal
      number of bonus dice to a d20 roll. If she applies this benefit to a check
      modified by her adrenaline rush, she instead rolls and adds 3 × the number
      of bonus dice.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Adrenaline Rush (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.sYpUn32u8UCvhGAb
      - level: 1
        name: Dare (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.4hkvJKRAMWZf51V9
      - level: 1
        name: Daring Endurance
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.qRpZW1yfWTOSn5vz
      - level: 1
        name: Daring Finish
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.G3oZrlX9em3ZSXpx
      - level: 1
        name: Daring Persistence
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.aCrUAbmAwonehlWI
      - level: 2
        name: Adrenaline Deed (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.qz3sBtaFF0ykzxFG
      - level: 10
        name: Record Breaker (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.9WMAeWlP2sDVc3Cg
  savingThrows:
    fort:
      value: high
    ref:
      value: high
  skillsPerLevel: 4
  sources:
    - id: PZO9474
      pages: "10"
  subType: prestige
  tag: darechaser
type: class

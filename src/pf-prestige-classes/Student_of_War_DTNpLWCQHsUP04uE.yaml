_id: DTNpLWCQHsUP04uE
_key: "!items!DTNpLWCQHsUP04uE"
_stats:
  coreVersion: "12.331"
img: icons/weapons/polearms/halberd-crescent-glowing.webp
name: Student of War
system:
  bab: high
  classSkills:
    clm: true
    crf: true
    dev: true
    han: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    lin: true
    per: true
    pro: true
    spl: true
    sur: true
    swm: true
  description:
    value: >-
      <p>To hear most warriors talk, battles are won by sharp iron and mighty
      thews. Yet the student of war knows that the key to victory is the mind
      behind the mettle, the training that guides the blade, and the discernment
      of when and where to strike. Any dedicated martial scholar can join this
      prestige class, but the Pathfinder Society draws the lion’s share. The
      Society’s libraries are a treasure trove to the aspiring student of war,
      home to obscure combat manuals, moldy bestiaries, and detailed histories
      of battle. Armed with these texts and hardened by constant drilling, the
      student of war fills her repertoire with tricks and techniques designed to
      exploit every weakness and negate every advantage of her foes.</p><p>The
      path of the student of war is particularly enticing to martially minded
      characters who seek to enhance and expand their mastery of skills.
      Students of war often pride themselves in studying topics that
      combat-focused types might not normally find value in, and their ability
      to draw upon unexpected lore and information has become something of a
      legend among their kind. This might be demonstrated by a heavily armored
      fighter’s training in Acrobatics allowing her to outmaneuver an overly
      confident swashbuckler, a rugged barbarian masterfully manipulating a
      magic wand via Use Magic Device, or similar unexpected
      techniques.</p><h2>Requirements</h2><p>To qualify to become a student of
      war, a character must fulfill the following criteria. <br /><strong>Base
      Attack Bonus</strong>: +5. <br /><strong>Feats</strong>: Combat Expertise,
      Dodge, Skill Focus (any one Knowledge skill). <br
      /><strong>Proficiency</strong>: Must be proficient with two martial
      weapons. <br /><strong>Skills</strong>: Knowledge (any two) 4 ranks in
      each. <br /><strong>Special</strong>: Must have succeeded at Knowledge
      checks against five distinct creatures prior to defeating
      them.</p><h2>Class Skills</h2><p>The Student of War's class skills are
      Climb (Str), Craft (Int), Disable Device (Dex), Handle Animal (Cha),
      Knowledge (all) (Int), Linguistics (Int), Perception (Wis), Profession
      (Wis), Sense Motive (Wis), Spellcraft (Int), Survival (Wis), and Swim
      (Str).</p><p><strong>Skill Points at each Level</strong>: 6 + Int
      modifier.<br /><strong>Hit Die</strong>: d10.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.65134%"><p><strong>Level</strong></p></td><td
      style="width:16.8608%"><p><strong>Base Attack Bonus</strong></p></td><td
      style="width:9.86965%"><p><strong>Fort Save</strong></p></td><td
      style="width:8.93855%"><p><strong>Ref Save</strong></p></td><td
      style="width:9.68343%"><p><strong>Will Save</strong></p></td><td
      style="width:45.6238%"><p><strong>Special</strong></p></td></tr><tr><td
      style="width:8.65134%"><p>1st</p></td><td
      style="width:16.8608%"><p>+1</p></td><td
      style="width:9.86965%"><p>+0</p></td><td
      style="width:8.93855%"><p>+0</p></td><td
      style="width:9.68343%"><p>+1</p></td><td
      style="width:45.6238%"><p>Additional skill, know your enemy +1
      (move)</p></td></tr><tr><td style="width:8.65134%"><p>2nd</p></td><td
      style="width:16.8608%"><p>+2</p></td><td
      style="width:9.86965%"><p>+1</p></td><td
      style="width:8.93855%"><p>+1</p></td><td
      style="width:9.68343%"><p>+1</p></td><td style="width:45.6238%"><p>Combat
      feat, mind over metal</p></td></tr><tr><td
      style="width:8.65134%"><p>3rd</p></td><td
      style="width:16.8608%"><p>+3</p></td><td
      style="width:9.86965%"><p>+1</p></td><td
      style="width:8.93855%"><p>+1</p></td><td
      style="width:9.68343%"><p>+2</p></td><td
      style="width:45.6238%"><p>Additional skill, anticipate
      1/day</p></td></tr><tr><td style="width:8.65134%"><p>4th</p></td><td
      style="width:16.8608%"><p>+4</p></td><td
      style="width:9.86965%"><p>+1</p></td><td
      style="width:8.93855%"><p>+1</p></td><td
      style="width:9.68343%"><p>+2</p></td><td style="width:45.6238%"><p>Know
      your enemy +2</p></td></tr><tr><td
      style="width:8.65134%"><p>5th</p></td><td
      style="width:16.8608%"><p>+5</p></td><td
      style="width:9.86965%"><p>+2</p></td><td
      style="width:8.93855%"><p>+2</p></td><td
      style="width:9.68343%"><p>+3</p></td><td
      style="width:45.6238%"><p>Additional skill, combat
      feat</p></td></tr><tr><td style="width:8.65134%"><p>6th</p></td><td
      style="width:16.8608%"><p>+6</p></td><td
      style="width:9.86965%"><p>+2</p></td><td
      style="width:8.93855%"><p>+2</p></td><td
      style="width:9.68343%"><p>+3</p></td><td
      style="width:45.6238%"><p>Anticipate 2/day, telling
      blow</p></td></tr><tr><td style="width:8.65134%"><p>7th</p></td><td
      style="width:16.8608%"><p>+7</p></td><td
      style="width:9.86965%"><p>+2</p></td><td
      style="width:8.93855%"><p>+2</p></td><td
      style="width:9.68343%"><p>+4</p></td><td
      style="width:45.6238%"><p>Additional skill, know your enemy +3
      (swift)</p></td></tr><tr><td style="width:8.65134%"><p>8th</p></td><td
      style="width:16.8608%"><p>+8</p></td><td
      style="width:9.86965%"><p>+3</p></td><td
      style="width:8.93855%"><p>+3</p></td><td
      style="width:9.68343%"><p>+4</p></td><td style="width:45.6238%"><p>Combat
      feat</p></td></tr><tr><td style="width:8.65134%"><p>9th</p></td><td
      style="width:16.8608%"><p>+9</p></td><td
      style="width:9.86965%"><p>+3</p></td><td
      style="width:8.93855%"><p>+3</p></td><td
      style="width:9.68343%"><p>+5</p></td><td
      style="width:45.6238%"><p>Additional skill, anticipate 3/day,
      nemesis</p></td></tr><tr><td style="width:8.65134%"><p>10th</p></td><td
      style="width:16.8608%"><p>+10</p></td><td
      style="width:9.86965%"><p>+3</p></td><td
      style="width:8.93855%"><p>+3</p></td><td
      style="width:9.68343%"><p>+5</p></td><td style="width:45.6238%"><p>Deadly
      blow</p></td></tr></tbody></table><p><br />The following are class
      features of the student of war prestige class.</p><p><strong>Additional
      Skill</strong>: At 1st level and every 2 levels thereafter, a student of
      war gains a new class skill of her choice.</p><p><strong>Know Your Enemy
      (Ex)</strong>: As a move action, a student of war can study a foe she can
      see and attempt a Knowledge check appropriate to the creature’s type (DC =
      10 + the target’s HD). Success grants her a +1 insight bonus against her
      enemy, which can be applied via one of the following stances (chosen when
      the check is attempted) to the indicated statistics and
      rolls.</p><p><em>Defensive Stance</em>: The bonus applies to Armor Class
      and on saving throws against the target’s attacks. At 6th level, the
      student of war is treated as having the Mobility feat when provoking
      attacks of opportunity from the studied foe. If she already has Mobility,
      the bonus she gains to her AC in this case increases to
      +6.</p><p><em>Martial Stance</em>: The bonus applies on attack and damage
      rolls against the target. At 4th level, the student of war is treated as
      having the Critical Focus feat for the purpose of attacks against the
      studied foe. If she already has Critical Focus, the bonus she gains when
      confirming her critical hits in this case is +6
      instead.</p><p><em>Tactical Stance</em>: The bonus applies on combat
      maneuver checks and to CMD when initiating or defending against bull rush,
      disarm, grapple, overrun, and trip combat maneuvers. At 8th level, the
      student of war no longer provokes attacks of opportunity from a studied
      foe when attempting to bull rush, disarm, grapple, overrun, or trip that
      target; this does not affect attacks of opportunity made by any creature
      other than the studied foe.</p><p>A student of war can change her stance
      as a move action. The bonus lasts for 1 minute per class level or until
      the character uses this ability on another target. The bonus increases to
      +2 at 4th level and +3 at 7th level. At 7th level, the student can use
      this ability as a swift action rather than a move
      action.</p><p><strong>Combat Feat</strong>: At 2nd, 5th, and 8th levels, a
      student of war gains a bonus combat feat.</p><p><strong>Mind Over Metal
      (Ex)</strong>: At 2nd level, when a student of war is using armor or a
      shield, she can use her Intelligence modifier in place of her Dexterity
      modifier for determining her Armor Class. The armor’s normal maximum
      Dexterity bonus still applies (limiting how much of the character’s
      Intelligence bonus she can apply to her AC).</p><p><strong>Anticipate
      (Ex)</strong>: At 3rd level, once per day as an immediate action, a
      student of war can ignore any damage and effects of a spell or ability she
      successfully saved against, such as the outlining effect of a
      <em>glitterdust</em> spell or the half damage from an <em>inflict serious
      wounds</em> spell. This ability has no effect against effects that do not
      allow saving throws (such as <em>darkness</em>, difficult terrain, etc.).
      This ability is usable one additional time per day for every 3 class
      levels beyond 3rd.</p><p><strong>Telling Blow (Ex)</strong>: At 6th level,
      a student of war can aim her blows at the weakest point in a studied foe’s
      defense, ignoring up to 5 points of damage reduction. This does not apply
      to damage reduction without a type (such as DR 10/—). This ability cannot
      be used against creatures that are immune to critical hits or otherwise
      lack discernible weak points. This ability stacks with the Penetrating
      Strike feat.</p><p><strong>Nemesis (Su)</strong>: At 9th level, once per
      day as a swift action, a student of war can focus on a weapon she holds
      and render it anathema to her studied foe. The weapon gains the bane
      special ability against the creature for 1 minute or until the student of
      war uses know your enemy against a different foe.</p><p><strong>Deadly
      Blow (Su)</strong>: At 10th level, a student of war can find weak spots
      where none should exist. A student of war who uses her know your enemy
      ability and exceeds the Knowledge check DC by 10 or more can ignore the
      target’s natural damage reduction and immunity to critical hits and sneak
      attacks. This does not apply to immunities granted by spells,
      environmental effects, or equipment.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Additional Skill
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.GVq3P0cSk7m8PVzE
      - level: 1
        name: Know Your Enemy (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.eiLHn0IZnfsM0DJc
      - level: 1
        name: Martial Stance
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.OiFHTMLTEu9jdEru
      - level: 1
        name: Tactical Stance
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.vBvrssRaFsLaONyy
      - level: 1
        name: Defensive Stance
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.e46u6sTgyqUTenni
      - level: 2
        name: Combat Feat
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.ZgR9VbwlkekF1qCR
      - level: 2
        name: Mind Over Metal (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.myMrIU9QyZYz9P7u
      - level: 3
        name: Anticipate (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.amXSU5iEz0GTRPYW
      - level: 6
        name: Telling Blow (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.aT6POJLz15UMCLvU
      - level: 9
        name: Nemesis (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.2RSWjRqweDYPGXWD
      - level: 10
        name: Deadly Blow (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.w56sdEVxgXA1mLzj
  savingThrows:
    will:
      value: high
  skillsPerLevel: 6
  sources:
    - id: PZO1138
      pages: "142"
    - id: PZO9211
      pages: "62"
  subType: prestige
  tag: studentOfWar
type: class

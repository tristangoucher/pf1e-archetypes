_id: 0BqObtiNldRybouC
_key: "!items!0BqObtiNldRybouC"
_stats:
  coreVersion: "12.331"
img: icons/sundries/survival/cuffs-shackles-steel.webp
name: Liberator
system:
  bab: high
  classSkills:
    clm: true
    esc: true
    int: true
    kdu: true
    klo: true
    sur: true
  description:
    value: >-
      <p><strong>Source</strong>: Second Darkness Player's Guide pg. 22<br
      />Liberators fight to free the wrongfully imprisoned&mdash;be they slaves,
      political prisoners, or princesses locked in towers. They rely on physical
      strength&mdash;but also quick wits&mdash;to overcome oppressors. Since
      charging blindly into battle could result in a prisoner&rsquo;s death,
      liberators plan their rescues carefully.</p><p>Liberators despise slavers
      above all others, particularly races well known for these practices, such
      as the gnolls. While some liberators operate solely in cities, most dwell
      in or near territories held by known slaver organizations or groups.
      Liberators typically strike under the cover of darkness, and they usually
      seek to smuggle slaves to safety without alerting any guards. As a result,
      liberators develop many abilities related to navigating darkness and
      stealth.</p><p>In large cities, the law typically views the actions of
      liberators as dangerous, since they often show no respect for traditional
      investigative procedures. These feelings of distrust and disdain are
      shared by most liberators, who tend to see the law as a lumbering,
      ineffective giant. Liberators often operate in the shadows of society as a
      result, lest they become prisoners themselves. Rarely, city officials view
      liberators as expensive but effective mercenaries who bring results the
      average guardsman cannot. Liberators often find work in such cities, and
      in return gain cautious but respectful accolades for their
      deeds.</p><h2>Requirements</h2><p>To qualify to become a liberator, a
      character must fulfill all the following
      criteria.</p><p><strong>Alignment</strong>: Any nonlawful.<br
      /><strong>Base Attack Bonus</strong>: +5.<br /><strong>Skills</strong>:
      Escape Artist 6 ranks, Open Lock 6 ranks.<br /><strong>Feats</strong>:
      Improved Sunder, Power Attack, Stealthy.</p><h2>Class Skills</h2><p>The
      Liberator's class skills are Climb, Escape Artist, Gather Information,
      Hide, Intimidate, Knowledge (dungeoneering), Knowledge (local), Open Lock,
      Search, Sense Motive, Survival, Use Rope.</p><p><strong>Skill Points at
      each Level</strong>: 2 + Int modifier.<br /><strong>Hit Die</strong>:
      d10.</p><h2>Class Features</h2><table class="inner" style="width:
      96.9314%;"><tbody><tr><td style="width:
      8.65134%;"><strong>Level</strong></td><td style="width:
      18.1643%;"><strong>Base Attack Bonus</strong></td><td style="width:
      10.6145%;"><strong>Fort Save</strong></td><td style="width:
      9.49721%;"><strong>Ref Save</strong></td><td style="width:
      10.4283%;"><strong>Will Save</strong></td><td style="width:
      42.2719%;"><strong>Special</strong></td></tr><tr><td style="width:
      8.65134%;">1st</td><td style="width: 18.1643%;">+1</td><td style="width:
      10.6145%;">+2</td><td style="width: 9.49721%;">+0</td><td style="width:
      10.4283%;">+0</td><td style="width: 42.2719%;">Poison resistance +2, to
      the rescue</td></tr><tr><td style="width: 8.65134%;">2nd</td><td
      style="width: 18.1643%;">+2</td><td style="width: 10.6145%;">+3</td><td
      style="width: 9.49721%;">+0</td><td style="width: 10.4283%;">+0</td><td
      style="width: 42.2719%;">Lockbreaker +1</td></tr><tr><td style="width:
      8.65134%;">3rd</td><td style="width: 18.1643%;">+3</td><td style="width:
      10.6145%;">+3</td><td style="width: 9.49721%;">+1</td><td style="width:
      10.4283%;">+1</td><td style="width: 42.2719%;">Bonus feat, silent
      sunder</td></tr><tr><td style="width: 8.65134%;">4th</td><td style="width:
      18.1643%;">+4</td><td style="width: 10.6145%;">+4</td><td style="width:
      9.49721%;">+1</td><td style="width: 10.4283%;">+1</td><td style="width:
      42.2719%;">Lockbreaker +2</td></tr><tr><td style="width:
      8.65134%;">5th</td><td style="width: 18.1643%;">+5</td><td style="width:
      10.6145%;">+4</td><td style="width: 9.49721%;">+1</td><td style="width:
      10.4283%;">+1</td><td style="width: 42.2719%;">Slaver's
      ruin</td></tr><tr><td style="width: 8.65134%;">6th</td><td style="width:
      18.1643%;">+6</td><td style="width: 10.6145%;">+5</td><td style="width:
      9.49721%;">+2</td><td style="width: 10.4283%;">+2</td><td style="width:
      42.2719%;">Greater sunder, lockbreaker +3</td></tr><tr><td style="width:
      8.65134%;">7th</td><td style="width: 18.1643%;">+7</td><td style="width:
      10.6145%;">+5</td><td style="width: 9.49721%;">+2</td><td style="width:
      10.4283%;">+2</td><td style="width: 42.2719%;">Poison resistance +4,
      steely resolve</td></tr><tr><td style="width: 8.65134%;">8th</td><td
      style="width: 18.1643%;">+8</td><td style="width: 10.6145%;">+6</td><td
      style="width: 9.49721%;">+2</td><td style="width: 10.4283%;">+2</td><td
      style="width: 42.2719%;">Darkvision, lockbreaker +4</td></tr><tr><td
      style="width: 8.65134%;">9th</td><td style="width: 18.1643%;">+9</td><td
      style="width: 10.6145%;">+6</td><td style="width: 9.49721%;">+3</td><td
      style="width: 10.4283%;">+3</td><td style="width: 42.2719%;">Quick
      pick</td></tr><tr><td style="width: 8.65134%;">10th</td><td style="width:
      18.1643%;">+10</td><td style="width: 10.6145%;">+7</td><td style="width:
      9.49721%;">+3</td><td style="width: 10.4283%;">+3</td><td style="width:
      42.2719%;">Freedom of movement, lockbreaker
      +5</td></tr></tbody></table><p><br />The following are class features of
      the liberator.</p><p><strong>To the Rescue (Su)</strong>: The sight of a
      liberator staging a daring rescue proves inspirational to his allies. As a
      standard action, the liberator can shout an encouraging phrase or raise a
      rallying cry to bring hope to those awaiting rescue. All allies within 30
      feet of the liberator gain a +1 morale bonus on attack rolls, a +4 morale
      bonus on Escape Artist checks, and a +4 morale bonus on saving throws made
      against fear effects. These bonuses persist for a number of rounds equal
      to 3 + the liberator&rsquo;s Charisma modifier (minimum 1
      round).</p><p><strong>Poison Resistance (Ex)</strong>: Slavers often make
      liberal use of poison and drugs to keep their captives lethargic and under
      control. A liberator purposefully exposes himself to a variety of venoms
      in order to build up a resistance to such toxins, should he ever fall
      victim to those he opposes. He gains a +2 bonus on saving throws made
      against poisons. This bonus increases to +4 at 7th
      level.</p><p><strong>Lockbreaker (Ex)</strong>: Starting at 2nd level, a
      liberator receives a competence bonus equal to half his class level on all
      Open Lock checks. He also gains this bonus on all Escape Artist checks
      made to escape bindings, shackles, and other
      restraints.</p><p><strong>Bonus Feat</strong>: At 3rd level, the liberator
      gains a bonus feat. He must qualify for the feat, and it must be from the
      following list: Blind-Fight, Iron Will, or Skill Focus (any class
      skill).</p><p><strong>Silent Sunder (Ex)</strong>: At 3rd level, a
      liberator learns methods of applying force and striking objects in such a
      way as to minimize noise. He can choose to make no more noise than people
      talking when making a sunder attack action or attempting to break an
      object with a Strength check.</p><p><strong>Slaver&rsquo;s Ruin
      (Ex)</strong>: The sight of imprisonment and slavery can drive a liberator
      into a righteous frenzy. A liberator of at least 5th level can designate
      one creature engaged in such acts as the target of his wrath. The
      designated foe is considered flat-footed against the liberator&rsquo;s
      next attack. Opponents who cannot be caught flat-footed (such as through
      improved uncanny dodge) are immune to this effect.</p><p><strong>Greater
      Sunder (Ex)</strong>: Beginning at 6th level, a liberator ignores half of
      an object&rsquo;s hardness when making a sunder attack or attacking an
      object. He also gains a +2 morale bonus on his opposed attack roll when
      making a sunder attempt (but not when defending against a sunder
      attempt).</p><p><strong>Steely Resolve (Ex)</strong>: A liberator fears
      magical domination more than anything else, for the enslavement of the
      mind is difficult to detect and remove. At 7th level, whenever a liberator
      is targeted by a charm or compulsion effect and fails his saving throw, he
      may attempt his saving throw again 1 round later at the same DC to resist
      the effect. He gets only this one extra chance to throw off any one
      effect.</p><p><strong>Darkvision (Su)</strong>: At 8th level, a liberator
      has spent so much time sneaking through the dark to free the victims of
      unjust imprisonment that he develops darkvision with a range of 60 feet.
      If the liberator already possesses darkvision, the range of his darkvision
      increases by 60 feet.</p><p><strong>Quick Pick (Ex)</strong>:
      There&rsquo;s not always time to properly pick a lock, and sometimes
      liberators have to improvise. At 9th level, a liberator has perfected the
      art of lockpicking to such an extent that he may attempt an Open Locks
      check as a move action instead of a full-round action without any
      additional penalties.</p><p><strong>Freedom of Movement (Su)</strong> At
      10th level, the liberator&rsquo;s focus against slavery and restriction to
      mobility develops into a potent resistance to restraint. He is now treated
      as if under the effects of a <em>freedom of movement</em> spell at all
      times. This ability cannot be dispelled. Once per day, he can also use
      <em>freedom of movement</em> as a spell-like ability (caster level equals
      his class level) to share this benefit with others.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Poison Resistance (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.eBMtTdKIROnXb5fF
      - level: 1
        name: To the Rescue (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.KCzCnIRL0kXH8Aaz
      - level: 2
        name: Lockbreaker (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.6QPYuJ1grudBotuu
      - level: 3
        name: Bonus Feat
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.j6PYH91fK22X3iCb
      - level: 3
        name: Silent Sunder (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.11hwygTuY1sILaeS
      - level: 4
        name: Slaver's Ruin (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.kjrYwoNq2MXNNhMo
      - level: 6
        name: Greater Sunder (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.umr9pzGSXWhyVxX3
      - level: 7
        name: Steely Resolve (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.MGHI15x1NEZIdFx0
      - level: 8
        name: Darkvision (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.9jCL5GcUBySFjSsE
      - level: 9
        name: Quick Pick (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.kurYn8UcuCkOKNbp
      - level: 10
        name: Freedom of Movement (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.TdL3rLVffSVvwegK
  savingThrows:
    fort:
      value: high
  skillsPerLevel: 2
  subType: prestige
  tag: liberator
type: class

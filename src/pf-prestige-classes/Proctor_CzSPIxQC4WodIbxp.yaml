_id: CzSPIxQC4WodIbxp
_key: "!items!CzSPIxQC4WodIbxp"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/feats/sickening-critical.jpg
name: Proctor
system:
  bab: med
  classSkills:
    crf: true
    dip: true
    int: true
    kpl: true
    kre: true
    per: true
    spl: true
  description:
    value: >-
      <p>The most ardent worshippers of the monitor demigods, called proctors,
      devote their lives to serving their divinities. Some proctors are
      wanderers, but most embed themselves in a community; experienced proctors
      are often highly respected for their even-handed wisdom as mediators or
      judges. In the case of proctors who revere the protean lords, these
      “communities” might consist of thieves or anarchists, but such proctors
      are no less respected for their expertise.</p><h2>Requirements</h2><p>To
      qualify to become a proctor, a character must fulfill all of the following
      criteria.</p><p><br /><strong>Alignment</strong>: Any neutral. <br
      /><strong>Feats</strong>: <a style="text-decoration:underline"
      href="https://www.aonprd.com/FeatDisplay.aspx?ItemName=Alertness">Alertness</a>,
      <a style="text-decoration:underline"
      href="https://www.aonprd.com/FeatDisplay.aspx?ItemName=Monitor%20Obedience">Monitor
      Obedience</a>. <br /><strong>Skills</strong>: Knowledge (planes) 7 ranks,
      Knowledge (religion) 7 ranks. <br /><strong>Spells</strong>: Ability to
      cast at least two abjuration spells of two different spell levels. <br
      /><strong>Special</strong>: Must worship a monitor demigod; must have met
      with a monitor of CR 5 or greater and declined an offer of assistance or
      power from a celestial or fiendish being of CR 10 or greater.</p><h2>Class
      Skills</h2><p>The Proctor's class skills are Diplomacy (Cha), Intimidate
      (Cha), Knowledge (planes) (Int), Knowledge (religion) (Int), Perception
      (Wis), Sense Motive (Wis), and Spellcraft (Int).</p><p><strong>Skill
      Points at each Level</strong>: 2 + Int modifier.<br /><strong>Hit
      Die</strong>: d8.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:6.92057%"><strong>Level</strong></td><td
      style="width:15.0534%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.00745%"><strong>Fort Save</strong></td><td
      style="width:7.26257%"><strong>Ref Save</strong></td><td
      style="width:8.00745%"><strong>Will Save</strong></td><td
      style="width:33.7058%"><strong>Special</strong></td><td
      style="width:20.6704%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:6.92057%">1st</td><td style="width:15.0534%">+0</td><td
      style="width:8.00745%">+1</td><td style="width:7.26257%">+0</td><td
      style="width:8.00745%">+0</td><td style="width:33.7058%">Monitor familiar,
      obedience, soultended</td><td style="width:20.6704%">—</td></tr><tr><td
      style="width:6.92057%">2nd</td><td style="width:15.0534%">+1</td><td
      style="width:8.00745%">+1</td><td style="width:7.26257%">+1</td><td
      style="width:8.00745%">+1</td><td style="width:33.7058%">Monitor
      expression</td><td style="width:20.6704%">+1 level of existing
      class</td></tr><tr><td style="width:6.92057%">3rd</td><td
      style="width:15.0534%">+2</td><td style="width:8.00745%">+2</td><td
      style="width:7.26257%">+1</td><td style="width:8.00745%">+1</td><td
      style="width:33.7058%">Monitor boon 1</td><td style="width:20.6704%">+1
      level of existing class</td></tr><tr><td
      style="width:6.92057%">4th</td><td style="width:15.0534%">+3</td><td
      style="width:8.00745%">+2</td><td style="width:7.26257%">+1</td><td
      style="width:8.00745%">+1</td><td style="width:33.7058%">Summon
      monitor</td><td style="width:20.6704%">+1 level of existing
      class</td></tr><tr><td style="width:6.92057%">5th</td><td
      style="width:15.0534%">+3</td><td style="width:8.00745%">+3</td><td
      style="width:7.26257%">+2</td><td style="width:8.00745%">+2</td><td
      style="width:33.7058%">Improved monitor expression</td><td
      style="width:20.6704%">+1 level of existing class</td></tr><tr><td
      style="width:6.92057%">6th</td><td style="width:15.0534%">+4</td><td
      style="width:8.00745%">+3</td><td style="width:7.26257%">+2</td><td
      style="width:8.00745%">+2</td><td style="width:33.7058%">Monitor boon
      2</td><td style="width:20.6704%">+1 level of existing
      class</td></tr><tr><td style="width:6.92057%">7th</td><td
      style="width:15.0534%">+5</td><td style="width:8.00745%">+4</td><td
      style="width:7.26257%">+2</td><td style="width:8.00745%">+2</td><td
      style="width:33.7058%">Solemn voice</td><td style="width:20.6704%">+1
      level of existing class</td></tr><tr><td
      style="width:6.92057%">8th</td><td style="width:15.0534%">+6</td><td
      style="width:8.00745%">+4</td><td style="width:7.26257%">+3</td><td
      style="width:8.00745%">+3</td><td style="width:33.7058%">Summon
      monitor</td><td style="width:20.6704%">+1 level of existing
      class</td></tr><tr><td style="width:6.92057%">9th</td><td
      style="width:15.0534%">+6</td><td style="width:8.00745%">+5</td><td
      style="width:7.26257%">+3</td><td style="width:8.00745%">+3</td><td
      style="width:33.7058%">Monitor boon 3</td><td style="width:20.6704%">+1
      level of existing class</td></tr><tr><td
      style="width:6.92057%">10th</td><td style="width:15.0534%">+7</td><td
      style="width:8.00745%">+5</td><td style="width:7.26257%">+3</td><td
      style="width:8.00745%">+3</td><td style="width:33.7058%">Expression
      immunities</td><td style="width:20.6704%">+1 level of existing
      class</td></tr></tbody></table><p><br />The following are class features
      of the proctor prestige class. </p><p><strong>Weapon and Armor
      Proficiency</strong>: A proctor gains proficiency with her monitor
      demigod's favored weapon. </p><p><strong>Spells per Day</strong>: At the
      indicated levels, a proctor gains new spells per day as if she had gained
      a level in a spellcasting class she belonged to before adding the prestige
      class. She does not gain any other benefits a character of that class
      would have gained except for additional spells per day, spells known (if
      she is a spontaneous spellcaster), and an increased effective level of
      spellcasting. If a character had more than one spellcasting class before
      becoming a proctor, she must decide which class she adds each new proctor
      level for the purpose of determining spells per day.
      </p><p><strong>Monitor Familiar</strong> (Ex): The proctor gains the
      services of an <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Arbiter">arbiter</a>,
      <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Nosoi">nosoi</a>,
      <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Paracletus">paracletus</a>,
      or <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Voidworm">voidworm</a>
      familiar, as appropriate for the proctor's monitor demigod. This functions
      as the <a
      href="https://www.aonprd.com/ClassDisplay.aspx?ItemName=Wizard">wizard's
      arcane bond class feature</a>, but as if the proctor had selected the <a
      href="https://www.aonprd.com/FeatDisplay.aspx?ItemName=Improved%20Familiar">Improved
      Familiar</a> feat. If the proctor has levels in another class that grants
      a familiar, levels in that class and the proctor class stack for
      determining the overall abilities of her familiar.
      </p><p><strong>Obedience (Ex)</strong>: In order to maintain the abilities
      granted by this prestige class (including all spellcasting abilities
      augmented by this prestige class), a proctor must perform a daily
      obedience to her monitor, as set forth in the description of each monitor
      demigod. </p><p><strong>Soultended (Ex)</strong>: Upon death, a proctor's
      soul appears on one of the neutral Outer Planes that corresponds to her
      monitor demigod and begins the gradual transformation into an <a
      href="https://www.aonprd.com/MonsterFamilies.aspx?ItemName=Aeon">aeon</a>,
      <a
      href="https://www.aonprd.com/MonsterFamilies.aspx?ItemName=Inevitable">inevitable</a>,
      <a
      href="https://www.aonprd.com/MonsterFamilies.aspx?ItemName=Protean">protean</a>,
      or <a
      href="https://www.aonprd.com/MonsterFamilies.aspx?ItemName=Psychopomp">psychopomp</a>.
      A character attempting to resurrect a slain proctor must succeed at a
      caster level check with a DC equal to 10 + the proctor's character level
      or the spell fails. That character cannot attempt to resurrect the proctor
      again until 24 hours have passed, though other characters can still
      attempt to do so. The comforting inevitability of this fate grants the
      proctor a +2 bonus on Will saves. </p><p><strong>Monitor Expression
      (Su)</strong>: At 2nd level, a proctor must select a specific expression
      of her role for her monitor demigod. Once made, this choice cannot be
      changed. Although a proctor's expression often aligns with her demigod,
      this isn't a strict requirement. Each expression provides a different
      benefit to the proctor.</p><p>Executors discharge their assigned duties in
      an efficient and orderly manner, and most often align with the primal
      inevitables. A proctor with this expression gains a +4 bonus on saving
      throws against disease, paralysis, poison, sleep, and stunning. At 5th
      level, this bonus also applies on saves against mind-affecting
      effects.</p><p>Fosters work to strengthen existing social relationships,
      and most often align with the psychopomp ushers. As a swift action, a
      proctor with this expression can gain the <a
      href="https://www.aonprd.com/MonsterSubtypes.aspx?ItemName=Psychopomp">spiritsense
      ability</a>, and she can end the effect as a free action. The foster can
      use this ability for a number of rounds per day equal to her proctor
      level. These rounds need not be consecutive. At 5th level, the duration
      increases to 1 minute, and the foster can use this ability for a number of
      minutes per day equal to her proctor level. These minutes need not be
      consecutive, but they must be used in 1-minute
      increments.</p><p>Harmonizers bring balance to opposing factions, and most
      often align with the Monad. As a swift action, a proctor with this
      expression can become semitangible and gain a +2 deflection bonus to AC.
      The harmonizer can use this ability for a number of rounds per day equal
      to her proctor level, and she can end the effect as a free action. These
      rounds need not be consecutive. At 5th level, this deflection bonus
      increases to +4.</p><p>Impulsives let their instincts guide them, and most
      often align with the protean lords. A proctor with this expression can
      cast <a
      href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=freedom%20of%20movement"><em>freedom
      of movement</em></a> once per day as a spell-like ability. At 5th level,
      the proctor can use this ability as a swift action. </p><p><strong>Monitor
      Boon</strong>: As a proctor gains levels, she gains boons from her monitor
      demigod. The nature of the boons varies depending on the proctor's chosen
      demigod. A monitor demigod grants three boons, each more powerful than the
      last. At 3rd level, the proctor gains the first boon. At 6th level, she
      gains the second boon, and at 9th level, she gains the third boon. Consult
      the <a
      href="https://www.aonprd.com/FeatDisplay.aspx?ItemName=Monitor%20Obedience">Monitor
      Obedience</a> feat and the monitor demigod descriptions for details on
      monitor boons.</p><p>When a monitor demigod grants a spell-like ability,
      the proctor's level for the spell-like ability is equal to her total
      character level. This ability allows a proctor to access these boons
      earlier than normal; it does not grant additional uses of the boons once
      the character reaches the necessary Hit Dice to earn the boons normally.
      </p><p><strong>Summon Monitor (Sp)</strong>: At 4th level, a proctor can
      cast <a
      href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=summon%20monster%206"><em>summon
      monster VI</em></a> once per day to conjure monitors based on her monitor
      expression. An executor can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=axiomite">Axiomite</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Mercane">mercanes</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Novenarut">novenaruts</a>.
      A foster can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Vanth">vanth</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Morbai">morbais</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Catrina">catrinas</a>.
      A harmonizer can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Theletos">theletos</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Othaos">othaoses</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Comozant%20Wyrd">comozant
      wyrds</a>. An impulsive can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Naunet">naunet</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Azuretzi">azuretzis</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Ourdivar">ourdivars</a>.<br
      /><br />At 8th level, the proctor also gains the ability to cast <a
      href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=summon%20monster%208"><em>summon
      monster VIII</em></a> once per day to conjure additional monitors of the
      same type as her monitor expression. An executor can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Kolyarut">kolyarut</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Zelekhut">zelekhuts</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Axiomite">axiomites</a>.
      A foster can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Algea">algea</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Shoki">shokis</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Vanth">vanths</a>.
      A harmonizer can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Akhana">akhana</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Jyoti">jyoti</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Theletos">theletoses</a>.
      An impulsive can summon one <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Ibshaunet">ibshaunet</a>,
      1d3 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Illureshi">illureshis</a>,
      or 1d4+1 <a
      href="https://www.aonprd.com/MonsterDisplay.aspx?ItemName=Naunet">naunets</a>.
      At the GM's discretion, specific monitor demigods may allow proctors who
      worship them to summon other neutral outsiders of equal power with this
      ability. </p><p><strong>Solemn Voice (Su)</strong>: At 7th level, the
      proctor gains an ability based on her monitor expression. Executors and
      impulsives can speak with any creature that has a language, as <a
      href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=tongues"><em>tongues</em></a>.
      A foster can cast <a
      href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=speak%20with%20dead"><em>speak
      with dead</em></a> as a spell-like ability at will. A harmonizer gains
      telepathy with a range of 30 feet but cannot communicate telepathically
      with creatures immune to mind-affecting effects. </p><p><strong>Expression
      Immunities (Su)</strong>: At 10th level, the proctor gains one or more
      immunities based on her monitor expression. Executors are immune to
      ability damage, ability drain, and energy drain. Fosters are immune to
      death effects, disease, and poison. Harmonizers are immune to critical
      hits. Impulsives are immune to transmutation effects, except those they
      willingly accept.</p>
  links:
    classAssociations:
      - level: 1
        name: Soultended (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.AyxrTS91cPId2UtF
      - level: 1
        name: Obedience (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.993fzdeYtVswiXIg
      - level: 1
        name: Monitor Familiar (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.ijRPs0Oz1n6cBq8a
      - level: 2
        name: Monitor Expression (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.nAf2hU0pfOBrOSAr
      - level: 3
        name: Monitor Boon
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.M767olPoQ97UpbpG
      - level: 4
        name: Summon Monitor (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.E1Kv3oxjl4ArG8xx
      - level: 7
        name: Solemn Voice (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.Dv5XQe1sP22H6yDz
      - level: 10
        name: Expression Immunities (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.tOD5oqXzBv2E8vTM
  savingThrows:
    fort:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO92114
      pages: "44"
  subType: prestige
  tag: proctor
type: class

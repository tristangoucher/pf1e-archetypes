_id: LeZSAkVvvU75ois8
_key: "!items!LeZSAkVvvU75ois8"
_stats:
  coreVersion: "12.331"
img: icons/sundries/books/book-backed-wood-tan.webp
name: Magaambyan Arcanist
system:
  classSkills:
    crf: true
    dip: true
    han: true
    hea: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    rid: true
    spl: true
    sur: true
  description:
    value: >-
      <p>A Magaambyan arcanist studies and follows traditions of magic
      originally discovered by Old-Mage Jatembe himself, and uses these arcane
      secrets to continue Jatembe’s quest to bring decency and integrity to
      civilization. Despite the name of the class, any arcane spellcaster can
      expand her powers via this prestige class, not only arcanists. Most
      Magaambyan arcanists consider their studies to be a sacred duty and a
      tradition that spans the ages. As a Magaambyan arcanist learns the deeper
      meanings locked within the academy’s wisdom, she discovers the true
      enlightened and philanthropic purpose of the Magaambya.</p><p>Perhaps the
      most unusual facet of the Magaambyan arcanists’ magical techniques is
      their method of interpreting and using magic of a druidic nature as arcane
      power. This unusual combination allows the Magaambyan arcanist incredible
      versatility in her studies, for the secrets of druidic magic are normally
      beyond the ken of arcane spellcasters. Yet the Magaambyan arcanist does
      not merely copy the magic of the natural world—she expands upon it. She
      simultaneously respects the traditions from which this magic stems while
      exploring it in ways that druids, who view their magic as a result of
      their faith, rarely consider. This method of converting druidic spells to
      arcane spells is taxing to the mind, however, and since each Magaambyan
      arcanist must create her own such translations, the school has not made
      arcane versions of druidic spells available to all arcane spellcasters. In
      fact, most students of the Magaambya are opposed to such an endeavor, for
      as intrigued as they are by this magic, they respect its traditions and do
      not wish to see these secrets made freely available to those who may not
      share their respect.</p><h2>Requirements</h2><p>To qualify to become a
      Magaambyan arcanist, a character must fulfill the following
      criteria.</p><p><strong>Alignment</strong>: Any good. <br
      /><strong>Feats</strong>: Scholar, Spell Mastery. <br
      /><strong>Skills</strong>: Spellcraft 5 ranks, Knowledge (arcana) 5 ranks,
      Knowledge (nature) 5 ranks. <br /><strong>Spells</strong>: Ability to
      prepare 3rd-level arcane spells.</p><h2>Class Skills</h2><p>The Magaambyan
      Arcanist's class skills are Diplomacy (Cha), Handle Animal (Cha), Heal
      (Wis), Knowledge (all) (Int), Ride (Dex), Spellcraft (Int), and Survival
      (Wis).</p><p><strong>Skill Points at each Level</strong>: 2 + Int
      modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.84365%"><p><strong>Level</strong></p></td><td
      style="width:10.5232%"><p><strong>Base Attack Bonus</strong></p></td><td
      style="width:6.89013%"><p><strong>Fort Save</strong></p></td><td
      style="width:6.14525%"><p><strong>Ref Save</strong></p></td><td
      style="width:6.70391%"><p><strong>Will Save</strong></p></td><td
      style="width:35.0093%"><p><strong>Special</strong></p></td><td
      style="width:25.5121%"><p><strong>Spells Per
      Day</strong></p></td></tr><tr><td
      style="width:8.84365%"><p>1st</p></td><td
      style="width:10.5232%"><p>+0</p></td><td
      style="width:6.89013%"><p>+0</p></td><td
      style="width:6.14525%"><p>+0</p></td><td
      style="width:6.70391%"><p>+1</p></td><td style="width:35.0093%"><p>Aura of
      good, halcyon magic</p></td><td style="width:25.5121%"><p>+1 level of
      arcane spellcasting class</p></td></tr><tr><td
      style="width:8.84365%"><p>2nd</p></td><td
      style="width:10.5232%"><p>+1</p></td><td
      style="width:6.89013%"><p>+1</p></td><td
      style="width:6.14525%"><p>+1</p></td><td
      style="width:6.70391%"><p>+1</p></td><td
      style="width:35.0093%"><p>Superior spell mastery, virtuous
      spells</p></td><td style="width:25.5121%"><p>+1 level of arcane
      spellcasting class</p></td></tr><tr><td
      style="width:8.84365%"><p>3rd</p></td><td
      style="width:10.5232%"><p>+1</p></td><td
      style="width:6.89013%"><p>+1</p></td><td
      style="width:6.14525%"><p>+1</p></td><td
      style="width:6.70391%"><p>+2</p></td><td style="width:35.0093%"><p>Intoned
      recollection (1 minute), lasting goodness</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr><tr><td style="width:8.84365%"><p>4th</p></td><td
      style="width:10.5232%"><p>+2</p></td><td
      style="width:6.89013%"><p>+1</p></td><td
      style="width:6.14525%"><p>+1</p></td><td
      style="width:6.70391%"><p>+2</p></td><td
      style="width:35.0093%"><p>Spontaneous spell mastery (1/day)</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr><tr><td style="width:8.84365%"><p>5th</p></td><td
      style="width:10.5232%"><p>+2</p></td><td
      style="width:6.89013%"><p>+2</p></td><td
      style="width:6.14525%"><p>+2</p></td><td
      style="width:6.70391%"><p>+3</p></td><td style="width:35.0093%"><p>Blessed
      warding</p></td><td style="width:25.5121%"><p>+1 level of arcane
      spellcasting class</p></td></tr><tr><td
      style="width:8.84365%"><p>6th</p></td><td
      style="width:10.5232%"><p>+3</p></td><td
      style="width:6.89013%"><p>+2</p></td><td
      style="width:6.14525%"><p>+2</p></td><td
      style="width:6.70391%"><p>+3</p></td><td
      style="width:35.0093%"><p>Immediate spell mastery</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr><tr><td style="width:8.84365%"><p>7th</p></td><td
      style="width:10.5232%"><p>+3</p></td><td
      style="width:6.89013%"><p>+2</p></td><td
      style="width:6.14525%"><p>+2</p></td><td
      style="width:6.70391%"><p>+4</p></td><td
      style="width:35.0093%"><p>Righteous contravention</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr><tr><td style="width:8.84365%"><p>8th</p></td><td
      style="width:10.5232%"><p>+4</p></td><td
      style="width:6.89013%"><p>+3</p></td><td
      style="width:6.14525%"><p>+3</p></td><td
      style="width:6.70391%"><p>+4</p></td><td
      style="width:35.0093%"><p>Spontaneous spell mastery (2/day)</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr><tr><td style="width:8.84365%"><p>9th</p></td><td
      style="width:10.5232%"><p>+4</p></td><td
      style="width:6.89013%"><p>+3</p></td><td
      style="width:6.14525%"><p>+3</p></td><td
      style="width:6.70391%"><p>+5</p></td><td style="width:35.0093%"><p>Holy
      arcana, intoned recollection (full round)</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr><tr><td style="width:8.84365%"><p>10th</p></td><td
      style="width:10.5232%"><p>+5</p></td><td
      style="width:6.89013%"><p>+3</p></td><td
      style="width:6.14525%"><p>+3</p></td><td
      style="width:6.70391%"><p>+5</p></td><td
      style="width:35.0093%"><p>Altruistic guardian, timeless body</p></td><td
      style="width:25.5121%"><p>+1 level of arcane spellcasting
      class</p></td></tr></tbody></table><p><br />The following are class
      features of the Magaambyan arcanist prestige class.</p><p><strong>Aura of
      Good (Ex)</strong>: A Magaambyan arcanist radiates an aura of good as if
      she were a cleric of a level equal to her class level (see the <em>detect
      good</em> spell).</p><p><strong>Halcyon Magic (Su)</strong>: At each class
      level, a Magaambyan arcanist chooses a spell from the druid spell list and
      treats it as if it were on the spell list of one of her arcane
      spellcasting classes. A Magaambyan arcanist must choose a druid spell at
      least 2 levels lower than the highest-level spell she can currently cast.
      The spell’s type becomes arcane and its save DC functions as normal for
      the arcane spellcasting class list she adds it to. The Magaambyan arcanist
      automatically learns this spell, adding it to her spellbook or familiar as
      appropriate for the class of the spell list to which she added the
      spell.</p><p><strong>Superior Spell Mastery (Ex)</strong>: At 2nd level,
      by spending a total of 24 hours studying over a maximum of 3 days, a
      Magaambyan arcanist can change the spells she has mastered with the Spell
      Mastery feat. She can choose a number of spells she knows up to her
      Intelligence modifier (which can have a maximum combined spell level equal
      to or less than her caster level) to be her Spell Mastery spells in place
      of the same number of spells she previously selected for her Spell Mastery
      feat.</p><p><strong>Virtuous Spells (Su)</strong>: At 2nd level, a
      Magaambyan arcanist casts spells with the good descriptor at +1 caster
      level. To prepare a spell with the evil descriptor, she must use two spell
      slots of that spell level.</p><p><strong>Intoned Recollection
      (Ex)</strong>: At 3rd level, by performing a special ritual lasting 1
      minute, a Magaambyan arcanist can prepare a spell into an open arcane
      spell slot. The open spell slot must have purposefully been left empty at
      the beginning of the day when the Magaambyan arcanist prepared her spells.
      She must be able to read her spellbook or have mastered the spell with the
      Spell Mastery feat to prepare it with this ability. She doesn’t need to
      rest for 8 hours beforehand, but all other requirements of the preparation
      process still apply. At 9th level, she can do this as a full-round action,
      provided she has her spellbook in hand or has mastered the spell with the
      Spell Mastery feat. She can use this ability a number of times per day
      equal to her Intelligence modifier.</p><p><strong> Lasting Goodness
      (Su)</strong>: At 3rd level, all of a Magaambyan arcanist’s spells with
      the good descriptor have their durations extended by a number of rounds
      equal to her class level. A spell with a duration of concentration,
      instantaneous, or permanent isn’t affected by this
      ability.</p><p><strong>Spontaneous Spell Mastery (Ex)</strong>: At 4th
      level, a Magaambyan arcanist can lose any prepared spell to cast a spell
      of the same spell level or lower that she has mastered with the Spell
      Mastery feat. She can use this ability once per day, plus one additional
      time per day at 8th level.</p><p><strong>Blessed Warding (Sp)</strong>: At
      5th level, a Magaambyan arcanist is under a constant <em>protection from
      evil</em> effect (CL equal to her class level). If this effect is
      dispelled, she can reactivate it as a swift
      action.</p><p><strong>Immediate Spell Mastery (Sp)</strong>: At 6th level,
      once per day, a Magaambyan arcanist can cast any spell she has mastered
      with the Spell Mastery feat, even if she hasn’t prepared it that day. This
      spell is treated like any other spell cast by the Magaambyan arcanist, but
      can’t be modified by metamagic feats or other
      abilities.</p><p><strong>Righteous Contravention (Su)</strong>: At 7th
      level, a Magaambyan arcanist gains a +2 bonus on checks to overcome the
      spell resistance of evil creatures or evil objects, and on checks to
      dispel spells cast by evil creatures, spells with the evil descriptor, or
      evil magic items.</p><p><strong>Holy Arcana (Ex)</strong>: At 9th level, a
      Magaambyan arcanist expands her skill at translating divine magic into
      arcane spells. She adds all the bonus spells from the Good cleric domain
      to the spell list of one of her arcane spellcasting classes. These spells’
      type becomes arcane and the save DCs function as normal for the arcane
      spellcasting class list she adds them to.</p><p><strong>Altruistic
      Guardian (Su)</strong>: At 10th level, once per day when a spell or
      supernatural effect that allows spell resistance targets a Magaambyan
      arcanist’s allies or if they are within its area of effect, as an
      immediate action she can redirect the effects to herself. She can do this
      for a number of allies up to her Intelligence modifier, provided they are
      within 30 feet of her. The Magaambyan arcanist must attempt the
      appropriate saving throws for each ally, possibly attempting multiple
      saving throws against the same spell or effect. She takes all damage and
      suffers all conditions and other effects that her allies would have taken,
      possibly taking these multiple times. This protects allies only from
      damage and conditions they’d take at the time she spends the immediate
      action, not from any they might later suffer from a noninstantaneous spell
      or effect.</p><p><strong>Timeless Body (Ex)</strong>: At 10th level, a
      Magaambyan arcanist no longer takes penalties for aging. This functions
      exactly like the druid ability of the same name.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - level: 1
        name: Aura of Good (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.g7dOocadCrEgbO2F
      - level: 1
        name: Halcyon Magic (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.pgzWRyEFgjMmGHeR
      - level: 2
        name: Superior Spell Mastery (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.C8eRVgcQeXy2QiBK
      - level: 2
        name: Virtuous Spells (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.ptj8fsunJerZutyJ
      - level: 3
        name: Intoned Recollection (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.y20pzrzm7hDwvwUt
      - level: 3
        name: Lasting Goodness (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.HQSJqAuGMUFB0Lyv
      - level: 4
        name: Spontaneous Spell Mastery (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.DaeYUsTsutpYMwIu
      - level: 5
        name: Blessed Warding (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.hHKdMIDQe6Sxb4mT
      - level: 6
        name: Immediate Spell Mastery (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.mKjkyUOwjU00vwSt
      - level: 7
        name: Righteous Contravention (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.iOYvcmxnuOA5h5H8
      - level: 9
        name: Holy Arcana (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.wbpZGlafOCpaLS3m
      - level: 10
        name: Altruistic Guardian (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.1gjCsX6yiP4vV6eC
      - level: 10
        name: Timeless Body (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.1QAyvkvu1svUPTSi
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO1138
      pages: "118"
    - id: PZO9249
      pages: "34"
  subType: prestige
  tag: magaambyanArcanist
type: class

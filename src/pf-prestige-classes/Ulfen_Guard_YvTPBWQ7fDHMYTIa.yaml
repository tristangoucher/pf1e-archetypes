_id: YvTPBWQ7fDHMYTIa
_key: "!items!YvTPBWQ7fDHMYTIa"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/feats/tower-shield-proficiency.jpg
name: Ulfen Guard
system:
  bab: high
  classSkills:
    dip: true
    kno: true
    per: true
  description:
    value: >-
      <p>After centuries of raiding and pillaging throughout Avistan, the Ulfen
      have earned their reputation for ferocity and fearlessness. Though
      seemingly at odds with this perception of barbarism, Ulfen are also known
      for their staunch loyalty, strong sense of honor, and unyielding resolve.
      The combination of these reputations is in large part why they have become
      desired as bodyguards throughout the region. Nowhere is this practice more
      pronounced than in Taldor, where Ulfen have become ensconced in the Taldan
      tradition and adopted as the elite bodyguards of Grand Prince Stavian III
      himself. Ulfen Guards, as outsiders, are generally thought to have no
      ulterior motives or secret loyalty to other Taldan families or political
      organizations; their honor keeps them loyal to the crown. An Ulfen Guard
      is required to be of at least half- Ulfen descent, with one or both
      parents hailing from the Lands of the Linnorm Kings. Non-Ulfen may serve
      the organization in a support capacity, but may not be Ulfen Guards
      themselves. While the Ulfen Guards are officially Stavian III’s personal
      bodyguards, he often has them perform other tasks that take them all over
      the Inner Sea region, protecting his interests as well as his person, or
      operating secretly under the auspices of traveling Ulfen merchants,
      raiding seafarers, or unaffiliated mercenaries, all the while serving the
      Taldan crown.</p><h2>Requirements</h2><p>To qualify to become an Ulfen
      Guard, a character must fulfill all of the following
      criteria.</p><p><strong>Base Attack Bonus</strong>: +5.<br
      /><strong>Skills</strong>: Knowledge (nobility) 2 ranks, Perception 5
      ranks, Sense Motive 3 ranks.<br /><strong>Special</strong>: Ulfen
      descent.</p><h2>Class Skills</h2><p>The Ulfen Guard's class skills are
      Diplomacy (Cha), Knowledge (nobility) (Int), Perception (Wis), Sense
      Motive (Wis).</p><p><strong>Skill Points at each Level</strong>: 4 + Int
      modifier.<br /><strong>Hit Die</strong>: d10.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.84365%"><strong>Level</strong></td><td
      style="width:18.9031%"><strong>Base Attack Bonus</strong></td><td
      style="width:10.987%"><strong>Fort Save</strong></td><td
      style="width:9.86965%"><strong>Ref Save</strong></td><td
      style="width:10.8007%"><strong>Will Save</strong></td><td
      style="width:40.2235%"><strong>Special</strong></td></tr><tr><td
      style="width:8.84365%">1st</td><td style="width:18.9031%">+1</td><td
      style="width:10.987%">+1</td><td style="width:9.86965%">+0</td><td
      style="width:10.8007%">+1</td><td style="width:40.2235%">Chosen ally,
      guard dedication, rage</td></tr><tr><td style="width:8.84365%">2nd</td><td
      style="width:18.9031%">+2</td><td style="width:10.987%">+1</td><td
      style="width:9.86965%">+1</td><td style="width:10.8007%">+1</td><td
      style="width:40.2235%">Guarded thoughts</td></tr><tr><td
      style="width:8.84365%">3rd</td><td style="width:18.9031%">+3</td><td
      style="width:10.987%">+2</td><td style="width:9.86965%">+1</td><td
      style="width:10.8007%">+2</td><td style="width:40.2235%">Guard
      dedication</td></tr><tr><td style="width:8.84365%">4th</td><td
      style="width:18.9031%">+4</td><td style="width:10.987%">+2</td><td
      style="width:9.86965%">+1</td><td style="width:10.8007%">+2</td><td
      style="width:40.2235%">Adaptable guardian</td></tr><tr><td
      style="width:8.84365%">5th</td><td style="width:18.9031%">+5</td><td
      style="width:10.987%">+3</td><td style="width:9.86965%">+2</td><td
      style="width:10.8007%">+3</td><td style="width:40.2235%">Guard
      dedication</td></tr><tr><td style="width:8.84365%">6th</td><td
      style="width:18.9031%">+6</td><td style="width:10.987%">+3</td><td
      style="width:9.86965%">+2</td><td style="width:10.8007%">+3</td><td
      style="width:40.2235%">Tight follower</td></tr><tr><td
      style="width:8.84365%">7th</td><td style="width:18.9031%">+7</td><td
      style="width:10.987%">+4</td><td style="width:9.86965%">+2</td><td
      style="width:10.8007%">+4</td><td style="width:40.2235%">Guard
      dedication</td></tr><tr><td style="width:8.84365%">8th</td><td
      style="width:18.9031%">+8</td><td style="width:10.987%">+4</td><td
      style="width:9.86965%">+3</td><td style="width:10.8007%">+4</td><td
      style="width:40.2235%">Reactive strike</td></tr><tr><td
      style="width:8.84365%">9th</td><td style="width:18.9031%">+9</td><td
      style="width:10.987%">+5</td><td style="width:9.86965%">+3</td><td
      style="width:10.8007%">+5</td><td style="width:40.2235%">Guard
      dedication</td></tr><tr><td style="width:8.84365%">10th</td><td
      style="width:18.9031%">+10</td><td style="width:10.987%">+5</td><td
      style="width:9.86965%">+3</td><td style="width:10.8007%">+5</td><td
      style="width:40.2235%">Unbreakable
      defender</td></tr></tbody></table><p><br />The following are class
      features of the Ulfen Guard prestige class.<br
      />@Compendium[pf1e-archetypes.pf-prestige-features.pgixjJZ2e5K2zd2E]{Chosen
      Ally (Ex)}<br /><strong>Chosen Ally (Ex)</strong>: At 1st level, when
      entering a rage, an Ulfen Guard can choose an ally to protect as a free
      action. The Ulfen Guard must make this choice when beginning her rage and
      cannot change her designated ally while raging. Whenever the Ulfen Guard
      is adjacent to her chosen ally, she can choose to take a –1 penalty to her
      AC to grant her ally a +1 dodge bonus to AC and Reflex saves. This bonus
      increases by 1 at 5th and 10th level.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.NKUVAqFlA93zmC4E]{Guard
      Dedications}<br /><strong>Guard Dedications</strong>: At 1st level, and at
      every 2 levels thereafter, an Ulfen Guard chooses a guard dedication from
      the list below. An Ulfen Guard cannot choose a guard dedication more than
      once unless the dedication specifies otherwise.</p><p><em>Alert Guardian
      (Ex)</em>: An Ulfen Guard with this dedication can enter a rage as an
      immediate action, and can do so even when flat-footed or during a surprise
      round in which she cannot otherwise act.</p><p><em>Clothed in Civilization
      (Ex)</em>: The Ulfen Guard gains proficiency in heavy armor. At 5th level,
      if the Ulfen Guard has the barbarian fast movement ability, she can use
      her fast movement ability while wearing heavy armor. This does not negate
      the penalties to movement imposed by the armor itself.</p><p><em>Curiosity
      (Ex)</em>: The Ulfen Guard’s peculiar nature makes her an object of
      intrigue. Civilized people are fascinated by the “tamed” barbarian, while
      more barbaric cultures are fascinated by her choice to turn her back on
      her heritage. The Ulfen Guard gains a bonus equal to her class level on
      Diplomacy checks.</p><p><em>Deflect Arrows (Ex)</em>: The Ulfen Guard
      gains Deflect Arrows as a bonus feat. If she is adjacent to her chosen
      ally, she can use this ability to deflect an arrow targeting her chosen
      ally. The Ulfen Guard does not need to meet the prerequisites for this
      bonus feat.</p><p><em>Formal Training (Ex)</em>: When selecting this
      dedication, the Ulfen Guard gains a bonus feat. She must select this feat
      from the list of fighter bonus feats. Additionally, the Ulfen Guard adds
      her level to any levels of fighter she has for the purpose of meeting the
      prerequisites for feats (if she has no fighter levels, treat her Ulfen
      guard levels as fighter levels). The Ulfen Guard must be at least 3rd
      level to select this dedication.</p><p><em>Greater Rage (Ex)</em>: The
      Ulfen Guard gains greater rage; this works as the barbarian ability of the
      same name. The Ulfen Guard must be at least 7th level to select this
      dedication.</p><p><em>Rage Power (Ex)</em>: The Ulfen Guard can select a
      barbarian rage power for which she qualifies, adding her Ulfen Guard level
      to her barbarian level to determine access. This guard dedication can be
      chosen up to three times.</p><p><em>Teamwork (Ex)</em>: Each time she
      begins her rage, the Ulfen Guard can select a teamwork feat (<em>Advanced
      Players Guide </em>150) that her chosen ally possesses; this must be a
      teamwork feat for which the Ulfen Guard otherwise qualifies. For the
      duration of her rage, the Ulfen Guard gains the benefits of that feat.
      This guard dedication can be chosen multiple times; each additional time
      allows the Ulfen Guard to select an additional teamwork feat at the start
      of her rage, thus granting her the use of more than one teamwork feat
      simultaneously.</p><p><em>Uncanny Dodge (Ex)</em>: The Ulfen Guard gains
      uncanny dodge, as the barbarian class feature of the same name. If the
      guard already has uncanny dodge from another source (or a previous
      instance of this dedication), she instead gains improved uncanny dodge. If
      she already has improved uncanny dodge, taking this dedication grants the
      effects of improved uncanny dodge to her chosen ally as long as the guard
      is adjacent to the ally. Use the guard’s class level plus her barbarian
      levels to determine the minimum rogue level required to flank either
      character. This guard dedication can be chosen up to three times.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.su8CRv3Khas8Mhm2]{Rage
      (Ex)}<br /><strong>Rage (Ex)</strong>: An Ulfen Guard is trained to
      harness her brutish nature while in combat in the defense of her charge.
      This works as the barbarian ability of the same name. An Ulfen Guard’s
      class levels stack with any other classes granting this ability for
      determining the effects of rage powers and the number of rounds per day
      the guard can rage.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.5GZUKheeuViri7GV]{Guarded
      Thoughts (Ex)}<br /><strong>Guarded Thoughts (Ex)</strong>: At 2nd level,
      if the Ulfen Guard fails a saving throw against a charm or compulsion
      effect, she immediately receives an additional save with a +5 circumstance
      bonus if the effect would compel her to attack her chosen ally. The guard
      can use this ability any number of times, but only once per effect.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.uQlns00xS6v3EWtk]{Adaptable
      Guardian (Ex)}<br /><strong>Adaptable Guardian (Ex)</strong>: At 4th
      level, the Ulfen Guard can change her chosen ally during a rage as a move
      action. At 8th level, the Ulfen Guard can change her chosen ally as a
      swift action.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.iKGmFT2CmQ71UvOX]{Tight
      Follower (Ex)}<br /><strong>Tight Follower (Ex)</strong>: At 6th level, if
      the Ulfen Guard is within 10 feet of her chosen ally and the ally moves
      more than 10 feet away from her, the Ulfen Guard can move up to her speed
      as an immediate action so long as she ends the movement within 10 feet of
      her chosen ally.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.MVbykx0SfVcMXrGI]{Reactive
      Strike (Ex)}</p><p><strong>Reactive Strike (Ex)</strong>: At 8th level, an
      Ulfen Guard can make an attack of opportunity against an enemy that moves
      adjacent to or attacks the Ulfen Guard’s chosen ally. The Ulfen Guard
      cannot make a reactive strike against a creature she has already made an
      attack of opportunity against during the round.</p><hr
      /><p>@Compendium[pf1e-archetypes.pf-prestige-features.9auxl0Pb2fdNhTfV]{Unbreakable
      Defender (Ex)}</p><p><strong>Unbreakable Defender (Ex)</strong>: At 10th
      level, as long as she is within 10 feet of her chosen ally, the Ulfen
      Guard gains the benefits of the Die Hard feat and no longer becomes
      fatigued when she ends her rage.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Chosen Ally (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.pgixjJZ2e5K2zd2E
      - level: 1
        name: Guard Dedications
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.NKUVAqFlA93zmC4E
      - level: 1
        name: Rage (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.su8CRv3Khas8Mhm2
      - level: 2
        name: Guarded Thoughts (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.5GZUKheeuViri7GV
      - level: 4
        name: Adaptable Guardian (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.uQlns00xS6v3EWtk
      - level: 6
        name: Tight Follower (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.iKGmFT2CmQ71UvOX
      - level: 8
        name: Reactive Strike (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.MVbykx0SfVcMXrGI
      - level: 10
        name: Unbreakable Defender (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.9auxl0Pb2fdNhTfV
  savingThrows:
    fort:
      value: high
    will:
      value: high
  skillsPerLevel: 4
  sources:
    - id: PZO9268
      pages: "34"
  subType: prestige
  tag: ulfenGuard
type: class

_id: lhvByJWlX3eumE46
_key: "!items!lhvByJWlX3eumE46"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/feats/weapon-focus.jpg
name: Holy Vindicator
system:
  bab: high
  classSkills:
    clm: true
    crf: true
    hea: true
    int: true
    kpl: true
    kre: true
    rid: true
    spl: true
    swm: true
  description:
    value: >-
      <p>Many faiths have within their membership an order of the church
      militant, be they holy knights or dark warriors, who put their lives and
      immortal souls on the line for their faith. They are paragons of battle,
      eschewing sermons for steel. These men and women are living conduits of
      divine power, down to their very blood, which they happily shed in a
      moment if it brings greater glory to their deity or judgment upon
      heretics, infidels, and all enemies of the faith.</p><p>Holy vindicators
      are usually clerics or fighter/clerics, though many paladins (or even
      paladin/clerics) are drawn to this class as well. In all cases, the class
      offers a further opportunity to fuse and refine their martial and
      ministerial powers and role.</p><p><strong>Role</strong>: The holy
      vindicator has substantial spellcasting ability, though not so much as a
      focused cleric or paladin. His combat skills are considerable and his
      healing powers prodigious, and those whose religious views align well with
      the vindicator will find a ready ally.</p><p><strong>Alignment</strong>:
      While lawful vindicators are somewhat more common, vindicators may be of
      any alignment.</p><h2>Requirements</h2><p>To qualify to become a holy
      vindicator, a character must fulfill all the following
      criteria.</p><p><strong>Base Attack Bonus</strong>: +5.<br
      /><strong>Special</strong>: Channel energy class feature.<br
      /><strong>Skills</strong>: Knowledge (religion) 5 ranks.<br
      /><strong>Feats</strong>: Alignment Channel or Elemental Channel.<br
      /><strong>Spells</strong>: Able to cast 1st-level divine
      spells.</p><h2>Class Skills</h2><p>The Holy Vindicator's class skills are
      Climb (Str), Heal (Wis), Intimidate (Cha), Knowledge (planes) (Int),
      Knowledge (religion) (Int), Ride (Dex), Sense Motive (Wis), Spellcraft
      (Int), and Swim (Str).</p><p><strong>Skill Points at each Level</strong>:
      2 + Int modifier.<br /><strong>Hit Die</strong>: d10.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.07441%"><strong>Level</strong></td><td
      style="width:13.1546%"><strong>Base Attack Bonus</strong></td><td
      style="width:7.63501%"><strong>Fort Save</strong></td><td
      style="width:6.89013%"><strong>Ref Save</strong></td><td
      style="width:7.63501%"><strong>Will Save</strong></td><td
      style="width:27.933%"><strong>Special</strong></td><td
      style="width:28.3054%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:8.07441%">1st</td><td style="width:13.1546%">+1</td><td
      style="width:7.63501%">+1</td><td style="width:6.89013%">+0</td><td
      style="width:7.63501%">+1</td><td style="width:27.933%">Channel energy,
      vindicator's shield</td><td style="width:28.3054%">—</td></tr><tr><td
      style="width:8.07441%">2nd</td><td style="width:13.1546%">+2</td><td
      style="width:7.63501%">+1</td><td style="width:6.89013%">+1</td><td
      style="width:7.63501%">+1</td><td style="width:27.933%">Stigmata</td><td
      style="width:28.3054%">+1 level of divine spellcasting
      class</td></tr><tr><td style="width:8.07441%">3rd</td><td
      style="width:13.1546%">+3</td><td style="width:7.63501%">+2</td><td
      style="width:6.89013%">+1</td><td style="width:7.63501%">+2</td><td
      style="width:27.933%">Faith healing (empower)</td><td
      style="width:28.3054%">+1 level of divine spellcasting
      class</td></tr><tr><td style="width:8.07441%">4th</td><td
      style="width:13.1546%">+4</td><td style="width:7.63501%">+2</td><td
      style="width:6.89013%">+1</td><td style="width:7.63501%">+2</td><td
      style="width:27.933%">Divine wrath</td><td style="width:28.3054%">+1 level
      of divine spellcasting class</td></tr><tr><td
      style="width:8.07441%">5th</td><td style="width:13.1546%">+5</td><td
      style="width:7.63501%">+3</td><td style="width:6.89013%">+2</td><td
      style="width:7.63501%">+3</td><td style="width:27.933%">Bloodfire, Channel
      Smite</td><td style="width:28.3054%">—</td></tr><tr><td
      style="width:8.07441%">6th</td><td style="width:13.1546%">+6</td><td
      style="width:7.63501%">+3</td><td style="width:6.89013%">+2</td><td
      style="width:7.63501%">+3</td><td style="width:27.933%">Versatile
      channel</td><td style="width:28.3054%">+1 level of divine spellcasting
      class</td></tr><tr><td style="width:8.07441%">7th</td><td
      style="width:13.1546%">+7</td><td style="width:7.63501%">+4</td><td
      style="width:6.89013%">+2</td><td style="width:7.63501%">+4</td><td
      style="width:27.933%">Divine judgment</td><td style="width:28.3054%">+1
      level of divine spellcasting class</td></tr><tr><td
      style="width:8.07441%">8th</td><td style="width:13.1546%">+8</td><td
      style="width:7.63501%">+4</td><td style="width:6.89013%">+3</td><td
      style="width:7.63501%">+4</td><td style="width:27.933%">Faith healing
      (maximize)</td><td style="width:28.3054%">+1 level of divine spellcasting
      class</td></tr><tr><td style="width:8.07441%">9th</td><td
      style="width:13.1546%">+9</td><td style="width:7.63501%">+5</td><td
      style="width:6.89013%">+3</td><td style="width:7.63501%">+5</td><td
      style="width:27.933%">Bloodrain</td><td
      style="width:28.3054%">—</td></tr><tr><td
      style="width:8.07441%">10th</td><td style="width:13.1546%">+10</td><td
      style="width:7.63501%">+5</td><td style="width:6.89013%">+3</td><td
      style="width:7.63501%">+5</td><td style="width:27.933%">Divine
      retribution</td><td style="width:28.3054%">+1 level of divine spellcasting
      class</td></tr></tbody></table><p><br />The following are class features
      of the holy vindicator prestige class. </p><p><strong>Weapon and Armor
      Proficiency</strong>: A vindicator is proficient with all simple and
      martial weapons and all armor and shields (except tower shields). <br
      /><br /><strong>Spells per Day</strong>: At the indicated levels, a
      vindicator gains new spells per day as if he had also gained a level in a
      divine spellcasting class he belonged to before adding the prestige class.
      He does not, however, gain other benefits of that class other than spells
      per day, spells known, and an increased effective level of spellcasting.
      If he had more than one divine spellcasting class before becoming a
      vindicator, he must decide to which class he adds the new level for the
      purpose of determining spells per day. </p><p><strong>Channel Energy
      (Su)</strong>: The vindicator's class level stacks with levels in any
      other class that grants the channel energy ability.
      </p><p><strong>Vindicator's Shield (Su)</strong>: A vindicator can channel
      energy into his shield as a standard action; when worn, the shield gives
      the vindicator a sacred bonus (if positive energy) or profane bonus (if
      negative energy) to his Armor Class equal to the number of dice of the
      vindicator's channel energy. This bonus lasts for 24 hours or until the
      vindicator is struck in combat, whichever comes first. The shield does not
      provide this bonus to any other wielder, but the vindicator does not need
      to be holding the shield for it to retain this power.
      </p><p><strong>Stigmata (Su)</strong>: A vindicator willingly gives his
      blood in service to his faith, and is marked by scarified wounds
      appropriate to his deity. He may stop or start the flow of blood by force
      of will as a standard action; at 6th level it becomes a move action, and
      at 10th level it becomes a swift action. Activating stigmata causes bleed
      damage equal to half the vindicator's class level, and this bleed damage
      is not halted by curative magic. While the stigmata are bleeding, the
      vindicator gains a sacred bonus (if he channels positive energy) or
      profane bonus (if he channels negative energy) equal to half his class
      level. Each time he activates his stigmata, the vindicator decides if the
      bonus applies to attack rolls, weapon damage rolls, Armor Class, caster
      level checks, or saving throws; to change what the bonus applies to, the
      vindicator must deactivate and reactivate his stigmata. </p><p>While his
      stigmata are bleeding, the vindicator ignores blood drain and bleed damage
      from any other source and can use <em>bleed </em>or <em>stabilize </em>at
      will as a standard action. </p><p><strong>Faith Healing (Su)</strong>: At
      3rd level, any <em>cure wounds </em>spells a vindicator casts on himself
      are automatically empowered as if by the Empower Spell feat, except they
      do not use higher spell level slots or an increased casting time. If the
      vindicator targets himself with a cure spell that affects multiple
      creatures, this ability only applies to himself. At 8th level, these
      healing spells are maximized rather than empowered. </p><p><strong>Divine
      Wrath (Sp)</strong>: At 4th level, when a vindicator confirms a critical
      hit, he may sacrifice a prepared 1st-level spell or available 1st-level
      spell slot to invoke <em>doom </em>upon the target as an immediate action
      (using the vindicator's caster level). The save DC is increased by +2 if
      his weapon has a ×3 damage multiplier, or by +4 if it is ×4. The
      vindicator can also use this ability in response to being critically hit,
      even if the attack incapacitates or kills the vindicator.
      </p><p><strong>Bloodfire (Su)</strong>: At 5th level, while a vindicator's
      stigmata are bleeding, his blood runs down his weapons like sacred or
      profane liquid energy; when he uses Channel Smite, the damage increases by
      1d6, and if the target fails its save, it is sickened and takes 1d6 points
      of bleed damage each round on its turn. The target can attempt a new save
      every round to end the sickened and bleed effects. </p><p><strong>Channel
      Smite</strong>: At 5th level, a vindicator gains Channel Smite as a bonus
      feat. </p><p><strong>Versatile Channel (Su)</strong>: At 6th level, a
      vindicator's channel energy can instead affect a 30-foot cone or a
      120-foot line. </p><p><strong>Divine Judgment (Sp)</strong>: At 7th level,
      when a vindicator's melee attack reduces a creature to –1 or fewer hit
      points, he may sacrifice a prepared 2nd-level spell or available 2nd-level
      spell slot to invoke <em>death knell </em>upon the target as an immediate
      action (using the vindicator's caster level). As vindicators mete out
      divine judgment, this is not an evil act. The save DC increases by +2 if
      his weapon has a ×3 critical multiplier, or by +4 if it is ×4.
      </p><p><strong>Bloodrain (Su)</strong>: At 9th level, while his stigmata
      are bleeding, the vindicator's harmful channeled energy is accompanied by
      a burst of sacred or profane liquid energy, increasing the damage by 1d6.
      Creatures failing their saves against the channeled energy become sickened
      and take 1d6 points of bleed damage each round. Affected creatures can
      attempt a new save every round to end the sickened and bleed effects.
      </p><p><strong>Divine Retribution (Sp)</strong>: At 10th level, when a
      vindicator confirms a critical hit, he may sacrifice a prepared 3rd-level
      spell or available 3rd-level spell slot to invoke <em>bestow curse
      </em>upon the target as an immediate action (using the vindicator's caster
      level). The save DC increases by +2 if his weapon has a ×3 critical
      multiplier, or by +4 if it is ×4. The vindicator can also use this ability
      in response to being critically hit, even if the attack incapacitates or
      kills the vindicator.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Channel Energy (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.77vml3kTsvyvHo7W
      - level: 1
        name: Vindicator's Shield (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.zhdL3vktoC8T9KCS
      - level: 2
        name: Stigmata (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.BtBdlTU7FsUQRzyD
      - level: 3
        name: Faith Healing (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.59JSdAjkvAaqG8yJ
      - level: 4
        name: Divine Wrath (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.lvHZh0Db4VK3jzx3
      - level: 5
        name: Bloodfire (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.AxK4RyAZ6NF2kist
      - level: 5
        name: Channel Smite
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.j9Ni4BQTUWLNh0k4
      - level: 6
        name: Versatile Channel (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.tetq6l8e2mh0cm42
      - level: 7
        name: Divine Judgment (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.4pS3fTxGrKhiRlvB
      - level: 9
        name: Bloodrain (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.KFdSTyVzojbYIysE
      - level: 10
        name: Divine Retribution (Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.gO4gmVkxrc7Kwor0
  savingThrows:
    fort:
      value: high
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO1115
      pages: "263"
  subType: prestige
  tag: holyVindicator
type: class

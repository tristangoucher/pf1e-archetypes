_id: 9wR7WdiRDVThoNud
_key: "!items!9wR7WdiRDVThoNud"
_stats:
  coreVersion: "12.331"
img: systems/pf1/icons/items/potions/magic-purple.jpg
name: Master Chymist
system:
  bab: high
  classSkills:
    acr: true
    clm: true
    esc: true
    int: true
    kdu: true
    ste: true
    swm: true
  description:
    value: >-
      <p>When alchemists blithely use mutagens to turn themselves into hulking
      creatures of muscle and reflex, civilized folk often turn their heads and
      mutter that such transformations must have a price. For a few alchemists,
      that price is transformation into a master chymist, a creature able to
      take a monstrous brute form as an act of will. </p><p>Master chymists
      become two personalities sharing a single body. Both the hulking
      “mutagenic form” of alchemical prowess and the original alchemist who
      created it think of themselves as the true form, and they must learn to
      work together to achieve their joint goals. More often than not, master
      chymists eventually become their mutagenic form, and the original
      alchemist's body and mind may only be brought forth when required by
      social custom or a need for obscurity and stealth arises. Unfortunately,
      the mutagenic form of a master chymist is often a more violent,
      unforgiving personality (which can lead to conflict between the two
      versions of the same character). </p><p><strong>Role</strong>: Master
      chymists are rarely accepted by society once their nature is revealed, and
      thus they have a strong motivation to keep on the move. Adventuring is one
      of the few activities that their monstrous form can be useful for, leading
      many to constantly explore dangerous areas on the edge of civilization. A
      master chymist's resilience and ability to combine bomb-throwing mayhem
      with up-close melee carnage is a great benefit to many adventuring
      parties, buying the master chymist allies who are at least willing to
      overlook her more distasteful features. </p><p><strong>Alignment</strong>:
      A master chymist actually has two alignments (see the mutagenic form
      ability). The only restriction on these alignments is that they cannot be
      exactly the same.</p><h2>Requirements</h2><p>To qualify to become a master
      chymist, a character must fulfill all the following criteria.
      </p><p><strong>Spells</strong>: Ability to create 3rd-level extracts. <br
      /><strong>Special</strong>: Mutagen class feature, feral mutagen or infuse
      mutagen discovery.</p><h2>Class Skills</h2><p>The Master Chymist's class
      skills are Acrobatics (Dex), Climb (Str), Escape Artist (Dex), Intimidate
      (Cha), Knowledge (dungeoneering) (Int), Sense Motive (Wis), Stealth (Dex),
      and Swim (Str).</p><p><strong>Skill Points at each Level</strong>: 2 + Int
      modifier.<br /><strong>Hit Die</strong>: d10.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.26672%"><strong>Level</strong></td><td
      style="width:13.3348%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.00745%"><strong>Fort Save</strong></td><td
      style="width:7.07635%"><strong>Ref Save</strong></td><td
      style="width:7.82123%"><strong>Will Save</strong></td><td
      style="width:37.8026%"><strong>Special</strong></td><td
      style="width:17.3184%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:8.26672%">1st</td><td style="width:13.3348%">+1</td><td
      style="width:8.00745%">+1</td><td style="width:7.07635%">+1</td><td
      style="width:7.82123%">+0</td><td style="width:37.8026%">Bomb-thrower,
      mutagenic form, mutate 2/day</td><td
      style="width:17.3184%">—</td></tr><tr><td
      style="width:8.26672%">2nd</td><td style="width:13.3348%">+2</td><td
      style="width:8.00745%">+1</td><td style="width:7.07635%">+1</td><td
      style="width:7.82123%">+1</td><td style="width:37.8026%">Advanced
      mutagen</td><td style="width:17.3184%">+1 level of
      alchemist</td></tr><tr><td style="width:8.26672%">3rd</td><td
      style="width:13.3348%">+3</td><td style="width:8.00745%">+2</td><td
      style="width:7.07635%">+2</td><td style="width:7.82123%">+1</td><td
      style="width:37.8026%">Brutality (+2)</td><td style="width:17.3184%">+1
      level of alchemist</td></tr><tr><td style="width:8.26672%">4th</td><td
      style="width:13.3348%">+4</td><td style="width:8.00745%">+2</td><td
      style="width:7.07635%">+2</td><td style="width:7.82123%">+1</td><td
      style="width:37.8026%">Advanced mutagen</td><td
      style="width:17.3184%">—</td></tr><tr><td
      style="width:8.26672%">5th</td><td style="width:13.3348%">+5</td><td
      style="width:8.00745%">+3</td><td style="width:7.07635%">+3</td><td
      style="width:7.82123%">+2</td><td style="width:37.8026%">Mutate
      3/day</td><td style="width:17.3184%">+1 level of
      alchemist</td></tr><tr><td style="width:8.26672%">6th</td><td
      style="width:13.3348%">+6</td><td style="width:8.00745%">+3</td><td
      style="width:7.07635%">+3</td><td style="width:7.82123%">+2</td><td
      style="width:37.8026%">Advanced mutagen</td><td style="width:17.3184%">+1
      level of alchemist</td></tr><tr><td style="width:8.26672%">7th</td><td
      style="width:13.3348%">+7</td><td style="width:8.00745%">+4</td><td
      style="width:7.07635%">+4</td><td style="width:7.82123%">+2</td><td
      style="width:37.8026%">Brutality (+4)</td><td style="width:17.3184%">+1
      level of alchemist</td></tr><tr><td style="width:8.26672%">8th</td><td
      style="width:13.3348%">+8</td><td style="width:8.00745%">+4</td><td
      style="width:7.07635%">+4</td><td style="width:7.82123%">+3</td><td
      style="width:37.8026%">Advanced mutagen, mutate 4/day</td><td
      style="width:17.3184%">—</td></tr><tr><td
      style="width:8.26672%">9th</td><td style="width:13.3348%">+9</td><td
      style="width:8.00745%">+5</td><td style="width:7.07635%">+5</td><td
      style="width:7.82123%">+3</td><td style="width:37.8026%">Brutality
      (+6)</td><td style="width:17.3184%">+1 level of alchemist</td></tr><tr><td
      style="width:8.26672%">10th</td><td style="width:13.3348%">+10</td><td
      style="width:8.00745%">+5</td><td style="width:7.07635%">+5</td><td
      style="width:7.82123%">+3</td><td style="width:37.8026%">Advanced mutagen,
      mutate 5/day</td><td style="width:17.3184%">+1 level of
      alchemist</td></tr></tbody></table><p><br />The following are class
      features of the master chymist prestige class. </p><p><strong>Weapon and
      Armor Proficiency</strong>: A master chymist gains no proficiency with any
      weapon or armor. </p><p><strong>Extracts per Day</strong>: At the
      indicated levels, a master chymist gains new extracts per day as if she
      had also gained a level in alchemist. She does not, however, gain other
      benefits a character of that class would have gained, except for extracts
      per day and an increased effective caster level for extracts.
      </p><p><strong>Bomb-Thrower (Ex)</strong>: The destructive power of bombs
      appeals to the violent urges of a master chymist. Add the character's
      alchemist and master chymist levels together to determine the damage done
      by her bombs. </p><p><strong>Mutagenic Form (Ex)</strong>: A master
      chymist's mutagenic form is an alter ego that has a different personality
      than her normal form, an outgrowth of the mental changes caused by the
      mutagenic potions she has consumed over the course of her career. The
      mutagenic form shares memories and basic goals with the chymist's normal
      personality but goes about meeting those goals in a different manner. The
      mutagenic form is often ugly and monstrous and may even appear to be a
      different race or gender than her normal form; they may look as different
      as two unrelated people. Indeed, the mutagenic form often has his or her
      own name, and may attempt to maintain independent relationships and
      strongholds (though the alter ego's limited time in existence often makes
      this difficult). The mutagenic form even has his or her own alignment
      (which is selected by the player, but must be different from the master
      chymist's normal alignment). The change in alignment only affects the
      master chymist while in her mutagenic form. </p><p><em>Example</em>:
      Darabont is a neutral good gnome alchemist 7/master chymist 4. Her
      mutagenic form is a deformed, twisted creature called Butcher. Butcher is
      neutral, and more interested in seeing the world kept in balance than
      promoting the greatest good. Butcher is aware she exists only when called
      on by Darabont, but seeks to build her own circle of like-minded friends
      during the hours she exists. Butcher does not dislike Darabont, but feels
      her gnome form is too soft and innocent to survive in the harsh world the
      chymist lives in. As Darabont, the character detects as good and is
      affected as a good character by spells with effects that vary by
      alignment; as Butcher, the same character is neutral, is not revealed by a
      detect good spell, and is treated as neutral for all spells and effects.
      </p><p><strong>Mutate (Su)</strong>: At 1st level, as a result of repeated
      exposure to her mutagens, the master chymist can now assume a mutagenic
      form twice per day without imbibing her mutagen. In this form, she gains
      all the bonuses and penalties of her mutagen and adds together her
      alchemist and master chymist levels together to determine her effective
      alchemist level for the duration of this form. Using a mutagen also forces
      the chymist into this form. Taking a mutagen or using the mutate ability
      again while in her mutagenic form works normally (with the new mutagen's
      modifiers replacing the current modifiers, and the longer duration taking
      precedent). The chymist remains in her mutagenic form until its duration
      expires, her magic is interrupted (as with an <em>antimagic field</em>),
      or she expends another use of her mutate ability. </p><p>A chymist may be
      forced to take her mutagenic form against her will by stress or damage.
      Anytime the character is in her normal form and has daily uses of the
      mutate ability available, she may be forced to switch after suffering a
      critical hit or failing a Fortitude save. In these situations the chymist
      must make a DC 25 Will save; if she fails, on her next turn she uses a
      standard action to change to her mutagenic form (which counts as a use of
      the mutate ability). </p><p>At 5th level, the master chymist can assume
      her mutagenic form three times per day; this increases to four times per
      day at 8th level and five times per day at 10th level.
      </p><p><strong>Advanced Mutagen (Su)</strong>: At 2nd level, the mutagenic
      form of the master chymist continues to evolve and develop as she grows in
      power. The master chymist selects an advanced mutagen, a power that
      changes how her mutagen form works or can only be accessed in her
      mutagenic form. She gains additional advanced mutagens at 4th, 6th, 8th,
      and 10th level. The chymist cannot select the same advanced mutagen more
      than once. </p><p><em>Burly (Ex)</em>: In her mutagenic form, the master
      chymist's heavy physical frame gives her an alchemical bonus on Strength
      checks, Constitution checks, and Strength-based skill checks as well as a
      bonus to CMB and CMD. The bonus is equal to half the master chymist's
      class level. </p><p><em>Disguise (Ex)</em>: When in her mutagenic form,
      the chymist can temporarily change her appearance to her normal form and
      still retain most of the abilities of her mutagenic form. As a standard
      action, she may make a Will saving throw (DC 20) to assume the appearance
      of her normal form for one minute. Each additional minute beyond the first
      requires a new saving throw with a +1 increase to the DC. Failure means
      the chymist assumes her normal form (as if ending the use of mutagenic
      form) or reverts fully to her mutagenic form. At any time while using this
      ability, the chymist can resume her normal form as a standard action or
      relax her will and revert to her mutagenic form as a free action. Once
      this ability ends, the chymist cannot use it again until 10 minutes have
      passed. Obvious physical changes in mutagenic form such as draconic
      mutagen, feral mutagen, and growth mutagen do not work while the chymist
      is disguised in her normal form. Time spent disguised counts toward the
      chymist's time in her mutagenic form. </p><p><em>Draconic Mutagen
      (Su)</em>: When the chymist assumes her mutagenic form, she gains
      dragon-like features—scaly skin, reptilian eyes, and so on, resembling a
      half-dragon. The chymist chooses one dragon type (see the draconic
      bloodline) when selecting this advanced mutagen; once selected, this
      choice cannot be changed and determines her draconic resistances and
      breath weapon type. The chymist gains resistance 20 to the dragon's energy
      type. The chymist's breath weapon deals 8d8 points of energy damage
      (Reflex half, DC 10 + the chymist's class level + the chymist's
      Intelligence modifier); she may use her breath weapon once per
      transformation into her mutagenic form. The character must have an
      effective alchemist level (alchemist level plus master chymist levels) of
      at least 16, must know the <em>form of the dragon I </em>extract, and must
      have the feral mutagen discovery or advanced mutagen to select this
      ability. </p><p><em>Dual Mind (Ex)</em>: The chymist's alter ego gives her
      a +2 bonus on Will saving throws in her normal and mutagenic forms. If she
      is affected by an enchantment spell or effect and fails her saving throw,
      she can attempt it again 1 round later at the same DC; if she succeeds,
      she is free of the effect (as if she had made her original save) and
      immediately changes to her mutagenic form or back to her normal form. If
      she has no more uses of the mutate ability remaining for the day, she
      cannot use dual mind. The character must have an effective alchemist level
      (alchemist level plus master chymist levels) of at least 10 to select this
      ability. </p><p><em>Evasion (Ex)</em>: This mutagen functions as the rogue
      ability of the same name, except that it only applies in the chymist's
      mutagenic form. </p><p><em>Extended Mutagen (Ex)</em>: The duration of the
      master chymist's mutation is doubled. </p><p><em>Feral Mutagen (Ex)</em>:
      This mutagen is identical to the alchemist discovery of the same name and
      counts as that discovery for the purpose of qualifying for other
      discoveries or advanced mutagens. </p><p><em>Furious Mutagen (Ex)</em>:
      The damage dice for the feral mutagen's bite and claw attacks increase by
      one die step. The character must have an effective alchemist level
      (alchemist level plus chymist level) of at least 11 and must have the
      feral mutagen discovery or advanced mutagen to select this ability.
      </p><p><em>Grand Mutagen (Ex)</em>: This mutagen is identical to the
      alchemist discovery of the same name and counts as that discovery for the
      purpose of qualifying for other discoveries or advanced mutagens. The
      character must have an effective alchemist level (alchemist level plus
      master chymist levels) of at least 16 and must have the feral mutagen
      discovery or advanced mutagen to select this ability. </p><p><em>Greater
      Mutagen (Ex)</em>: This mutagen is identical to the alchemist discovery of
      the same name and counts as that discovery for the purpose of qualifying
      for other discoveries or advanced mutagens. The character must have an
      effective alchemist level (alchemist level plus chymist level) of at least
      12 and must have the feral mutagen discovery or advanced mutagen to select
      this ability. </p><p><em>Growth Mutagen (Su)</em>: When the chymist
      assumes her mutagenic form, she increases one size category, as if under
      the effects of an <em>enlarge person </em>spell. The character must have
      an effective alchemist level (alchemist level plus chymist level) of at
      least 16 and must know the <em>enlarge person</em>, <em>giant form I</em>,
      or polymorph extract to select this ability. </p><p><em>Night Vision
      (Ex)</em>: The chymist gains darkvision 60 feet and low-light vision in
      her mutagenic form. </p><p><em>Nimble (Ex)</em>: The master chymist's
      lithe physical frame gives her an alchemical bonus on all Dexterity
      checks, Dexterity skill checks, and CMD, and a natural armor bonus to her
      Armor Class. The bonus is equal to half the master chymist's class level.
      </p><p><em>Restoring Change (Su)</em>: When the chymist assumes her
      mutagenic form or returns to her normal form from her mutagenic form, she
      heals a number of hit points equal to 1d8 + her character level.
      </p><p><em>Scent (Ex)</em>: The master chymist gains the scent ability in
      her mutagenic form. </p><p><strong>Brutality (Ex)</strong>: At 3rd level,
      a master chymist's taste for violence leads her to strike more powerful
      blows with weapons easily mastered by her bestial mind. At 3rd level, a
      chymist in her mutagenic form deals +2 damage when attacking with simple
      weapons and natural attacks. This bonus increases to +4 at 7th level and
      to +6 at 9th level.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Mutate (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.dw2lo0ytXUQfpxaB
      - level: 1
        name: Mutagenic Form (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.J5uqa9brTgiNRozt
      - level: 1
        name: Bomb-Thrower (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.fv9k329A8WycEdr4
      - level: 2
        name: Advanced Mutagen (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.D00WDZUSAheogWri
      - level: 3
        name: Brutality (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.x6B1UjQy8TO4FVhf
  savingThrows:
    fort:
      value: high
    ref:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO1115
      pages: "267"
  subType: prestige
  tag: masterChymist
type: class

_id: FFjy4c39WOyew64b
_key: "!items!FFjy4c39WOyew64b"
_stats:
  coreVersion: "12.331"
flags:
  pf1:
    links:
      classAssociations:
        FyCYDbVwdRwv9sBz: 1
        YcwaSbvVwdqekW5e: 3
        yeMOHi93gdPPz4dA: 1
img: icons/equipment/head/hood-cloth-teal.webp
name: Arcane Trickster
system:
  classSkills:
    acr: true
    apr: true
    blf: true
    clm: true
    crf: true
    dev: true
    dip: true
    dis: true
    esc: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    per: true
    spl: true
    ste: true
    swm: true
  description:
    value: >-
      <p>Few can match the guile and craftiness of arcane tricksters. These
      prodigious thieves blend the subtlest aspects of the arcane with the
      natural cunning of the bandit and the scoundrel, using spells to enhance
      their natural thieving abilities. Arcane tricksters can pick locks, disarm
      traps, and lift purses from a safe distance using their magical
      legerdemain, and as often as not seek humiliation as a goal to triumph
      over their foes than more violent solutions. </p><p>The path to becoming
      an arcane trickster is a natural progression for rogues who have
      supplemented their talents for theft with the study of the arcane.
      Multiclass rogue/sorcerers and rogue/bards are the most common arcane
      tricksters, although other combinations are possible. Arcane tricksters
      are most often found in large, cosmopolitan cities where their talents for
      magical larceny can be most effectively put to use, prowling the streets
      and stealing from the unwary. </p><p><strong>Role</strong>: With their
      mastery of magic, arcane tricksters can make for even more subtle or
      confounding opponents than standard rogues. Ranged legerdemain enhances
      their skill as thieves, and their ability to make sneak attacks without
      flanking or as part of a spell can make arcane tricksters formidable
      damage-dealers. </p><p><strong>Alignment</strong>: All arcane tricksters
      have a penchant for mischief and thievery, and are therefore never lawful.
      Although they sometimes acquire their magical abilities through the
      studious path of wizardry, their magical aptitude more often stems from a
      sorcerous bloodline. As such, many arcane tricksters are of a chaotic
      alignment.</p><h2>Requirements</h2><p>To qualify to become an arcane
      trickster, a character must fulfill all of the following criteria.
      </p><p><strong>Alignment</strong>: Any nonlawful. <br
      /><strong>Skills</strong>: Disable Device 4 ranks, Escape Artist 4 ranks,
      Knowledge (arcana) 4 ranks. <br /><strong>Spells</strong>: Ability to cast
      <em>mage hand </em>and at least one arcane spell of 2nd level or higher.
      <br /><strong>Special</strong>: Sneak attack +2d6.</p><h2>Class
      Skills</h2><p>The Arcane Trickster's class skills are Acrobatics (Dex),
      Appraise (Int), Bluff (Cha), Climb (Str), Diplomacy (Cha), Disable Device
      (Int), Disguise (Cha), Escape Artist (Dex), Knowledge (all skills taken
      individually) (Int), Perception (Wis), Sense Motive (Wis), Sleight of Hand
      (Dex), Spellcraft (Int), Stealth (Dex), and Swim
      (Str).</p><p><strong>Skill Points at each Level</strong>: 4 + Int
      modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:10.7667%"><strong>Level</strong></td><td
      style="width:9.90371%"><strong>Base Attack Bonus</strong></td><td
      style="width:7.44879%"><strong>Fort Save</strong></td><td
      style="width:6.70391%"><strong>Ref Save</strong></td><td
      style="width:7.26257%"><strong>Will Save</strong></td><td
      style="width:38.5475%"><strong>Special</strong></td><td
      style="width:18.9944%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:10.7667%">1st</td><td style="width:9.90371%">+0</td><td
      style="width:7.44879%">+0</td><td style="width:6.70391%">+1</td><td
      style="width:7.26257%">+1</td><td style="width:38.5475%">Ranged
      legerdemain</td><td style="width:18.9944%">+1 level of existing
      class</td></tr><tr><td style="width:10.7667%">2nd</td><td
      style="width:9.90371%">+1</td><td style="width:7.44879%">+1</td><td
      style="width:6.70391%">+1</td><td style="width:7.26257%">+1</td><td
      style="width:38.5475%">Sneak attack +1d6</td><td style="width:18.9944%">+1
      level of existing class</td></tr><tr><td
      style="width:10.7667%">3rd</td><td style="width:9.90371%">+1</td><td
      style="width:7.44879%">+1</td><td style="width:6.70391%">+2</td><td
      style="width:7.26257%">+2</td><td style="width:38.5475%">Impromptu sneak
      attack 1/day</td><td style="width:18.9944%">+1 level of existing
      class</td></tr><tr><td style="width:10.7667%">4th</td><td
      style="width:9.90371%">+2</td><td style="width:7.44879%">+1</td><td
      style="width:6.70391%">+2</td><td style="width:7.26257%">+2</td><td
      style="width:38.5475%">Sneak attack +2d6</td><td style="width:18.9944%">+1
      level of existing class</td></tr><tr><td
      style="width:10.7667%">5th</td><td style="width:9.90371%">+2</td><td
      style="width:7.44879%">+2</td><td style="width:6.70391%">+3</td><td
      style="width:7.26257%">+3</td><td style="width:38.5475%">Tricky spells
      3/day</td><td style="width:18.9944%">+1 level of existing
      class</td></tr><tr><td style="width:10.7667%">6th</td><td
      style="width:9.90371%">+3</td><td style="width:7.44879%">+2</td><td
      style="width:6.70391%">+3</td><td style="width:7.26257%">+3</td><td
      style="width:38.5475%">Sneak attack +3d6</td><td style="width:18.9944%">+1
      level of existing class</td></tr><tr><td
      style="width:10.7667%">7th</td><td style="width:9.90371%">+3</td><td
      style="width:7.44879%">+2</td><td style="width:6.70391%">+4</td><td
      style="width:7.26257%">+4</td><td style="width:38.5475%">Impromptu sneak
      attack 2/day, tricky spells 4/day</td><td style="width:18.9944%">+1 level
      of existing class</td></tr><tr><td style="width:10.7667%">8th</td><td
      style="width:9.90371%">+4</td><td style="width:7.44879%">+3</td><td
      style="width:6.70391%">+4</td><td style="width:7.26257%">+4</td><td
      style="width:38.5475%">Sneak attack +4d6</td><td style="width:18.9944%">+1
      level of existing class</td></tr><tr><td
      style="width:10.7667%">9th</td><td style="width:9.90371%">+4</td><td
      style="width:7.44879%">+3</td><td style="width:6.70391%">+5</td><td
      style="width:7.26257%">+5</td><td style="width:38.5475%">Invisible thief,
      tricky spells 5/day</td><td style="width:18.9944%">+1 level of existing
      class</td></tr><tr><td style="width:10.7667%">10th</td><td
      style="width:9.90371%">+5</td><td style="width:7.44879%">+3</td><td
      style="width:6.70391%">+5</td><td style="width:7.26257%">+5</td><td
      style="width:38.5475%">Sneak attack +5d6, surprise spells</td><td
      style="width:18.9944%">+1 level of existing
      class</td></tr></tbody></table><p><br />All of the following are class
      features of the arcane trickster prestige class. </p><p><strong>Weapon and
      Armor Proficiency</strong>: Arcane tricksters gain no proficiency with any
      weapon or armor. </p><p><strong>Spells per Day</strong>: When a new arcane
      trickster level is gained, the character gains new spells per day as if
      she had also gained a level in a spellcasting class she belonged to before
      adding the prestige class. She does not, however, gain other benefits a
      character of that class would have gained, except for additional spells
      per day, spells known (if she is a spontaneous spellcaster), and an
      increased effective level of spellcasting. If a character had more than
      one spellcasting class before becoming an arcane trickster, she must
      decide to which class she adds the new level for purposes of determining
      spells per day. </p><p><strong>Ranged Legerdemain (Su)</strong>: An arcane
      trickster can use Disable Device and Sleight of Hand at a range of 30
      feet. Working at a distance increases the normal skill check DC by 5, and
      an arcane trickster cannot take 10 on this check. Any object to be
      manipulated must weigh 5 pounds or less. She can only use this ability if
      she has at least 1 rank in the skill being used. </p><p><strong>Sneak
      Attack</strong>: This is exactly like the rogue ability of the same name.
      The extra damage dealt increases by +1d6 every other level (2nd, 4th, 6th,
      8th, and 10th). If an arcane trickster gets a sneak attack bonus from
      another source, the bonuses on damage stack. </p><p><strong>Impromptu
      Sneak Attack (Ex)</strong>: Beginning at 3rd level, once per day an arcane
      trickster can declare one melee or ranged attack she makes to be a sneak
      attack (the target can be no more than 30 feet distant if the impromptu
      sneak attack is a ranged attack). The target of an impromptu sneak attack
      loses any Dexterity bonus to AC, but only against that attack. The power
      can be used against any target, but creatures that are not subject to
      critical hits take no extra damage (though they still lose any Dexterity
      bonus to AC against the attack). </p><p>At 7th level, an arcane trickster
      can use this ability twice per day. </p><p><strong>Tricky Spells
      (Su)</strong>: Starting at 5th level, an arcane trickster can cast her
      spells without their somatic or verbal components, as if using the Still
      Spell and Silent Spell feats. Spells cast using this ability do not
      increase in spell level or casting time. She can use this ability 3 times
      per day at 5th level and one additional time per every two levels
      thereafter, to a maximum of 5 times per day at 9th level. The arcane
      trickster decides to use this ability at the time of casting.
      </p><p><strong>Invisible Thief (Su)</strong>: At 9th level, an arcane
      trickster can become <em>invisible</em>, as if under the effects of
      <em>greater invisibility</em>, as a free action. She can remain invisible
      for a number of rounds per day equal to her arcane trickster level. Her
      caster level for this effect is equal to her caster level. These rounds
      need not be consecutive. </p><p><strong>Surprise Spells</strong>: At 10th
      level, an arcane trickster can add her sneak attack damage to any spell
      that deals damage, if the targets are flat-footed. This additional damage
      only applies to spells that deal hit point damage, and the additional
      damage is of the same type as the spell. If the spell allows a saving
      throw to negate or halve the damage, it also negates or halves the sneak
      attack damage.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - img: systems/pf1/icons/skills/red_13.jpg
        level: 1
        name: Sneak Attack (ROG)
        uuid: Compendium.pf1.class-abilities.Item.6pjr8jMFSKqXkBKk
      - img: icons/equipment/head/hood-cloth-teal.webp
        level: 2
        name: Impromptu Sneak Attack (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.FNOug8fXW22o0wKX
      - img: icons/equipment/head/hood-cloth-teal.webp
        level: 3
        name: Ranged Legerdemain (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.qsdjQoX1dj2uwgm5
      - img: icons/equipment/head/hood-cloth-teal.webp
        level: 7
        name: Tricky Spells (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.TXwSslX728gyaxaS
      - img: icons/equipment/head/hood-cloth-teal.webp
        level: 9
        name: Invisible Thief (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.uz9ltiJv8SbMet98
      - img: icons/equipment/head/hood-cloth-teal.webp
        level: 10
        name: Surprise Spells
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Item.VTVzfi8EPHWqkP65
  savingThrows:
    ref:
      value: high
    will:
      value: high
  skillsPerLevel: 4
  sources:
    - id: PZO1110
      pages: "376"
  subType: prestige
  tag: arcaneTrickster
type: class
